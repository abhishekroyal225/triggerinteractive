part of 'authentication_bloc.dart';

abstract class AuthenticationState extends BaseEquatable {}

class AuthenticationUninitializedState extends AuthenticationState {}

class AuthenticationAuthenticatedState extends AuthenticationState {}

class AuthenticationUnauthenticatedState extends AuthenticationState {}

class AuthenticationLoadingState extends AuthenticationState {}
