import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/un_authenticated_event.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/preference_helper.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  SubscriptionStatus subscriptionStatus = SubscriptionStatus.none;

  AuthenticationBloc() : super(AuthenticationUninitializedState()) {
    eventBus.on<UnAuthenticatedEvent>().listen((event) {
      add(LoggedOutEvent());
    });
  }

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppStartedEvent) {
      await Future.delayed(const Duration(seconds: 1));
      yield AuthenticationUnauthenticatedState();
    }

    if (event is LoggedInEvent) {
      yield AuthenticationAuthenticatedState();
    }

    if (event is LoggedOutEvent) {
      PreferenceHelper.clearStorage();
      yield AuthenticationUnauthenticatedState();
    }
  }
}

enum SubscriptionStatus { none, monthly, yearly }
