part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends BaseEquatable {}

class AppStartedEvent extends AuthenticationEvent {}

class LoggedInEvent extends AuthenticationEvent {}

class LoggedOutEvent extends AuthenticationEvent {}