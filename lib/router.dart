import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/authentication/authentication_bloc.dart';
import 'package:trigger_interactive_app/screens/edit_username/bloc/edit_username_bloc.dart';
import 'package:trigger_interactive_app/screens/edit_username/edit_username_screen.dart';
import 'package:trigger_interactive_app/screens/home/bloc/home_tab_bloc.dart';
import 'package:trigger_interactive_app/screens/home/home_tab_screen.dart';
import 'package:trigger_interactive_app/screens/landing/bloc/landing_bloc.dart';
import 'package:trigger_interactive_app/screens/landing/landing_screen.dart';
import 'package:trigger_interactive_app/screens/manage_devices/bloc/manage_devices_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/bloc/scenario_progress_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/scenario_progress_screen.dart';
import 'package:trigger_interactive_app/screens/scenario_settings/bloc/scenario_settings_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_settings/scenario_settings_screen.dart';
import 'package:trigger_interactive_app/screens/score_board/bloc/score_board_bloc.dart';
import 'package:trigger_interactive_app/screens/settings/bloc/settings_bloc.dart';
import 'package:trigger_interactive_app/screens/splash_screen.dart';
import 'package:trigger_interactive_app/screens/subscription/bloc/subscription_bloc.dart';
import 'package:trigger_interactive_app/screens/subscription/subscription_screen.dart';
import 'package:trigger_interactive_app/screens/train/bloc/train_bloc.dart';

class AppRoutes {
  AppRoutes._();

  static const String SPLASH_SCREEN = "splash_screen";
  static const String LANDING_SCREEN = "landing_screen";
  static const String HOME_SCREEN = "home_screen";
  static const String SIGNUP_SCREEN = "signup_screen";
  static const String SCENARIO_SETTINGS_SCREEN = "scenario_settings_screen";
  static const String SCENARIO_PROGRESS_SCREEN = "scenario_progress_screen";
  static const String SUBSCRIPTION_SCREEN = "subscription_screen";
  static const String EDIT_USERNAME_SCREEN = "edit_username_screen";
}

Route<dynamic> getRoute(RouteSettings settings) {
  switch (settings.name) {
    case AppRoutes.SPLASH_SCREEN:
      return buildSplashScreen();
    case AppRoutes.LANDING_SCREEN:
      return buildLandingScreen();
    case AppRoutes.HOME_SCREEN:
      return buildHomeScreen(settings);
    case AppRoutes.SCENARIO_SETTINGS_SCREEN:
      return buildScenarioSettingsScreen(settings);
    case AppRoutes.SCENARIO_PROGRESS_SCREEN:
      return buildScenarioProgressScreen(settings);
    case AppRoutes.SUBSCRIPTION_SCREEN:
      return buildSubscriptionScreen();
    case AppRoutes.EDIT_USERNAME_SCREEN:
      return buildEditUsernameScreen();
    default:
      return null;
  }
}

Route buildSplashScreen() {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.SPLASH_SCREEN),
    builder: (context) => addAuthBloc(context, PageBuilder.buildSplashScreen()),
  );
}

Route buildLandingScreen() {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.LANDING_SCREEN),
    builder: (context) =>
        addAuthBloc(context, PageBuilder.buildLandingScreen()),
  );
}

Route buildHomeScreen(RouteSettings settings) {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.HOME_SCREEN),
    builder: (context) => addAuthBloc(
      context,
      PageBuilder.buildHomeScreen(settings),
    ),
  );
}

Route buildScenarioSettingsScreen(RouteSettings settings) {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.SCENARIO_SETTINGS_SCREEN),
    builder: (context) => addAuthBloc(
      context,
      PageBuilder.buildScenarioSettingsScreen(settings),
    ),
  );
}

Route buildScenarioProgressScreen(RouteSettings settings) {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.SCENARIO_PROGRESS_SCREEN),
    builder: (context) => addAuthBloc(
      context,
      PageBuilder.buildScenarioProgressScreen(settings),
    ),
  );
}

Route buildSubscriptionScreen() {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.SUBSCRIPTION_SCREEN),
    builder: (context) => addAuthBloc(
      context,
      PageBuilder.buildSubscriptionScreen(),
    ),
  );
}

Route buildEditUsernameScreen() {
  return MaterialPageRoute(
    settings: const RouteSettings(name: AppRoutes.EDIT_USERNAME_SCREEN),
    builder: (context) => addAuthBloc(
      context,
      PageBuilder.buildEditUsernameScreen(),
    ),
  );
}

class PageBuilder {
  PageBuilder._();

  static Widget buildSplashScreen() {
    return SplashScreen();
  }

  static Widget buildLandingScreen() {
    return BlocProvider<LandingBloc>(
      create: (context) {
        return LandingBloc()..add(LandingInitialEvent());
      },
      child: LandingScreen(),
    );
  }

  static Widget buildHomeScreen(RouteSettings settings) {
    final HomeTabArguments args = settings.arguments as HomeTabArguments;

    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeTabBloc>(
          create: (context) {
            return HomeTabBloc(args)..add(HomeTabInitialEvent());
          },
        ),
        BlocProvider<TrainBloc>(
          create: (context) {
            return TrainBloc()..add(TrainInitialEvent());
          },
        ),
        BlocProvider<ManageDevicesBloc>(
          create: (context) {
            return ManageDevicesBloc()..add(ManageDevicesInitialEvent());
          },
        ),
        BlocProvider<ScoreBoardBloc>(
          create: (context) {
            return ScoreBoardBloc()..add(ScoreBoardInitialEvent());
          },
        ),
        BlocProvider<SettingsBloc>(
          create: (context) {
            return SettingsBloc()..add(SettingsInitialEvent());
          },
        )
      ],
      child: HomeTabScreen(),
    );
  }

  static Widget buildScenarioSettingsScreen(RouteSettings settings) {
    final ScenarioSettingsArguments args =
        settings.arguments as ScenarioSettingsArguments;

    return BlocProvider<ScenarioSettingsBloc>(
      create: (context) {
        return ScenarioSettingsBloc(args)..add(ScenarioSettingsInitialEvent());
      },
      child: ScenarioSettingsScreen(),
    );
  }

  static Widget buildScenarioProgressScreen(RouteSettings settings) {
    final ScenarioProgressArguments args =
        settings.arguments as ScenarioProgressArguments;

    return BlocProvider<ScenarioProgressBloc>(
      create: (context) {
        return ScenarioProgressBloc(args)..add(ScenarioProgressInitialEvent());
      },
      child: ScenarioProgressScreen(),
    );
  }

  static Widget buildSubscriptionScreen() {
    return BlocProvider(
      create: (context) {
        return SubscriptionBloc()..add(SubscriptionInitialEvent());
      },
      child: SubscriptionScreen(),
    );
  }

  static Widget buildEditUsernameScreen() {
    return BlocProvider(
      create: (context) {
        return EditUsernameBloc()..add(EditUsernameInitialEvent());
      },
      child: EditUsernameScreen(),
    );
  }
}

Widget addAuthBloc(BuildContext context, Widget widget) {
  return BlocConsumer<AuthenticationBloc, AuthenticationState>(
    listener: (BuildContext context, AuthenticationState state) {
      if (state is AuthenticationUnauthenticatedState) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        Navigator.pushReplacementNamed(context, AppRoutes.LANDING_SCREEN);
      }

      if (state is AuthenticationAuthenticatedState) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        Navigator.pushReplacementNamed(context, AppRoutes.HOME_SCREEN);
      }
    },
    builder: (context, state) {
      if (state is AuthenticationUninitializedState ||
          state is AuthenticationLoadingState) {
        return PageBuilder.buildSplashScreen();
      } else {
        return widget;
      }
    },
  );
}
