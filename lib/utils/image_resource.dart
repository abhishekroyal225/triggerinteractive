class ImageResource {
  ImageResource._();

  static const String TRIGGER_LOGO = "assets/trigger_interactive_logo.svg";
  static const String TRIGGER_LOGO_ICON = "assets/trigger_interactive_icon.svg";
  static const String PASSWORD_VISIBLE = "assets/visible_password_icon.svg";
  static const String PASSWORD_HIDDEN = "assets/hide_password_icon.svg";
  static const String TRAIN_NAV_ICON = "assets/train_nav_icon.svg";
  static const String STATS_NAV_ICON = "assets/stats_nav_icon.svg";
  static const String SCORE_BOARD_NAV_ICON = "assets/score_board_nav_icon.svg";
  static const String DEVICES_NAV_ICON = "assets/devices_nav_icon.svg";
  static const String SETTINGS_NAV_ICON = "assets/setting_nav_icon.svg";
  static const String SIGNAL_STRENGTH_1 = "assets/signal_strength_1.svg";
  static const String SIGNAL_STRENGTH_2 = "assets/signal_strength_2.svg";
  static const String SIGNAL_STRENGTH_3 = "assets/signal_strength_3.svg";
  static const String SIGNAL_STRENGTH_4 = "assets/signal_strength_4.svg";
  static const String BASIC_COMBAT_TILE = "assets/basic_combat_tile.svg";
  static const String DUELING_TREE_TILE = "assets/dueling_tree_tile.svg";
  static const String ELIMINATION_TILE = "assets/elimination_tile.svg";
  static const String FRENZY_TILE = "assets/frenzy_tile.svg";
  static const String RUN_AND_GUN_TILE = "assets/run_and_gun_tile.svg";
  static const String WAVES_TILE = "assets/waves_tile.svg";
  static const String EXIT_ICON = "assets/exit_icon.svg";
  static const String PAUSE_ICON = "assets/pause_icon.svg";
  static const String PLAY_ICON = "assets/play_icon.svg";
  static const String SETUP_ICON = "assets/setup_icon.svg";
  static const String RESTART_ICON = "assets/restart_icon.svg";
  static const String TEMPERATURE_WARNING_RED =
      "assets/temperature_warning_red.svg";
  static const String TEMPERATURE_WARNING_ORANGE =
      "assets/temperature_warning_red.svg";

  static const String BATTERY_ICON = "assets/battery_icon.svg";
  static const String RED_WARNING = "assets/red_warning.svg";
  static const String WIFI_ERROR = "assets/wifi_error.svg";
}
