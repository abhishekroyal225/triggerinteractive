class Font {
  static const Font HelveticaRegular = Font("Helvetica");
  static const Font HelveticaBold = Font("Helvetica-Bold");

  final String _fontName;

  const Font(this._fontName);

  String get value => _fontName;
}
