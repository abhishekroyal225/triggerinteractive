import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trigger_interactive_app/models/user.dart';

class PreferenceHelper {
  PreferenceHelper._();

  /// get a current user
  static Future<User> getUser() async {
    String value;
    final SharedPreferences pref = await SharedPreferences.getInstance();
    value = pref.getString("user");
    if (value?.isEmpty ?? true) {
      return null;
    } else {
      final Map<String, dynamic> map = await _parseJson(value);
      return User.fromJson(map);
    }
  }

  /// save a current user
  static Future saveUser(User user) async {
    if (user == null) return;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userString = jsonEncode(user.toJson());
    await prefs.setString("user", userString);
  }

  /// Clear a storage
  static Future clearStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  static Future<Map<String, dynamic>> _parseJson(String text) {
    return compute(_parseAndDecode, text);
  }

  static Map<String, dynamic> _parseAndDecode(String response) {
    return jsonDecode(response) as Map<String, dynamic>;
  }
}
