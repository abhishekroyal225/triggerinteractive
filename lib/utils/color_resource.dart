import 'package:flutter/material.dart';

class ColorResource {
  ColorResource._();

  static const ColorState RED_96262C = ColorState(0xFF96262C);
  static const ColorState BLACK_000000 = ColorState(0xFF000000);
  static const ColorState BLACK_333333 = ColorState(0xFF333333);
  static const ColorState BLACK_6E6E6E = ColorState(0xFF6E6E6E);
  static const ColorState GREY_969696 = ColorState(0xFF969696);
  static const ColorState GREY_D2D2D2 = ColorState(0xFFD2D2D2);
  static const ColorState GREY_E7E7E7 = ColorState(0xFFE7E7E7);
  static const ColorState WHITE_FFFFFF = ColorState(0xFFFFFFFF);
  static const ColorState WHITE_F2F2F2 = ColorState(0xFFF2F2F2);
  static const ColorState RED_600C0F = ColorState(0xFF600C0F);
}

class ColorState extends MaterialStateColor {
  final int colorValue;

  const ColorState(this.colorValue) : super(colorValue);

  @override
  ColorState resolve(Set<MaterialState> states) {
    return ColorState(colorValue);
  }
}
