import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:url_launcher/url_launcher.dart';

class DebugMode {
  DebugMode._();
  static bool get isInDebugMode {
    const bool inDebugMode = true;
    return inDebugMode;
  }
}

class AppUtils {
  AppUtils._();

  static void showToast(String text,
      {Color backgroundColor = Colors.red, ToastGravity toastGravity}) {
    Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_LONG,
      gravity: toastGravity ?? ToastGravity.BOTTOM,
      backgroundColor: backgroundColor,
      textColor: Colors.white,
      fontSize: FontSize.smallFontSize,
    );
  }

  static void hideKeyBoard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static bool isKeyBoardVisible(BuildContext context) {
    return !(MediaQuery.of(context).viewInsets.bottom == 0.0);
  }

  static Future<void> openUrl(String url) async {
    if (await canLaunch(url)) {
      try {
        await launch(url);
      } finally {}
    }
  }

  static void logMessage(String message) {
    if (DebugMode.isInDebugMode) {
      // ignore: avoid_print
      print(message);
    }
  }
}
