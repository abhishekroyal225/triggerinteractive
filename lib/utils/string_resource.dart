class StringResource {
  StringResource._();

  static const String CHECK_INTERNET_CONNECTION =
      "Check your internet connection";
  static const String CONNECTION_TIME_OUT = "Connection time out";
  static const String LOADING = "Loading";
  static const String USERNAME = "Username";
  static const String CONNECT_DEVICES = "Connect Devices";
  static const String SEARCH_FOR_DEVICES = "Search for Devices";
  static const String NO_DEVICE_FOUND = "No Device Found";
  static const String MANAGE_DEVICES = "MANAGE DEVICES";
  static const String IDENTIFY = "Identify";
  static const String CONNECT_ALL_DEVICES = "Connect All Devices";
  static const String ARE_YOU_SURE_CONNECT_ALL_DEVICES =
      "Are you sure you want to connect all available devices?";
  static const String YES = "YES";
  static const String NO = "NO";
  static const String TRAIN = "TRAIN";
  static const String DEVICES = "DEVICES";
  static const String STATS = "STATS";
  static const String SETTINGS = "SETTINGS";
  static const String BASIC_COMBAT = "Basic Combat";
  static const String ELIMINATION = "Elimination";
  static const String WAVES = "Waves";
  static const String FRENZY = "Frenzy";
  static const String DUELING_TREE = "Dueling Tree";
  static const String MULTIPLAYER = "Multiplayer";
  static const String RUN_AND_GUN = "Run and Gun";
  static const String ADD_NEW_USER = "Add New User"; //TODO: remove
  static const String ACTIVE_TIME = "ACTIVE TIME";
  static const String INACTIVE_TIME = "INACTIVE TIME";
  static const String SIMULTANEOUS_TARGETS = "SIMULTANEOUS TARGETS";
  static const String TARGET_PRESENTATIONS = "TARGET PRESENTATIONS";
  static const String START = "START";
  static const String INSTRUCTIONS = "Instructions";
  static const String BASIC_COMBAT_INSTRUCTIONS = "Basic Combat Instructions";
  static const String BASIC_COMBAT_DESCRIPTION =
      "Targets are randomly activated for a set amount of time based on the difficulty level selected. Hit as many targets as possible.";
  static const String MINIMUM_TARGETS_REQUIRED = "Minimum Targets Required: ";
  static const String REPORTS = "Reports";
  static const String ADD_NEW = "Add New";
  static const String SAVE = "Save";
  static const String OK = "OK";
  static const String DELETE_SETTING = "DELETE SETTING";
  static const String ARE_YOU_SURE_DELETE = "Are you sure you want to delete";
  static const String SCENARIO_SETTING = "scenario setting";
  static const String SCENARIO_STARTING_IN = "SCENARIO STARTING IN";
  static const String TARGETS_HIT = "Targets Hit";
  static const String TARGETS_MISSED = "Targets Missed";
  static const String TOTAL_TARGETS = "Total Targets";
  static const String REACTION_TIME = "REACTION TIME";
  static const String SCORE = "SCORE";
  static const String ORDER = "ORDER";
  static const String TARGET = "TARGET";
  static const String SPLIT_TIME = "SPLIT TIME";
  static const String EXIT = "Exit";
  static const String SETUP = "Setup";
  static const String CURRENT_MONTH = "CURRENT MONTH";
  static const String YEAR_TO_DATE = "YEAR TO DATE";
  static const String TOTAL_SCORE = "TOTAL SCORE";
  static const String RANK = "RANK";
  static const String UPDATE_SUBSCRIPTION = "Update Subscription";
  static const String SOUND = "Sound";
  static const String HIT_DETECTION_SENSITIVITY = "Hit Detection Sensitivity";
  static const String MINUTES_TO_POWER_DOWN = "Minutes to Power Down";
  static const String EDIT = "Edit";
  static const String TARGETS = "Targets";
  static const String PRESET_NAME_ALREADY_EXIST =
      "Preset with the same name already exists.";
  static const String CUSTOM = "Custom";
  static const String SEARCHING_FOR_DEVICES = "Searching for Devices";
  static const String OVERWRITE_SETTINGS = "OVERWRITE SETTINGS?";
  static const String OVERWRITE_SETTINGS_DESCRIPTION =
      "Are you sure you want to overwrite these existing settings?";
  static const String CAN_ONLY_ADD_10_PRESETS = "Can only add upto 10 presets";
  static const String BLUETOOTH_NOT_ENABLED = "Bluetooth not enabled";
  static const String LOCATION_NOT_ENABLED = "Location not enabled";
  static const String LOCATION_SERVICE_NOT_ENABLED =
      "Location service not enabled";
  static const String ENABLE = "Enable";
  static const String NO_TARGETS_MESSAGE = "Doesn't have any connected targets";
  static const String SIMULTANEOUS_TARGET_ERROR =
      "Simultaneous targets should not be higher than connected targets";
  static const String NEW_USER = "New user";
  static const String CANCEL = "Cancel";
  static const String IN_PROGRESS = "IN PROGRESS";
  static const String PAUSED = "PAUSED";
  static const String COMPLETED = "COMPLETED";
  static const String SCORES = "SCORES";
  static const String SUBMIT = "Submit";
  static const String ACTIVE_TIME_INFO_TEXT =
      "The amount of time a target, or set of targets, is actively indicating the shooter to engage it. Hits will only be recorded on active targets during this time.";
  static const String INACTIVE_TIME_INFO_TEXT =
      "The amount of time in between target presentations when no targets are active. During this time, any inactive target hit will be counted as a miss.";
  static const String SIMULTANEOUS_TARGETS_INFO_TEXT =
      "The number of targets that will be active during a single presentation.";
  static const String TARGET_PRESENTATION_INFO_TEXT =
      "The total number of times the targets will present themselves to the shooter during the scenario.";
  static const String SOMETHING_WENT_WRONG = "something went wrong";
  static const String SUBSCRIBE = "Subscribe";
  static const String ACCESS_NEED_SUBSCRIPTION_TEXT =
      "You must have a subscription to access this feature.";
  static const String YOU_MUST_CONNECT_AT_LEAST = "You must connect at least";
  static const String SMART_TARGET_IN_ORDER_TEXT =
      "Smart Target Attachments in order to train with this scenario.";
  static const String GET_FULL_ACCESS = "Get full access!";
  static const String YOU_ARE_SUBSCRIBED_FULL_ACCESS =
      "You are subscribed for full access!";
  static const String SKIP_CONNECT = "Skip Connect";
  static const String SIGN_UP_FOR_EMAILS = "Sign up for Emails";
  static const String RECEIVE_FIRMWARE_NOTIFICATION_TEXT =
      "Receive firmware notification & promotions via email";
  static const String SUBSCRIBE_FOR_FULL_ACCESS = "Subscribe for full access!";
  static const String APP_VERSION = "App Version";
  static const String SUBSCRIPTION = "SUBSCRIPTION";
  static const String ANNUAL_SUBSCRIPTION = "Annual Subscription";
  static const String MONTHLY_SUBSCRIPTION = "Monthly Subscription";
  static const String PAY_THE_YEAR_OFFER_TEXT =
      "Pay the full year and save 50%";
  static const String EDIT_USERNAME = "Edit Username";
  static const String RENEWAL_DATE = "Renewal Date";
  static const String CANCEL_SUBSCRIPTION = "Cancel Subscription";
  static const String SWITCH_TO_ANNUAL = "Switch to Annual";
  static const String UPDATE_USERNAME = "Update Username";
  static const String YOUR_USERNAME = "Your Username";
  static const String USERNAME_SHOULD_NOT_BE_BLANK =
      "Username should not be blank";
  static const String USERNAME_UPDATED = "Username updated";
  static const String YOU_WILL_LOSE_ACCESS_TO = "You will lose access to:";
  static const String ALL_ADDITIONAL_SCENARIO = "All additional scenarios";
  static const String SHOOTING_STATS_TRACKING = "Shooting stats tracking";
  static const String DETAILED_SCENARIO_REPORTS = "Detailed scenario reports";
  static const String CANCEL_YOUR_SUBSCRIPTION_TEXT =
      "Are you sure you want to\ncancel your subscription?";

  static const String DEMO_DATE = "MM/DD/YYYY";
  static const String UNKNOWN = "UNKNOWN";

  static const String HUB = "Hub";
  static const String STA = "STA";
  static const String RENEGADE1133 = "RENEGADE1133";
  static const String YOU_MUST_CONNECT_3STA_TEXT =
      " You must connect at least three Smart Target Attachments in order to train with this scenario.";
  static const String LOW = "LOW";
  static const String UPDATE_FIRMWARE = "Update Firmware";
  static const String FIRMWARE_OUT_OF_DATE =
      "Device firmware is out of date. Certain scenarios or features may not be available.";
  static const String LATEST_DEVICE_FIRMWARE =
      "Latest device firmware is available for download at www.triggerinteractive.com/firmware";
  static const String DEVICE_DISCONNECTED = "Device Disconnected";
  static const String DEVICE_DISCONNECTED_DESCRIPTION =
      "One or more devices have disconnected from the system";
  static const String COMING_SOON = "Coming soon";
  static const String COMING_SOON_DESCRIPTION =
      "This scenario and other features are in development and will be available soon!";
  static const String TRIGGER_MAJOR_VERSION = "Trigger FW major version";
}
