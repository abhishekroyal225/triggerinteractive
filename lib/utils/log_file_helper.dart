import 'dart:io';

import 'package:path_provider/path_provider.dart' as path;

class LogFileHelper {
  LogFileHelper._();

  static String _path;
  static Future<void> _getPath() async {
    final directory = await path.getExternalStorageDirectories(
      type: path.StorageDirectory.downloads,
    );
    _path = "${directory.first.path}/trigger_log.txt";
  }

  static Future<void> write(String log) async {
    if (_path == null) {
      await _getPath();
    }

    final File _file = File(_path);
    await _file.writeAsString(
      '[${DateTime.now().toIso8601String()}] : $log',
      mode: FileMode.append,
    );
  }
}
