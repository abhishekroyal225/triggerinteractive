class FontSize {
  FontSize._();

  //convert pt to px https://www.ninjaunits.com/converters/pixels/points-pixels/

  static const double baseFontSize = 21.3;
  static const double subFontSize = 18.6;
  static const double smallFontSize = 13.5;

  static const double mainNavFontSize = 13.3;

  static const double countdownFontSize = 480;
  static const double scenarioTimerFontSize = 80;

  static const double reactionScoreFontSize = 33.3;
  static const double incrementDecrementSize = 33.3;

  static const double scenarioSettingsFontSize = 17;
}
