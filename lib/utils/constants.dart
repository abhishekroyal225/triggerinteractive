class Constants {
  Constants._();

  static const APP_VERSION = 2.0;
  static const FIRMWARE_VERSION = 1;

  static const MONTHLY_SUBSCRIPTION_AMOUNT = 9.99;
  static const ANNUAL_SUBSCRIPTION_AMOUNT = 4.99;

  static const SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
  static const WRITE_CHARACTERISTICS_UUID =
      "0000ffe9-0000-1000-8000-00805f9b34fb";
  static const READ_CHARACTERISTICS_UUID =
      "0000ffe4-0000-1000-8000-00805f9b34fb";

  static const SIGN_UP_NOTIFICATION_EMAIL = "https://triggerinteractive.com/";
}
