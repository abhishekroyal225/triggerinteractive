import 'package:intl/intl.dart';

class TimeUtils {
  TimeUtils._(_);
  static DateTime internalUtc = DateTime.now().toUtc(); // init with local time
  ///timeAgoSinceDate
  static String timeAgoSinceDate(String dateString,
      {bool numericDates = true}) {
    DateTime date = DateTime.parse(dateString);
    date = convertToUtc(date);
    final Duration diff = DateTime.now().difference(date);
    if (diff.inDays > 365) {
      return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "year" : "years"} ago";
    }
    if (diff.inDays > 30) {
      return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "month" : "months"} ago";
    }
    if (diff.inDays > 7) {
      return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "week" : "weeks"} ago";
    }
    if (diff.inDays > 0) {
      return "${diff.inDays} ${diff.inDays == 1 ? "day" : "days"} ago";
    }
    if (diff.inHours > 0) {
      return "${diff.inHours} ${diff.inHours == 1 ? "hour" : "hours"} ago";
    }
    if (diff.inMinutes > 0) {
      return "${diff.inMinutes} ${diff.inMinutes == 1 ? "minute" : "minutes"} ago";
    }
    return "just now";
  }

  /// Convert MMMYYYY
  static String convertMMMYYYY(String strDate) {
    final DateTime date = DateTime.parse(strDate);
    final DateFormat dateFormat = DateFormat('MMM, yyyy');
    final String formattedDate = dateFormat.format(date);
    return formattedDate;
  }

  ///convertToUtc
  static DateTime convertToUtc(DateTime time) {
    return DateTime.utc(time.year, time.month, time.day, time.hour, time.minute,
        time.second, time.millisecond, time.microsecond);
  }

  ///convertDDMMMYYYY
  static String convertDDMMMYYYY(String strDate) {
    final DateTime date = DateTime.parse(strDate);
    final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    final String formattedDate = dateFormat.format(date);
    return formattedDate;
  }

  ///convertDDMMMYYYYDateTime
  static String convertDDMMMYYYYDateTime(DateTime dateTime) {
    final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    final String formattedDate = dateFormat.format(dateTime);
    return formattedDate;
  }

  ///convertMMDDYYYYDateTime
  static String convertMMDDYYYYDateTime(DateTime dateTime) {
    final DateFormat dateFormat = DateFormat('MM/dd/yyyy');
    final String formattedDate = dateFormat.format(dateTime);
    return formattedDate;
  }

  ///convertHHMM
  static String convertHHMM(String strDate) {
    final DateTime dateTime = DateTime.parse(strDate);
    final DateTime utcTime = DateTime.utc(
        dateTime.year,
        dateTime.month,
        dateTime.day,
        dateTime.hour,
        dateTime.minute,
        dateTime.second,
        dateTime.millisecond,
        dateTime.microsecond);
    final DateTime date = utcTime.toLocal();
    final DateFormat dateFormat = DateFormat('hh:mm a');
    final String formattedDate = dateFormat.format(date);
    return formattedDate;
  }

  ///getTimeAgo
  static String getTimeAgo(String date) {
    String dateForComplemo;
    final DateTime dateTime = DateTime.parse(date);
    final DateTime utcTime = DateTime.utc(
        dateTime.year,
        dateTime.month,
        dateTime.day,
        dateTime.hour,
        dateTime.minute,
        dateTime.second,
        dateTime.millisecond,
        dateTime.microsecond);
    final DateTime dateTimeLocal = utcTime.toLocal();
    final currentDate = DateTime.now();
    final differenceInDays = currentDate.day - dateTimeLocal.day;
    if (differenceInDays == 0) {
      dateForComplemo = "Today, ${DateFormat.jm().format(dateTimeLocal)}";
    } else if (differenceInDays == 1) {
      dateForComplemo = "Yesterday, ${DateFormat.jm().format(dateTimeLocal)}";
    } else if (currentDate.year != dateTimeLocal.year) {
      dateForComplemo =
          "${DateFormat.MMMd().format(dateTimeLocal)}, ${DateFormat.y().format(dateTimeLocal)}, ${DateFormat.jm().format(dateTimeLocal)}";
    } else {
      dateForComplemo =
          "${DateFormat.MMMd().format(dateTimeLocal)}, ${DateFormat.jm().format(dateTimeLocal)}";
    }
    return dateForComplemo;
  }

  ///formatHHMMSS
  static String formatHHMMSS(int seconds) {
    int secondsValue = seconds;
    final int hours = (secondsValue / 3600).truncate();
    secondsValue = (secondsValue % 3600).truncate();
    final int minutes = (secondsValue / 60).truncate();
    final String hoursStr = (hours).toString().padLeft(2, '0');
    final String minutesStr = (minutes).toString().padLeft(2, '0');
    final String secondsStr = (secondsValue % 60).toString().padLeft(2, '0');
    if (hours == 0) {
      return "$minutesStr:$secondsStr";
    }
    return "$hoursStr:$minutesStr:$secondsStr";
  }

  static String formatMMSS(int second) {
    int secondsValue = second;
    final int minutes = (secondsValue / 60).truncate();
    secondsValue %= 60;
    final int seconds = secondsValue.truncate();
    secondsValue %= 10;

    final String minutesString = minutes.toString().padLeft(2, '0');
    final String secondsString = seconds.toString().padLeft(2, '0');

    return "$minutesString:$secondsString";
  }

  static String formatMMSSMS(int milliSeconds) {
    int milliSecondsValue = milliSeconds;
    final int minutes = (milliSecondsValue / 60000).truncate();
    milliSecondsValue %= 60000;
    final int seconds = (milliSecondsValue / 1000).truncate();
    milliSecondsValue %= 1000;

    final String minutesString = minutes.toString().padLeft(2, '0');
    final String secondsString = seconds.toString().padLeft(2, '0');
    String milliSecondsString = "";

    if (milliSecondsValue != 0) {
      milliSecondsString = ".${milliSecondsValue.toString().padLeft(3, '0')}";
    }

    return "$minutesString:$secondsString$milliSecondsString";
  }
}
