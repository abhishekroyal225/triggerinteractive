import 'package:trigger_interactive_app/db/default_data.dart';
import 'package:trigger_interactive_app/db/local_storage_manager.dart';
import 'package:trigger_interactive_app/models/local/game/game.dart';
import 'package:trigger_interactive_app/models/local/game/session.dart';
import 'package:trigger_interactive_app/models/local/game/session_data.dart';
import 'package:trigger_interactive_app/models/local/setting.dart';
import 'package:trigger_interactive_app/models/local/user_data.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/screens/score_board/bloc/score_board_bloc.dart';
import 'package:uuid/uuid.dart';

class UserDataHandler {
  static List<UserData> _allUser;
  static UserData _currentUser;
  static String _currentGameType = "Basic combat";
  static List<Game> _allGameList = [];
  static Game _currentGame;
  static Setting _settings;

  static List<String> userScenarioPresetKeys = [];

  static List<ScenarioPreset> _scenarioPresetObjects = [];

  UserDataHandler._();

  static Future<void> initialize() async {
    await LocalStorageManager.setDefaultGames(gamesData, overwrite: false);
    await LocalStorageManager.setDefaulSettings(defaultSettings);

    _allUser = await LocalStorageManager.getAllUsers;

    if (_allUser.isEmpty) {
      addNewUser();
    }

    final String lastUserId = await LocalStorageManager.lastUserId;

    if (lastUserId != null) {
      _currentUser = _allUser.firstWhere(
        (user) => user.userId == lastUserId,
        orElse: () => null,
      );
    }
    _currentUser ??= _allUser.first;
    _settings = _currentUser.settings;

    _allGameList = await LocalStorageManager.allGames;
    _settings ??= await LocalStorageManager.defaultSettings;

    _currentUser.settings = _settings;
  }

  static Setting get settings => _settings;

  static UserData get currentUser => _currentUser;

  static List<UserData> get allUsers => _allUser;

  static List<ScenarioPreset> get scenarioPresetObjects =>
      _scenarioPresetObjects;

  static List<ScoreBoardData> scoreBoardData([bool isForMonth = false]) {
    final List<ScoreBoardData> scoreBoard = _allUser
        .map((e) => e.scoreData(isForMonth))
        .where((e) => e != null)
        .toList();

    scoreBoard.sort((a, b) => b.score.compareTo(a.score));
    return scoreBoard;
  }

  static Future<void> setGameType(String currentGameType) async {
    _currentGameType = currentGameType;
    _createGameIfNotExist();

    await _storeScenarioPresets();
  }

  static Future<void> selectUser(String id) async {
    final UserData user = _allUser.firstWhere(
      (element) => element.userId == id,
      orElse: () => null,
    );

    if (user != null) {
      _currentUser = user;
      _settings = _currentUser.settings;
      LocalStorageManager.setLastUserId(_currentUser);
    }
  }

  static Future<void> addScenarioPreset(ScenarioPreset scenarioPreset,
      [String originalPresetLabel]) async {
    final int index = _currentGame.scenarios.indexWhere(
      (scenario) =>
          scenario.label == (originalPresetLabel ?? scenarioPreset.label),
    );

    if (index == -1) {
      _currentGame.scenarios.add(scenarioPreset);
      userScenarioPresetKeys.add(scenarioPreset.label);
      scenarioPresetObjects.add(scenarioPreset);
    } else {
      userScenarioPresetKeys[userScenarioPresetKeys
          .indexOf(originalPresetLabel)] = scenarioPreset.label;
      _currentGame.scenarios[index] = scenarioPreset;

      final int objectIndex = scenarioPresetObjects
          .indexWhere((preset) => preset.label == originalPresetLabel);

      if (objectIndex != -1) {
        scenarioPresetObjects[objectIndex] = scenarioPreset;
      }
    }

    await LocalStorageManager.setAllGames(_allGameList);
  }

  static Future<void> deleteScenarioPreset(String presetLabel) async {
    _currentGame.scenarios
        .removeWhere((element) => element.label == presetLabel);

    userScenarioPresetKeys.remove(presetLabel);
    await LocalStorageManager.setAllGames(_allGameList);
  }

  static Future<void> addNewUser({String name}) async {
    final List<Session> sessions = [];
    final UserData newUser = UserData(
      sessions: sessions,
      userName: name ?? "SHOOTER 1",
      settings: defaultSettings.copyWith(),
    );
    _allUser.add(newUser);

    _currentUser = newUser;

    await LocalStorageManager.setAllUsers(_allUser);
    await LocalStorageManager.setLastUserId(newUser);

    return newUser;
  }

  static Future<void> addNewSession({
    double score,
    SessionData sessionData,
  }) async {
    _currentUser.sessions.add(
      Session(
        createdAt: DateTime.now().toIso8601String(),
        gameType: _currentGameType,
        score: score,
        sessionId: const Uuid().v1(),
        sessionData: sessionData,
      ),
    );

    _currentUser.sessions.sort((a, b) => b.score.compareTo(a.score));

    await LocalStorageManager.setAllUsers(_allUser);
  }

  static Future<void> updateSetting({
    String sensitivity,
    int minutesToPowerDown,
    bool sound,
  }) async {
    if (sensitivity != null) {
      _settings.sensitivity = sensitivity;
    }
    if (minutesToPowerDown != null) {
      _settings.minutesToPowerDown = minutesToPowerDown;
    }
    if (sound != null) {
      _settings.sound = sound;
    }

    await LocalStorageManager.setAllUsers(_allUser);
  }

  static Future<void> updateUsername(String username) async {
    if (username.isNotEmpty) {
      _currentUser.userName = username;
    }

    await LocalStorageManager.setAllUsers(_allUser);
  }

  static void _createGameIfNotExist() {
    _currentGame = _allGameList.firstWhere(
      (element) => element.gameType == _currentGameType,
      orElse: () => null,
    );

    if (_currentGame == null) {
      final List<ScenarioPreset> scenarioPresets = [];
      _currentGame = Game(_currentGameType, scenarioPresets);
      _allGameList.add(_currentGame);
      LocalStorageManager.setAllGames(_allGameList);
    }
  }

  static Future<void> _storeScenarioPresets() async {
    final List<Game> defaultGames = await LocalStorageManager.defaultGames;

    final Game currentDefaultGame = defaultGames.firstWhere(
      (game) => game.gameType == _currentGameType,
      orElse: () => null,
    );

    _scenarioPresetObjects = currentDefaultGame?.scenarios ?? [];

    userScenarioPresetKeys =
        _currentGame?.scenarios?.map((game) => game.label)?.toList();

    _scenarioPresetObjects.addAll(_currentGame?.scenarios ?? []);
  }
}
