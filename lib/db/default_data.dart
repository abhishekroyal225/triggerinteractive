import 'package:trigger_interactive_app/models/local/game/game.dart';
import 'package:trigger_interactive_app/models/local/setting.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';

final List<ScenarioPreset> scenarioPresetsData = [
  ScenarioPreset(
    label: "Beginner",
    activeTime: 3,
    inActiveTime: 3,
    simultaneousTarget: 3,
    targetPresentation: 3,
  ),
  ScenarioPreset(
    label: "Intermediate",
    activeTime: 4,
    inActiveTime: 4,
    simultaneousTarget: 3,
    targetPresentation: 4,
  ),
  ScenarioPreset(
    label: "Advanced",
    activeTime: 5,
    inActiveTime: 5,
    simultaneousTarget: 3,
    targetPresentation: 5,
  ),
  ScenarioPreset(
    label: "Expert",
    activeTime: 6,
    inActiveTime: 6,
    simultaneousTarget: 3,//TODO: set max simultaneousTarget for each game.
    targetPresentation: 6,
  ),
];

final List<Game> gamesData = [
  Game(
    "Basic Combat",
    scenarioPresetsData,
  )
];

final Setting defaultSettings = Setting(
  minutesToPowerDown: 30,
  sensitivity: "low",
  sound: false,
);
