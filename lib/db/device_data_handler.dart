import 'package:trigger_interactive_app/ble/data/responses/discover_device_response.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';

abstract class DeviceDataHandler {
  static HubDevice selectedHub;

  DeviceDataHandler._();

  static List<BaseDevice> get connectedDevices =>
      selectedHub?.targets?.where((target) => target.isConnected)?.toList() ??
      [];

  static BaseDevice getDevice(String mac) {
    if (selectedHub.mac == mac) return selectedHub;

    return connectedDevices.firstWhere(
      (element) => element.mac == mac,
      orElse: () => null,
    );
  }

  static int createSTAId(DeviceType type) {
    final List<int> countList = connectedDevices
        .where((element) => element.type == type)
        .map((target) => target.id)
        .where((count) => count != null)
        .toList()
          ..sort();

    int indexMissing = 1;
    for (final int count in countList) {
      if (indexMissing != count) break;

      indexMissing++;
    }
    return indexMissing;
  }
}
