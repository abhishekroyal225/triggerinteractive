import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trigger_interactive_app/http/json_helper.dart';
import 'package:trigger_interactive_app/models/local/game/game.dart';
import 'package:trigger_interactive_app/models/local/setting.dart';
import 'package:trigger_interactive_app/models/local/user_data.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';

class LocalStorageManager {
  LocalStorageManager._();

  static Future<List<Game>> get defaultGames async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String jsonString = pref.getString("default_games");
    final Map<String, dynamic> json = await _parseJson(jsonString);
    final List gamesList = json["games"] as List;

    return gamesList
        .map((game) => Game.fromJson(game as Map<String, dynamic>))
        .toList();
  }

  static Future<void> setDefaultGames(
    List<Game> games, {
    bool overwrite = true,
  }) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();

    final String game = pref.getString("default_games");

    //if default_game exists and if it should not overwrite
    if (game != null && game.isNotEmpty && !overwrite) return;

    final Map<String, dynamic> jsonString = {
      "games": JsonListHelpers.toJson(games)
    };

    await pref.setString("default_games", jsonEncode(jsonString));
  }

  static Future<List<Game>> get allGames async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String jsonString = pref.getString("games_list");
    final Map<String, dynamic> json = await _parseJson(jsonString);

    if (json == null) return [];

    final List gamesList = json["games"] as List;

    return gamesList
            ?.map((game) => Game.fromJson(game as Map<String, dynamic>))
            ?.toList() ??
        [];
  }

  static Future<void> setAllGames(
    List<Game> games, {
    bool overwrite = true,
  }) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();

    final String game = pref.getString("games_list");

    //if default_game exists and if it should not overwrite
    if (game != null && game.isNotEmpty && !overwrite) return;

    final Map<String, dynamic> jsonString = {
      "games": JsonListHelpers.toJson(games)
    };

    await pref.setString("games_list", jsonEncode(jsonString));
  }

  static Future<Setting> get defaultSettings async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String jsonString = pref.getString("default_settings");
    final Map<String, dynamic> json = await _parseJson(jsonString);

    return Setting.fromJson(json);
  }

  static Future<void> setDefaulSettings(Setting defaultSettings) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();

    await pref.setString(
      "default_settings",
      jsonEncode(defaultSettings.toJson()),
    );
  }

  static Future<List<UserData>> get getAllUsers async {
    final SharedPreferences pref = await SharedPreferences.getInstance();

    final String jsonString = pref.getString("users");
    final Map<String, dynamic> json = await _parseJson(jsonString);

    if (json == null) return [];

    final List usersList = json["users"] as List;

    if (usersList == null) return [];

    return usersList
        .map((user) => UserData.fromJson(user as Map<String, dynamic>))
        .toList();
  }

  static Future<void> setAllUsers(List<UserData> usersList) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();

    final Map<String, dynamic> jsonString = {
      "users": JsonListHelpers.toJson(usersList)
    };

    await pref.setString("users", jsonEncode(jsonString));
  }

  static Future<String> get lastUserId async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String lastUserId = pref.getString("last_user_id");

    return lastUserId;
  }

  static Future<void> setLastUserId(UserData user) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("last_user_id", user.userId);
  }

  static Future<String> getPreviousSelectedScenarioKey() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String prevScenario = pref.getString("prev_selected_scenario_key");
    return prevScenario;
  }

  static Future<void> setPreviousSelectedScenarioKey(
    String scenarioPresetKey,
  ) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("prev_selected_scenario_key", scenarioPresetKey);
  }

  static Future<ScenarioPreset> getUnSavedScenarioSettings() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    final String unSavedSettings = pref.getString("unsaved_scenario_settings");

    if (unSavedSettings?.isEmpty ?? true) return null;

    final Map<String, dynamic> map = await _parseJson(unSavedSettings);
    return ScenarioPreset.fromJson(map);
  }

  static Future<void> retainUnSavedScenarioSettings(
    ScenarioPreset unSavedSettings,
  ) async {
    if (unSavedSettings == null) return;

    final SharedPreferences pref = await SharedPreferences.getInstance();

    final String unSavedettingString = jsonEncode(unSavedSettings.toJson());
    await pref.setString("unsaved_scenario_settings", unSavedettingString);
  }

  static Future<void> clearUnSavedScenarioSettings() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.remove("unsaved_scenario_settings");
  }

  static Future<Map<String, dynamic>> _parseJson(String text) {
    return compute(_parseAndDecode, text);
  }

  static Map<String, dynamic> _parseAndDecode(String response) {
    if (response == null) return null;

    return jsonDecode(response) as Map<String, dynamic>;
  }
}
