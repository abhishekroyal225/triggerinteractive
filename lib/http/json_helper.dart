class JsonListHelpers {
  static List<Map<String, dynamic>> toJson(List items) {
    if (items != null && items.isNotEmpty) {
      final List<Map<String, dynamic>> list = [];

      list.addAll(items.map((e) => e.toJson() as Map<String, dynamic>));

      return list;
    }
    return [];
  }
}
