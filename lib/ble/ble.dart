import 'dart:async';
import 'dart:core';

import 'package:app_settings/app_settings.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';
import 'package:trigger_interactive_app/utils/constants.dart';

class BLE {
  final BleManager _bleManager = BleManager();
  bool _initialized = false;
  // final List<String> filters = [" "];

  static BLE _singleton;
  factory BLE() => _singleton ??= BLE._internal();
  BLE._internal();

  Future<void> initialize() async {
    if (_initialized) return;
    try {
      await _bleManager.createClient();
      _initialized = true;
    } finally {}
  }

  Future<bool> get isBluetoothAvailable async {
    final BluetoothState state = await _bleManager.bluetoothState();

    return state != BluetoothState.UNSUPPORTED;
  }

  Future<bool> get isBluetoothOn async {
    final BluetoothState state = await _bleManager.bluetoothState();

    return state == BluetoothState.POWERED_ON;
  }

  Future<bool> turnBluetoothOn() async {
    await _bleManager.enableRadio();
    return true;
  }

  Future<void> stopScan() async {
    await _bleManager.stopPeripheralScan();
  }

  Stream<HubDevice> get getAvailableDevices async* {
    try {
      final Stream<ScanResult> devices = _bleManager.startPeripheralScan(
        scanMode: ScanMode.lowLatency,
        uuids: [],
      );

      await for (final device in devices) {
        if (device.peripheral.name != null) {
          yield HubDevice(peripheral: device.peripheral, targets: []);
        }
      }
    } finally {}
  }

  Future<List<HubDevice>> getPairedDevices() async {
    final List<Peripheral> devices = [];
    try {
      devices.addAll(
        await _bleManager.connectedPeripherals([Constants.SERVICE_UUID]),
      );
    } finally {}
    return devices
        .map(
          (peripheral) => HubDevice(
            peripheral: peripheral,
            targets: [],
          ),
        )
        .toList();
  }

  Future<bool> get isLocationServicesEnabled async {
    final ServiceStatus serviceStatus = await Permission.location.serviceStatus;
    return serviceStatus.isEnabled;
  }

  Future<bool> get isLocationEnabled async {
    final PermissionStatus permissionStatus = await Permission.location.status;
    return permissionStatus.isGranted;
  }

  Future<bool> enableLocationPermission() async {
    final PermissionStatus newStatus = await Permission.location.request();
    return newStatus.isGranted;
  }

  Future<bool> enableLocationService() async {
    await AppSettings.openLocationSettings();
    final bool isEnabled = await isLocationServicesEnabled;
    return isEnabled;
  }

  void dispose() {
    _bleManager?.destroyClient();
  }
}
