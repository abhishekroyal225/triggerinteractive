import 'dart:typed_data';

import 'package:trigger_interactive_app/ble/data/responses/device_info_response.dart';
import 'package:trigger_interactive_app/ble/data/responses/discover_device_response.dart';
import 'package:trigger_interactive_app/ble/data/responses/game_event_response.dart';
import 'package:trigger_interactive_app/ble/data/responses/general_status_response.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/log_file_helper.dart';

import 'responses/error_notification_response.dart';

class BLEDataParser {
  String _combinedHex = "";

  BLEData parseData(Uint8List _byteData) {
    try {
      final String hexString = String.fromCharCodes(_byteData);

      if (hexString.isEmpty) return BLEData();

      _combinedHex += hexString;

      if (DebugMode.isInDebugMode) {
        LogFileHelper.write(hexString);
      }

      final String tempHex = _combinedHex;

      if (!_combinedHex.contains("\r\n")) {
        return BLEData();
      } else {
        final List<String> splittedHex = _combinedHex.split("\r\n");
        //Stores the rest of the hex to [_combinedHex]
        _combinedHex = splittedHex.sublist(1).join("\r\n");
      }

      final String mac = tempHex.substring(0, 16);
      final String type = tempHex.substring(16, 18);
      final String data = tempHex.substring(18);

      _combinedHex = "";

      switch (type) {
        case "03":
          return BLEData<DiscoverDeviceResponse>(
            type: BLEDataType.discover_response,
            data: DiscoverDeviceResponse.parse(mac, data),
          );
        case "05":
          return BLEData<GeneralStatusResponse>(
            type: BLEDataType.general_status,
            data: GeneralStatusResponse.parse(mac, data),
          );
        case "0b":
          return BLEData<GameEventResponse>(
            type: BLEDataType.game_event,
            data: GameEventResponse.parse(mac, data),
          );
        case "21":
          return BLEData<ErrorNotificationResponse>(
            type: BLEDataType.error_notification,
            data: ErrorNotificationResponse.parse(mac, data),
          );
        case "25":
          return BLEData<DeviceInfoResponse>(
            type: BLEDataType.device_info_response,
            data: DeviceInfoResponse.parse(mac, data),
          );
        default:
          return BLEData();
      }
    } catch (e) {
      return BLEData();
    }
  }

  ///Converts hexadecimal to decimal using little endian
  static int hexToInt(String hex) {
    final List<String> hexList = [];
    for (int i = 0; i + 2 <= hex.length; i += 2) {
      hexList.add(hex.substring(i, i + 2));
    }

    final List<int> bytes =
        hexList.map((e) => int.parse(e, radix: 16)).toList();

    if (bytes.length <= 1) {
      return bytes.first;
    }

    final Uint8List byteList = Uint8List.fromList(bytes);

    final ByteData byteData = ByteData.sublistView(byteList);
    final int value = byteData.getUint16(0, Endian.little);
    return value;
  }
}

class BLEData<T> {
  BLEDataType type;
  T data;

  BLEData({this.type = BLEDataType.none, this.data});

  @override
  String toString() => 'BLEData(data: $data)';
}

enum BLEDataType {
  discover_response,
  general_status,
  game_event,
  device_info_response,
  error_notification,
  none,
}
