import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';

class GameEventResponse {
  final EventStatus eventStatus;
  final int currentActiveTime;
  final int hitDetected;
  final int acceleration;
  final int accelerationOnDecimal;
  final int acquisitionTime;
  final int reacquisitionTime1;
  final int reacquisitionTime2;
  final String mac;

  GameEventResponse({
    this.eventStatus,
    this.currentActiveTime,
    this.hitDetected,
    this.acceleration,
    this.accelerationOnDecimal,
    this.acquisitionTime,
    this.reacquisitionTime1,
    this.reacquisitionTime2,
    this.mac,
  });

  factory GameEventResponse.parse(String mac, String hex) {
    final String eventStatusHex = hex.substring(0, 2);
    final String currentActiveTimeHex = hex.substring(2, 6);
    final String hitDetected = hex.substring(6, 8);
    final String accelerationHex = hex.substring(8, 10);
    final String accelerationDecimalHex = hex.substring(10, 12);
    final String acquisitionTimeHex = hex.substring(12, 16);
    final String reacquisitionTime1Hex = hex.substring(16, 20);
    final String reacquisitionTime2Hex = hex.substring(20, 22).padLeft(4, "0");

    return GameEventResponse(
      eventStatus: _getEventStatus(BLEDataParser.hexToInt(eventStatusHex)),
      currentActiveTime: BLEDataParser.hexToInt(currentActiveTimeHex),
      hitDetected: BLEDataParser.hexToInt(hitDetected),
      acceleration: BLEDataParser.hexToInt(accelerationHex),
      accelerationOnDecimal: BLEDataParser.hexToInt(accelerationDecimalHex),
      acquisitionTime: BLEDataParser.hexToInt(acquisitionTimeHex),
      reacquisitionTime1: BLEDataParser.hexToInt(reacquisitionTime1Hex),
      reacquisitionTime2: BLEDataParser.hexToInt(reacquisitionTime2Hex),
      mac: mac,
    );
  }

  static EventStatus _getEventStatus(int val) {
    switch (val) {
      case 0:
        return EventStatus.progress;
        break;
      case 1:
        return EventStatus.completed;
        break;
      case 2:
        return EventStatus.timed_out;
      default:
        return null;
    }
  }
}

enum EventStatus {
  progress,
  completed,
  timed_out,
}
