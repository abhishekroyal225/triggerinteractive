import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';

class GeneralStatusResponse {
  final String mac;
  final int battery;
  final int signal;
  final int idleTime;
  final String connectedMac;
  final bool highTemperature;

  GeneralStatusResponse({
    this.mac,
    this.battery,
    this.signal,
    this.idleTime,
    this.connectedMac,
    this.highTemperature = false,
  });

  factory GeneralStatusResponse.parse(String mac, String hex) {
    final batteryHex = hex.substring(0, 2);
    final signalHex = hex.substring(2, 4);
    final idleTime = hex.substring(4, 12);
    final connectedMac = hex.substring(12, 28);
    final temperatureHex = hex.substring(28, 30);

    return GeneralStatusResponse(
      mac: mac,
      battery: BLEDataParser.hexToInt(batteryHex),
      signal: BLEDataParser.hexToInt(signalHex),
      idleTime: BLEDataParser.hexToInt(idleTime),
      connectedMac: connectedMac,
      highTemperature: BLEDataParser.hexToInt(temperatureHex) != 0,
    );
  }
}
