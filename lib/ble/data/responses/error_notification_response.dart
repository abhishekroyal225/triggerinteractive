import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';

class ErrorNotificationResponse {
  final DeviceErrorType error;
  final String mac;

  ErrorNotificationResponse({this.error, this.mac});

  factory ErrorNotificationResponse.parse(String mac, String hex) {
    final String errorHex = hex.substring(0, 2);
    final int errorType = BLEDataParser.hexToInt(errorHex);

    return ErrorNotificationResponse(error: _getErrorType(errorType), mac: mac);
  }

  static DeviceErrorType _getErrorType(int errorType) {
    if (errorType < 1 || errorType > 12) return null;

    return DeviceErrorType.values[errorType - 1];
  }
}

enum DeviceErrorType {
  invalid_packet_app,
  invalid_packet_end_device,
  no_response_end_device,
  packet_not_valid_hub,
  packet_not_valid_end_device,
  error_transmitting_end_device,
  end_device_claimed,
  game_control_request_not_valid,
  low_battery_warning,
  low_battery_poweroff,
  high_temperature_warning,
  high_temperature_poweroff
}
