import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';

class DeviceInfoResponse {
  final String mac;
  final int builtHWVersion;
  final int digiFWVersion;
  final int triggerFWMajorVersion;
  final int triggerFWMinorVersion;

  DeviceInfoResponse({
    this.mac,
    this.builtHWVersion,
    this.digiFWVersion,
    this.triggerFWMajorVersion,
    this.triggerFWMinorVersion,
  });

  factory DeviceInfoResponse.parse(String mac, String hex) {
    final builtHWVersionHex = hex.substring(0, 2);
    final digiFWVersion = hex.substring(2, 6);
    final triggerFWMajorVersionHex = hex.substring(6, 8);
    final triggerFWMinorVersionHex = hex.substring(8, 10);

    return DeviceInfoResponse(
      mac: mac,
      builtHWVersion: BLEDataParser.hexToInt(builtHWVersionHex),
      digiFWVersion: BLEDataParser.hexToInt(digiFWVersion),
      triggerFWMajorVersion: BLEDataParser.hexToInt(triggerFWMajorVersionHex),
      triggerFWMinorVersion: BLEDataParser.hexToInt(triggerFWMinorVersionHex),
    );
  }
}
