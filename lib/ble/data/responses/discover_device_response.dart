import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';

class DiscoverDeviceResponse {
  final String mac;
  final bool isFinal;
  final DeviceType deviceType;

  DiscoverDeviceResponse({
    this.mac,
    this.isFinal,
    this.deviceType,
  });

  factory DiscoverDeviceResponse.parse(String mac, String hex) {
    final int finalValue = BLEDataParser.hexToInt(hex.substring(0, 2));
    final String deviceTypeHex = hex.substring(2, 4);
    return DiscoverDeviceResponse(
      mac: mac,
      isFinal: finalValue != 0,
      deviceType: _getDeviceType(deviceTypeHex),
    );
  }

  static DeviceType _getDeviceType(String deviceTypeHex) {
    final val = BLEDataParser.hexToInt(deviceTypeHex);
    switch (val) {
      case 1:
        return DeviceType.hub;
      case 2:
        return DeviceType.sta;
      case 3:
        return DeviceType.rpt;
    }
    return null;
  }
}

enum DeviceType { hub, sta, rpt }
