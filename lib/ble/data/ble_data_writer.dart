import 'dart:math';

import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/bloc/scenario_progress_bloc.dart';

mixin BLEDataWriter on BaseDevice {
  Peripheral get peripheral;

  @override
  String get mac;

  Future<void> write(String hex);

  Future<void> requestDiscovery([int times = 1]) async {
    const String tempMac = "0000000000000000";

    final List<String> hex = [];
    for (int i = 0; i < times; i++) {
      hex.add("${tempMac}02");
    }

    await write(hex.join("\r\n"));
  }

  Future<void> requestGeneralStatus([Iterable<String> targetMacList]) async {
    final List<String> hexBuffer = [];

    if (targetMacList == null) {
      if (mac == null) return;
      hexBuffer.add("${mac}04");
    }

    for (final String targetMac in targetMacList ?? []) {
      if (targetMac != null) hexBuffer.add("${targetMac}04");
    }

    await write(hexBuffer.join("\r\n"));
  }

  Future<void> updateSettings(int idleTime, String macAddress) async {
    final String idleTimeString = toHex(idleTime * 1000 * 1000, ByteSize.u32);
    final String hex = "${macAddress}06$idleTimeString";

    await write(hex);
  }

  Future<void> requestGameRequest(
      ScenarioPreset preset, List<TargetEvent> targetEventList) async {
    if (targetEventList.isEmpty) return;

    final List<String> hexBuffer = [];

    for (final TargetEvent target in targetEventList) {
      final String inactiveTimeHex =
          toHex(preset.inActiveTime * 1000, ByteSize.u16);

      final String activeTimeHex =
          toHex(preset.activeTime * 1000, ByteSize.u16);

      //For now hitsRequired is set to be 1 as request by the client
      final String hitsRequired = toHex(1);

      //sensitivity ranges from 0 - 255
      final String sensitivity =
          toHex(UserDataHandler.settings.sensitivityValue);

      final String event = toHex(target.eventId);
      hexBuffer.add(
        "${target.targetMac}0a$inactiveTimeHex$activeTimeHex$hitsRequired$sensitivity$event",
      );
    }

    await write(hexBuffer.join("\r\n"));
  }

  Future<void> identifyRequest([String targetMac]) async {
    final String hex = "${targetMac ?? mac}08";
    await write(hex);
  }

  Future<void> requestGameControl(GameControls control,
      [String targetMac]) async {
    const String tempMac = "0000000000000000";

    final String controlHex = toHex(GameControls.values.indexOf(control));
    final String hex = "${targetMac ?? tempMac}0c$controlHex";
    await write(hex);
  }

  Future<void> claimRequest(String macAddress, [bool claim = true]) async {
    final String hex = "${macAddress}0e${claim ? "01" : "00"}";
    await write(hex);
  }

  Future<void> requestDeviceInfo(String macAddress) async {
    final String hex = "${macAddress}24";
    await write(hex);
  }

  static String toHex(int val, [ByteSize byteSize = ByteSize.byte]) {
    final int sizePerByte = ByteSize.values.indexOf(byteSize) + 1;
    final int charSize = pow(2, sizePerByte).toInt();

    final String hex = val.toRadixString(16).padLeft(charSize, "0");

    //convert to little endian
    final List<String> hexList = [];
    for (int i = 0; i + 2 <= hex.length; i += 2) {
      hexList.add(hex.substring(i, i + 2));
    }

    return hexList.reversed.join("");
  }
}

enum GameControls {
  start,
  stop,
  pause,
  clear,
}

enum ByteSize { byte, u16, u32, u64 }
