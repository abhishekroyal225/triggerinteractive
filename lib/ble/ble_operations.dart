import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/utils/constants.dart';

mixin BLEOperations on BaseDevice {
  Peripheral get peripheral;

  final StreamController<bool> _connectionStreamController = StreamController();
  Stream<bool> _connectionStreamData;

  final StreamController<BLEData> _dataStreamController =
      StreamController.broadcast();

  Stream<BLEData> _dataStream;

  final StreamController<BLEData> _eventDataStreamController =
      StreamController.broadcast();

  StreamSubscription<CharacteristicWithValue> _eventDataSub;
  Stream<BLEData> _eventDataStream;

  final StreamController<int> _signalStreamController =
      StreamController.broadcast();

  Stream<int> _signalStream;

  Stream<bool> get connectionStateStream {
    try {
      _connectionStreamData ??=
          _connectionStreamController.stream.asBroadcastStream();

      if (peripheral != null) {
        final deviceConnectionSub = peripheral.observeConnectionState(
          emitCurrentValue: true,
        );

        deviceConnectionSub.listen((event) {
          _connectionStreamController.sink.add(
            event == PeripheralConnectionState.connected,
          );
        });
      }
    } catch (e) {/* */}

    return _connectionStreamData;
  }

  Stream<int> get signalStream {
    try {
      if (_signalStream == null) {
        _signalStream = _signalStreamController.stream.asBroadcastStream();

        Timer.periodic(const Duration(seconds: 2), (timer) async {
          int rssiValue;
          try {
            rssiValue =
                await peripheral?.rssi(transactionId: "1")?.catchError((_) {});
          } on PlatformException catch (_) {/* */}
          if (rssiValue != null) _signalStreamController.sink.add(rssiValue);
        });
      }
    } catch (e) {/* */}
    return _signalStream;
  }

  Stream<BLEData> get dataStream {
    try {
      final BLEDataParser parser = BLEDataParser();
      _dataStream ??= _dataStreamController.stream.asBroadcastStream();
      peripheral
          .monitorCharacteristic(
        Constants.SERVICE_UUID,
        Constants.READ_CHARACTERISTICS_UUID,
      )
          .listen((event) {
        _dataStreamController.sink.add(parser.parseData(event.value));
      });
    } finally {}

    return _dataStream;
  }

  Stream<BLEData> get eventDataStream {
    try {
      _eventDataSub?.cancel();
      final BLEDataParser parser = BLEDataParser();
      _eventDataStream ??=
          _eventDataStreamController.stream.asBroadcastStream();
      _eventDataSub = peripheral
          .monitorCharacteristic(
        Constants.SERVICE_UUID,
        Constants.READ_CHARACTERISTICS_UUID,
      )
          .listen((event) {
        final BLEData data = parser.parseData(event.value);
        if (data.type == BLEDataType.game_event) {
          _eventDataStreamController.sink.add(data);
        }
      });
    } finally {}

    return _eventDataStream;
  }

  Future<void> connect() async {
    try {
      if (await peripheral.isConnected()) {
        await peripheral.disconnectOrCancelConnection();
      }
      await peripheral.connect(requestMtu: 250);
      await peripheral.discoverAllServicesAndCharacteristics();
    } catch (e) {/* */}
  }

  Future<void> disconnect() async {
    try {
      if (await peripheral.isConnected()) {
        await peripheral.disconnectOrCancelConnection();
      }
    } catch (e) {/* */}
  }

  Future<bool> isConnectedBLE() async {
    return peripheral.isConnected();
  }

  Future<void> write(String hex) async {
    try {
      await peripheral.writeCharacteristic(
        Constants.SERVICE_UUID,
        Constants.WRITE_CHARACTERISTICS_UUID,
        Uint8List.fromList("$hex\r\n".codeUnits),
        false,
      );
    } finally {}
  }

  void dispose() {
    _connectionStreamController?.close();
    _dataStreamController?.close();
  }
}
