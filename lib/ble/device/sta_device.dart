import 'package:trigger_interactive_app/ble/data/responses/discover_device_response.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';

class StaDevice extends BaseDevice {
  StaDevice({
    int id,
    int batteryPercentage,
    int signalStrength,
    bool isConnected = false,
    String mac,
    bool highTemperature = false,
    bool updateAvailable = false,
    bool highTemperaturePowerOff = false,
    bool lowBatteryPowerOff = false,
    int updateVerion,
  }) : super(
          id: id,
          batteryPercentage: batteryPercentage,
          signalStrength: signalStrength,
          isConnected: isConnected,
          mac: mac,
          highTemperature: highTemperature,
          updateAvailable: updateAvailable,
          hightTemperaturePowerOff: highTemperaturePowerOff,
          lowBatteryPowerOff: lowBatteryPowerOff,
          updateVersion: updateVerion,
        );

  @override
  String get deviceName => "STA${id?.toString()?.padLeft(2, "0") ?? ""}";

  @override
  DeviceType get type => DeviceType.sta;
}
