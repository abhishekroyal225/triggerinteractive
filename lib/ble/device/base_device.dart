import 'package:trigger_interactive_app/ble/data/responses/discover_device_response.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';
import 'package:trigger_interactive_app/ble/device/rpt_device.dart';
import 'package:trigger_interactive_app/ble/device/sta_device.dart';

abstract class BaseDevice implements Comparable<BaseDevice> {
  int id;
  int batteryPercentage;
  int signalStrength;
  bool isConnected;
  bool highTemperature;
  bool hightTemperaturePowerOff;
  bool lowBatteryPowerOff;
  bool updateAvailable;
  int updateVersion;

  String mac;
  bool isUnavailable;

  BaseDevice({
    this.id,
    this.isConnected = false,
    this.batteryPercentage,
    this.signalStrength,
    this.mac,
    this.highTemperature = false,
    this.hightTemperaturePowerOff = false,
    this.lowBatteryPowerOff = false,
    this.isUnavailable = false,
    this.updateAvailable = false,
    this.updateVersion,
  });

  BaseDevice asDevice(DeviceType type) {
    BaseDevice base;
    switch (type) {
      case DeviceType.hub:
        base = HubDevice();
        break;
      case DeviceType.sta:
        base = StaDevice();
        break;
      case DeviceType.rpt:
        base = RptDevice();
        break;
    }

    base.id = id;
    base.isConnected = isConnected;
    base.batteryPercentage = batteryPercentage;
    base.signalStrength = signalStrength;
    base.mac = mac;
    base.isUnavailable = isUnavailable;
    base.hightTemperaturePowerOff = hightTemperaturePowerOff;
    base.lowBatteryPowerOff = lowBatteryPowerOff;
    base.updateVersion = updateVersion;

    return base;
  }

  @override
  int compareTo(BaseDevice obj) {
    final String first = _comparatorOperation(
      obj.isConnected,
      runtimeType,
      obj.signalStrength ?? 0,
    );

    final String second = _comparatorOperation(
      isConnected,
      obj.runtimeType,
      signalStrength ?? 0,
    );

    return first.compareTo(second);
  }

  String _comparatorOperation(
    bool connected,
    Type runtime,
    int signal,
  ) =>
      "$connected $runtime ${signal.toString().padLeft(3, "0")}";

  String get deviceName => "";

  DeviceType get type;

  @override
  String toString() {
    return 'BaseDevice(id: $id, batteryPercentage: $batteryPercentage, signalStrength: $signalStrength, isConnected: $isConnected, temperature: $highTemperature, mac: $mac, type: $runtimeType)';
  }
}

enum DeviceStatus {
  available,
  lost,
}
