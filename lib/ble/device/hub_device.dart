import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:trigger_interactive_app/ble/ble_operations.dart';
import 'package:trigger_interactive_app/ble/data/ble_data_writer.dart';
import 'package:trigger_interactive_app/ble/data/responses/discover_device_response.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';

class HubDevice extends BaseDevice with BLEDataWriter, BLEOperations {
  final List<BaseDevice> targets;

  @override
  Peripheral peripheral;

  HubDevice({
    int id,
    int batteryPercentage,
    int signalStrength,
    bool isConnected = false,
    String mac,
    bool highTemperature = false,
    bool updateAvailable = false,
    bool highTemperaturePowerOff = false,
    bool lowBatteryPowerOff = false,
    int updateVersion,
    this.targets,
    this.peripheral,
  }) : super(
          id: id,
          batteryPercentage: batteryPercentage,
          signalStrength: signalStrength,
          isConnected: isConnected,
          mac: mac,
          highTemperature: highTemperature,
          updateAvailable: updateAvailable,
          hightTemperaturePowerOff: highTemperaturePowerOff,
          lowBatteryPowerOff: lowBatteryPowerOff,
          updateVersion: updateVersion,
        );

  @override
  String get deviceName => peripheral.name;

  @override
  DeviceType get type => DeviceType.hub;
}
