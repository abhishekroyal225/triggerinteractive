import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends BaseEquatable {
  @JsonKey(name: 'id')
  final int userId;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'name')
  final String name;

  User(this.userId, this.email, this.name);

  factory User.fromJson(Map<String, dynamic> map) => _$UserFromJson(map);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}
