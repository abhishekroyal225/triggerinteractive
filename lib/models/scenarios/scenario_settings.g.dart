// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scenario_settings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScenarioSettings _$ScenarioSettingsFromJson(Map<String, dynamic> json) {
  return ScenarioSettings(
    label: json['label'] as String,
    level: json['level'] as int,
    min: json['min'] as int,
    max: json['max'] as int,
  );
}

Map<String, dynamic> _$ScenarioSettingsToJson(ScenarioSettings instance) =>
    <String, dynamic>{
      'label': instance.label,
      'level': instance.level,
      'min': instance.min,
      'max': instance.max,
    };
