import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_settings.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'scenario_preset.g.dart';

@JsonSerializable()
// ignore: must_be_immutable
class ScenarioPreset {
  @JsonKey(name: "label")
  String label;

  @JsonKey(name: "active_time")
  int activeTime;

  @JsonKey(name: "in_active_time")
  int inActiveTime;

  @JsonKey(name: "simultaneous_target")
  int simultaneousTarget;

  @JsonKey(name: "target_presentation")
  int targetPresentation;

  ScenarioPreset({
    this.label,
    this.activeTime = 0,
    this.inActiveTime = 0,
    this.simultaneousTarget = 0,
    this.targetPresentation = 0,
  });

  List<ScenarioSettings> getSettings() {
    return [
      ScenarioSettings(
        min: 1,
        max: 10,
        level: activeTime,
        label: StringResource.ACTIVE_TIME,
      ),
      ScenarioSettings(
        min: 0,
        max: 10,
        level: inActiveTime,
        label: StringResource.INACTIVE_TIME,
      ),
      ScenarioSettings(
        min: 1,
        max: 3,
        level: simultaneousTarget,
        label: StringResource.SIMULTANEOUS_TARGETS,
      ),
      ScenarioSettings(
        min: 1,
        max: 20,
        level: targetPresentation,
        label: StringResource.TARGET_PRESENTATIONS,
      )
    ];
  }

  Map<String, dynamic> toJson() => _$ScenarioPresetToJson(this);

  factory ScenarioPreset.fromJson(Map<String, dynamic> json) =>
      _$ScenarioPresetFromJson(json);
}
