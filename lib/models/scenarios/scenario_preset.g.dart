// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scenario_preset.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScenarioPreset _$ScenarioPresetFromJson(Map<String, dynamic> json) {
  return ScenarioPreset(
    label: json['label'] as String,
    activeTime: json['active_time'] as int,
    inActiveTime: json['in_active_time'] as int,
    simultaneousTarget: json['simultaneous_target'] as int,
    targetPresentation: json['target_presentation'] as int,
  );
}

Map<String, dynamic> _$ScenarioPresetToJson(ScenarioPreset instance) =>
    <String, dynamic>{
      'label': instance.label,
      'active_time': instance.activeTime,
      'in_active_time': instance.inActiveTime,
      'simultaneous_target': instance.simultaneousTarget,
      'target_presentation': instance.targetPresentation,
    };
