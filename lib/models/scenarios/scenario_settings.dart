import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'scenario_settings.g.dart';

@JsonSerializable()
// ignore: must_be_immutable
class ScenarioSettings extends BaseEquatable {
  @JsonKey(name: "label")
  String label;

  @JsonKey(name: "level")
  int level;

  @JsonKey(name: "min")
  int min;

  @JsonKey(name: "max")
  int max;

  ScenarioSettings({this.label, this.level, this.min, this.max}) {
    level ??= min;
  }

  ScenarioSettings copyWith({
    String label,
    int level,
    int min,
    int max,
  }) {
    return ScenarioSettings(
      label: label ?? this.label,
      level: level ?? this.level,
      min: min ?? this.min,
      max: max ?? this.max,
    );
  }

  Map<String, dynamic> toJson() => _$ScenarioSettingsToJson(this);

  ScenarioSettings fromJson(Map<String, dynamic> json) =>
      _$ScenarioSettingsFromJson(json);
}
