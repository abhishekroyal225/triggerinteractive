import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/models/local/game/session.dart';
import 'package:trigger_interactive_app/models/local/setting.dart';
import 'package:trigger_interactive_app/screens/score_board/bloc/score_board_bloc.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:uuid/uuid.dart';

part 'user_data.g.dart';

@JsonSerializable()
// ignore: must_be_immutable
class UserData extends BaseEquatable {
  @JsonKey(name: "user_id")
  String userId;

  @JsonKey(name: "user_name")
  String userName;

  @JsonKey(name: "settings")
  Setting settings;

  @JsonKey(name: "sessions")
  List<Session> sessions;

  UserData({
    this.userId,
    this.userName,
    this.settings,
    this.sessions,
  }) {
    userId ??= const Uuid().v1();
  }

  @override
  List<Object> get props => [userId];

  ScoreBoardData scoreData([bool isMonth = false]) {
    Iterable<double> scores;
    if (isMonth) {
      scores = sessions.where((session) {
        final DateTime currentDate = DateTime.now();
        final DateTime sessionDate = DateTime.parse(session.createdAt);
        return currentDate.month == sessionDate.month &&
            currentDate.year == sessionDate.year;
      }).map((e) => e.score);
    } else {
      scores = sessions?.map((e) => e.score);
    }

    if (scores?.isEmpty ?? true) {
      return ScoreBoardData(this, 0);
    }

    final double totalScore =
        scores.reduce((value, element) => value + element);

    return ScoreBoardData(this, totalScore);
  }

  factory UserData.fromJson(Map<String, dynamic> json) =>
      _$UserDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}
