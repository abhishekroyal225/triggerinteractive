import 'package:json_annotation/json_annotation.dart';

part 'setting.g.dart';

@JsonSerializable()
class Setting {
  @JsonKey(name: "sensitivity")
  String sensitivity;

  @JsonKey(name: "sound")
  bool sound;

  @JsonKey(name: "minutes_to_power_down")
  int minutesToPowerDown;

  Setting({this.sensitivity, this.sound, this.minutesToPowerDown});

  int get sensitivityValue {
    if (sensitivity == "low") {
      return 70;
    } else if (sensitivity == "medium") {
      return 100;
    } else {
      return 150;
    }
  }

  factory Setting.fromJson(Map<String, dynamic> json) =>
      _$SettingFromJson(json);
  Map<String, dynamic> toJson() => _$SettingToJson(this);

  Setting copyWith({
    String sensitivity,
    bool sound,
    int minutesToPowerDown,
  }) {
    return Setting(
      sensitivity: sensitivity ?? this.sensitivity,
      sound: sound ?? this.sound,
      minutesToPowerDown: minutesToPowerDown ?? this.minutesToPowerDown,
    );
  }
}
