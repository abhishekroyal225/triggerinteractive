import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/models/local/game/score_data.dart';

part 'session_data.g.dart';

@JsonSerializable()
class SessionData {
  @JsonKey(name: "targets_hit")
  int tartgetsHit;

  @JsonKey(name: "targets_miss")
  int targetsMiss;

  @JsonKey(name: "tartgets_total")
  int tartgetsTotal;

  @JsonKey(name: "reaction_time")
  int reactionTime;

  @JsonKey(name: "targets")
  List<ScoreData> targets;

  SessionData({
    this.tartgetsHit,
    this.targetsMiss,
    this.tartgetsTotal,
    this.reactionTime,
    this.targets,
  });

  factory SessionData.fromJson(Map<String, dynamic> json) => _$SessionDataFromJson(json);
  Map<String, dynamic> toJson() => _$SessionDataToJson(this);
}
