// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Session _$SessionFromJson(Map<String, dynamic> json) {
  return Session(
    sessionId: json['session_id'] as String,
    gameType: json['game_type'] as String,
    createdAt: json['created_at'] as String,
    score: (json['score'] as num)?.toDouble(),
    sessionData: json['session_data'] == null
        ? null
        : SessionData.fromJson(json['session_data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SessionToJson(Session instance) => <String, dynamic>{
      'session_id': instance.sessionId,
      'game_type': instance.gameType,
      'created_at': instance.createdAt,
      'score': instance.score,
      'session_data': instance.sessionData,
    };
