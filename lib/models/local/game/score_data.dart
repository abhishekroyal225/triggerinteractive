import 'package:json_annotation/json_annotation.dart';

part 'score_data.g.dart';

@JsonSerializable()
class ScoreData {
  @JsonKey(name: "order")
  int order;

  @JsonKey(name: "target")
  String target;

  @JsonKey(name: "split_time")
  double splitTime;

  @JsonKey(name: "isMissed")
  bool isMissed;

  ScoreData({
    this.order,
    this.target,
    this.splitTime,
    this.isMissed = false,
  });

  factory ScoreData.fromJson(Map<String, dynamic> json) =>
      _$ScoreDataFromJson(json);
  Map<String, dynamic> toJson() => _$ScoreDataToJson(this);
}
