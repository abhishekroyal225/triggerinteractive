// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Game _$GameFromJson(Map<String, dynamic> json) {
  return Game(
    json['game_type'] as String,
    (json['scenarios'] as List)
        ?.map((e) => e == null
            ? null
            : ScenarioPreset.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$GameToJson(Game instance) => <String, dynamic>{
      'game_type': instance.gameType,
      'scenarios': instance.scenarios,
    };
