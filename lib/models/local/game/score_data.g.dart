// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScoreData _$ScoreDataFromJson(Map<String, dynamic> json) {
  return ScoreData(
    order: json['order'] as int,
    target: json['target'] as String,
    splitTime: (json['split_time'] as num)?.toDouble(),
    isMissed: json['isMissed'] as bool,
  );
}

Map<String, dynamic> _$ScoreDataToJson(ScoreData instance) => <String, dynamic>{
      'order': instance.order,
      'target': instance.target,
      'split_time': instance.splitTime,
      'isMissed': instance.isMissed,
    };
