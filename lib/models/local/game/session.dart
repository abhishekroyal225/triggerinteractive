import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/models/local/game/session_data.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'session.g.dart';

@JsonSerializable()
// ignore: must_be_immutable
class Session extends BaseEquatable {
  @JsonKey(name: "session_id")
  String sessionId;

  @JsonKey(name: "game_type")
  String gameType;

  @JsonKey(name: "created_at")
  String createdAt;

  @JsonKey(name: "score")
  double score;

  @JsonKey(name: "session_data")
  SessionData sessionData;

  Session({
    this.sessionId,
    this.gameType,
    this.createdAt,
    this.score,
    this.sessionData,
  });

  factory Session.fromJson(Map<String, dynamic> json) =>
      _$SessionFromJson(json);
  Map<String, dynamic> toJson() => _$SessionToJson(this);

  @override
  List<Object> get props => [sessionId];
}
