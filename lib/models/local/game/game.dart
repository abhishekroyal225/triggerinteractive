import 'package:json_annotation/json_annotation.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'game.g.dart';

@JsonSerializable()
// ignore: must_be_immutable
class Game extends BaseEquatable {
  @JsonKey(name: "game_type")
  String gameType;

  @JsonKey(name: "scenarios")
  List<ScenarioPreset> scenarios;

  Game(this.gameType, this.scenarios);

  factory Game.fromJson(Map<String, dynamic> json) => _$GameFromJson(json);
  Map<String, dynamic> toJson() => _$GameToJson(this);

  @override
  List<Object> get props => [gameType];
}
