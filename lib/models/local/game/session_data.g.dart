// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SessionData _$SessionDataFromJson(Map<String, dynamic> json) {
  return SessionData(
    tartgetsHit: json['targets_hit'] as int,
    targetsMiss: json['targets_miss'] as int,
    tartgetsTotal: json['tartgets_total'] as int,
    reactionTime: json['reaction_time'] as int,
    targets: (json['targets'] as List)
        ?.map((e) =>
            e == null ? null : ScoreData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SessionDataToJson(SessionData instance) =>
    <String, dynamic>{
      'targets_hit': instance.tartgetsHit,
      'targets_miss': instance.targetsMiss,
      'tartgets_total': instance.tartgetsTotal,
      'reaction_time': instance.reactionTime,
      'targets': instance.targets,
    };
