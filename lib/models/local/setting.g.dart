// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Setting _$SettingFromJson(Map<String, dynamic> json) {
  return Setting(
    sensitivity: json['sensitivity'] as String,
    sound: json['sound'] as bool,
    minutesToPowerDown: json['minutes_to_power_down'] as int,
  );
}

Map<String, dynamic> _$SettingToJson(Setting instance) => <String, dynamic>{
      'sensitivity': instance.sensitivity,
      'sound': instance.sound,
      'minutes_to_power_down': instance.minutesToPowerDown,
    };
