import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';

class CustomScaffold extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget body;
  final Color color;
  final bool isBottomReSize;
  final bool fullScreen;
  final Color statusColor;
  final Widget bottomNavigationBar;
  final Widget floatingActionButton;

  const CustomScaffold({
    this.appBar,
    this.body,
    this.color = Colors.white,
    this.isBottomReSize = true,
    this.fullScreen = false,
    this.statusColor = Colors.black,
    this.bottomNavigationBar,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        systemNavigationBarColor: ColorResource.BLACK_333333,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: isBottomReSize,
        backgroundColor: color,
        appBar: appBar,
        body: _buildBody(context),
        bottomNavigationBar: bottomNavigationBar,
        floatingActionButton: floatingActionButton,
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (fullScreen) {
      return _buildLoader(context);
    } else {
      return SafeArea(
        child: _buildLoader(context),
      );
    }
  }

  Widget _buildLoader(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: body,
    );
  }
}
