import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CustomBottomNav extends StatelessWidget {
  final void Function(int) onTap;
  final int selectedIndex;

  const CustomBottomNav({Key key, this.onTap, this.selectedIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      color: ColorResource.BLACK_333333,
      child: Row(
        children: [
          _buildBottomNavigationItemWidget(
            index: 0,
            label: StringResource.TRAIN,
            icon: ImageResource.TRAIN_NAV_ICON,
          ),
          _buildBottomNavigationItemWidget(
            index: 1,
            label: StringResource.DEVICES,
            icon: ImageResource.DEVICES_NAV_ICON,
          ),
          // _buildBottomNavigationItemWidget(
          //   index: 2,
          //   label: StringResource.STATS,
          //   icon: ImageResource.STATS_NAV_ICON,
          // ),
          // _buildBottomNavigationItemWidget(
          //   index: 3,
          //   label: StringResource.SCORES,
          //   icon: ImageResource.SCORE_BOARD_NAV_ICON,
          // ),
          _buildBottomNavigationItemWidget(
            index: 2,
            label: StringResource.SETTINGS,
            icon: ImageResource.SETTINGS_NAV_ICON,
          ),
        ],
      ),
    );
  }

  Widget _buildBottomNavigationItemWidget({
    int index,
    String label,
    String icon,
  }) {
    return Expanded(
      child: Tooltip(
        message: label,
        child: InkWell(
          onTap: () => onTap(index),
          child: Container(
            color: selectedIndex == index ? ColorResource.RED_96262C : null,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(height: 30, width: 53, child: SvgPicture.asset(icon)),
                // const SizedBox(height: 7),
                CustomText(
                  label,
                  textAlign: TextAlign.center,
                  fontSize: FontSize.mainNavFontSize,
                  color: ColorResource.WHITE_FFFFFF,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
