import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class DeviceDetailWidget extends StatelessWidget {
  final BaseDevice deviceDetail;
  final bool isHub;
  final bool isDisabled;

  ///Defaults to ble identify function in device
  final void Function() onIdentifyButtonPressed;
  final void Function(BaseDevice deviceDetail, {bool isHub})
      onConnectSwitchFlipped;

  const DeviceDetailWidget({
    @required this.deviceDetail,
    @required this.onConnectSwitchFlipped,
    this.onIdentifyButtonPressed,
    this.isHub = false,
    this.isDisabled = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      width: double.infinity,
      decoration: BoxDecoration(
        color: isHub ? ColorResource.WHITE_FFFFFF : null,
        boxShadow: [
          if (isHub)
            BoxShadow(
              blurRadius: 12,
              offset: const Offset(0, 4),
              color: ColorResource.BLACK_000000.withAlpha(27),
            ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 65,
            child: Row(
              children: [
                _buildUpdateFirmware(context),
                SizedBox(width: 5.w),
                _buildDeviceNameWidget(context),
                _buildDividerWidget(isDisabled: deviceDetail.isUnavailable),
                _buildSignalStrengthWidget(),
                _buildDividerWidget(isDisabled: deviceDetail.isUnavailable),
                _buildBatteryWidget(),
                _buildDividerWidget(isDisabled: deviceDetail.isUnavailable),
                _buildTemperatureWidget(),
                SizedBox(width: 5.w),
                Expanded(child: Container()),
                _buildIdentifyWidget(),
                _buildConnectSwitchWidget(),
              ],
            ),
          ),
          if (deviceDetail.updateVersion != null)
            Container(
              margin: const EdgeInsets.only(left: 40),
              child: CustomText(
                "v${deviceDetail.updateVersion}",
                fontSize: 14,
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildConnectSwitchWidget() {
    final Color inActiveColor = deviceDetail.isUnavailable
        ? ColorResource.GREY_969696.withOpacity(0.4)
        : ColorResource.GREY_969696;
    return SizedBox(
      width: 55,
      child: Switch(
        value: deviceDetail.isConnected,
        onChanged: (_) => isDisabled || deviceDetail.isUnavailable
            ? null
            : onConnectSwitchFlipped(deviceDetail, isHub: isHub),
        activeColor: ColorResource.WHITE_FFFFFF,
        activeTrackColor: ColorResource.RED_96262C,
        inactiveTrackColor: inActiveColor,
      ),
    );
  }

  Widget _buildIdentifyWidget() {
    Color identifyColor = (!isDisabled && deviceDetail.mac != null)
        ? ColorResource.RED_96262C
        : ColorResource.GREY_969696;
    if (deviceDetail.isUnavailable) {
      identifyColor = ColorResource.GREY_969696.withOpacity(0.5);
    }
    return SizedBox(
      width: 65.w,
      child: CustomButton(
        35,
        (!isDisabled && deviceDetail.mac != null) || !deviceDetail.isUnavailable
            ? onIdentifyButtonPressed
            : null,
        titleFont: Font.HelveticaRegular,
        color: identifyColor,
        titleFontSize: 14.sp,
        focusColor: ColorResource.RED_600C0F,
        title: StringResource.IDENTIFY,
      ),
    );
  }

  Widget _buildBatteryWidget() {
    String batteryPercentageText = "-";
    Color batteryColor;
    Color batteryTextColor = ColorResource.GREY_969696;
    if (deviceDetail.isUnavailable && deviceDetail.lowBatteryPowerOff) {
      batteryTextColor = ColorResource.RED_600C0F;
      batteryColor = ColorResource.RED_600C0F;
      batteryPercentageText = "-- %";
    }
    if (deviceDetail.isConnected && deviceDetail.batteryPercentage != null) {
      if (deviceDetail.batteryPercentage >= 10) {
        batteryPercentageText = "${deviceDetail.batteryPercentage}%";
      } else {
        batteryPercentageText = StringResource.LOW;
        batteryColor = ColorResource.RED_600C0F;
        batteryTextColor = ColorResource.RED_600C0F;
      }
    }
    return SizedBox(
      height: 32,
      width: 54.w,
      child: Stack(
        children: [
          SvgPicture.asset(
            ImageResource.BATTERY_ICON,
            height: 30,
            width: 48.w,
            color: batteryColor,
          ),
          SizedBox(
            width: 44.w,
            child: Center(
              child: CustomText(
                batteryPercentageText,
                color: batteryTextColor,
                fontSize: FontSize.smallFontSize,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDeviceNameWidget(BuildContext context) {
    Color deviceNameColor =
        deviceDetail?.deviceName?.contains("Trigger") ?? false
            ? ColorResource.RED_600C0F
            : ColorResource.GREY_969696;
    if (deviceDetail.isUnavailable) {
      deviceNameColor = deviceNameColor.withOpacity(0.4);
    }
    return SizedBox(
      width: 46.w,
      child: CustomText(
        deviceDetail?.deviceName,
        textAlign: TextAlign.center,
        maxLines: 2,
        font: Font.HelveticaBold,
        color: deviceNameColor,
        fontSize: FontSize.scenarioSettingsFontSize.sp,
      ),
    );
  }

  Widget _buildSignalStrengthWidget() {
    Color signalIconColor = ColorResource.GREY_969696;
    if (deviceDetail.isUnavailable) {
      signalIconColor = signalIconColor.withOpacity(0.4);
    }
    if ((deviceDetail.signalStrength == null ||
            deviceDetail.signalStrength <= 0) &&
        deviceDetail is! HubDevice) {
      return _buildEmptyDashWidget();
    }

    if (deviceDetail.isUnavailable &&
        !deviceDetail.lowBatteryPowerOff &&
        !deviceDetail.hightTemperaturePowerOff) {
      return SizedBox(
        width: 22.w,
        height: 22,
        child: SvgPicture.asset(ImageResource.WIFI_ERROR),
      );
    }

    final List<String> signalIcons = [
      ImageResource.SIGNAL_STRENGTH_1,
      ImageResource.SIGNAL_STRENGTH_2,
      ImageResource.SIGNAL_STRENGTH_3,
      ImageResource.SIGNAL_STRENGTH_4,
    ];

    if (deviceDetail is HubDevice) {
      final HubDevice hubDevice = deviceDetail as HubDevice;
      return StreamBuilder<int>(
        stream: hubDevice.signalStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 22.w,
              height: 22,
              child: SvgPicture.asset(
                signalIcons[_getRssiSignalStrength(snapshot.data)],
                color: signalIconColor,
              ),
            );
          }

          return _buildEmptyDashWidget();
        },
      );
    }

    return SizedBox(
      width: 22.w,
      height: 22,
      child: SvgPicture.asset(
        signalIcons[_getPercentageSignalStrength(deviceDetail.signalStrength)],
        color: signalIconColor,
      ),
    );
  }

  Widget _buildTemperatureWidget() {
    String icon = ImageResource.TEMPERATURE_WARNING_RED;
    if (deviceDetail.highTemperature) {
      icon = ImageResource.TEMPERATURE_WARNING_ORANGE;
    } else if (deviceDetail.hightTemperaturePowerOff) {
      icon = ImageResource.TEMPERATURE_WARNING_RED;
    } else {
      return SizedBox(width: 20.w);
    }
    return SvgPicture.asset(
      icon,
      width: 20.w,
    );
  }

  Widget _buildDividerWidget({bool isDisabled = false}) {
    return Container(
      height: double.infinity,
      width: 1,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 9.w),
      color: !isDisabled
          ? ColorResource.GREY_969696
          : ColorResource.GREY_969696.withOpacity(0.4),
    );
  }

  Widget _buildEmptyDashWidget() {
    return SizedBox(
      width: 22.w,
      child: CustomText(
        "-",
        color: ColorResource.RED_96262C,
        font: Font.HelveticaBold,
        textAlign: TextAlign.center,
        fontSize: 16.sp,
      ),
    );
  }

  Widget _buildUpdateFirmware(BuildContext context) {
    if (!deviceDetail.updateAvailable) return SizedBox(width: 20.w);
    return SizedBox(
      width: 20.w,
      height: 20,
      child: InkWell(
        onTap: () => _onUpdateWarningPressed(context),
        child: SvgPicture.asset(
          ImageResource.RED_WARNING,
          width: 20.w,
          height: 20,
        ),
      ),
    );
  }

  int _getRssiSignalStrength(int signal) {
    final int signalAbs = signal.abs();
    if (signalAbs > 80) return 0;
    if (signalAbs > 70) return 1;
    if (signalAbs > 65) return 2;

    return 3;
  }

  int _getPercentageSignalStrength(int signal) {
    if (signal == 100) return 3;
    return signal ~/ 25;
  }

  void _onUpdateWarningPressed(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(
        title: StringResource.UPDATE_FIRMWARE,
        isTitleCenter: true,
        descriptionWidget: Column(
          children: const [
            CustomText(
              StringResource.FIRMWARE_OUT_OF_DATE,
              color: ColorResource.WHITE_FFFFFF,
              textAlign: TextAlign.center,
              lineHeight: 1.7,
            ),
            SizedBox(height: 16),
            CustomText(
              StringResource.LATEST_DEVICE_FIRMWARE,
              color: ColorResource.WHITE_FFFFFF,
              fontSize: FontSize.smallFontSize,
              lineHeight: 1.7,
              textAlign: TextAlign.center,
            ),
          ],
        ),
        acceptButtonText: StringResource.OK,
        onAcceptButtonPressed: () => Navigator.pop(context),
      ),
    );
  }
}
