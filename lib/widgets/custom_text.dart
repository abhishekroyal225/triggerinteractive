import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';

class CustomText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Font font;
  final Color color;
  final double lineHeight;
  final TextAlign textAlign;
  final GestureTapCallback onTap;
  final bool isUnderLine;
  final bool isSingleLine;
  final int maxLines;
  final bool allowCopy;
  final String copyText;

  const CustomText(this.text,
      {this.fontSize = FontSize.subFontSize,
      this.font = Font.HelveticaRegular,
      this.color = ColorResource.BLACK_000000,
      this.lineHeight = 1.21, // Line Height - 17
      this.textAlign = TextAlign.left,
      this.onTap,
      this.isUnderLine = false,
      this.isSingleLine = false,
      this.maxLines,
      this.allowCopy = false,
      this.copyText});

  @override
  Widget build(BuildContext context) {
    final Text textWidget = Text(text ?? "",
        textAlign: textAlign,
        overflow: isSingleLine ? TextOverflow.ellipsis : null,
        softWrap: true,
        maxLines: maxLines,
        style: TextStyle(
            decoration:
                isUnderLine ? TextDecoration.underline : TextDecoration.none,
            color: color,
            fontFamily: font.value,
            fontSize: fontSize,
            //  fontSize: ScreenUtil().setSp(fontSize),
            height: lineHeight));

    if (onTap != null || allowCopy) {
      return GestureDetector(
        onTap: onTap,
        onLongPress: allowCopy
            ? () {
                Clipboard.setData(ClipboardData(text: copyText ?? ""));
                AppUtils.showToast("Copied to Clipboard");
              }
            : null,
        child: textWidget,
      );
    } else {
      return textWidget;
    }
  }
}
