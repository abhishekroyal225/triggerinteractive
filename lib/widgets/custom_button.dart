import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CustomButton extends StatelessWidget {
  final double height;
  final bool isGradient;
  final Color startColor;
  final Color endColor;
  final FractionalOffset begin;
  final FractionalOffset end;
  final String title;
  final Font titleFont;
  final Color titleColor;
  final double titleFontSize;
  final String leadingImage;
  final double leadingImageWidth;
  final double leadingImageHeight;
  final Color leadingImageColor;
  final bool isEnableLeadingMargin;
  final String trailingImage;
  final double trailingImageWidth;
  final double trailingImageHeight;
  final Color trailingImageColor;
  final double radius;
  final void Function() tapHandler;
  final Color color;
  final Color shadowColor;
  final double spreadRadius;
  final double blurRadius;
  final Offset offset;
  final double borderWidth;
  final Color borderColor;
  final Color focusColor;

  const CustomButton(this.height, this.tapHandler,
      {this.title,
      this.titleFont = Font.HelveticaBold,
      this.titleFontSize = FontSize.baseFontSize,
      this.titleColor = Colors.white,
      this.color = Colors.transparent,
      this.leadingImage,
      this.leadingImageWidth,
      this.leadingImageHeight,
      this.isEnableLeadingMargin = true,
      this.isGradient = false,
      this.startColor,
      this.endColor,
      this.begin,
      this.end,
      this.shadowColor,
      this.blurRadius,
      this.spreadRadius,
      this.offset,
      this.radius = 0,
      this.trailingImageWidth,
      this.trailingImage,
      this.trailingImageHeight,
      this.borderColor = Colors.transparent,
      this.borderWidth = 0,
      this.leadingImageColor,
      this.trailingImageColor,
      this.focusColor = ColorResource.WHITE_F2F2F2});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      child: InkWell(
        onTap: tapHandler,
        highlightColor: focusColor,
        focusColor: focusColor,
        hoverColor: focusColor,
        splashColor: Colors.transparent,
        child: SizedBox(
          height: height,
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(radius),
                border: Border.all(color: borderColor, width: borderWidth),
                gradient: isGradient
                    ? LinearGradient(
                        begin: begin,
                        end: end,
                        colors: [startColor, endColor],
                      )
                    : null,
                boxShadow: shadowColor != null
                    ? [
                        BoxShadow(
                          color: shadowColor,
                          spreadRadius: spreadRadius,
                          blurRadius: blurRadius,
                          offset: offset,
                        )
                      ]
                    : []),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (leadingImage != null)
                  Container(
                      margin: isEnableLeadingMargin
                          ? const EdgeInsets.only(right: 10)
                          : const EdgeInsets.all(0),
                      width: leadingImageWidth,
                      height: leadingImageHeight,
                      child: Image.asset(
                        leadingImage,
                        fit: BoxFit.contain,
                        color: leadingImageColor,
                      )),
                if (title != null)
                  Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.only(top: 3),
                    child: CustomText(
                      title,
                      font: titleFont,
                      color: titleColor,
                      fontSize: titleFontSize,
                    ),
                  ),
                if (trailingImage != null)
                  Container(
                    margin: const EdgeInsets.only(left: 15, top: 3),
                    alignment: Alignment.centerRight,
                    width: trailingImageWidth,
                    height: trailingImageHeight,
                    child: Image.asset(
                      trailingImage,
                      fit: BoxFit.contain,
                      color: trailingImageColor,
                    ),
                  )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
