import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';

class CustomLoadingWidget extends StatelessWidget {
  final Color color;

  const CustomLoadingWidget({Key key, this.color = ColorResource.WHITE_FFFFFF})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: color,
      child: const Center(
        child: CupertinoActivityIndicator(radius: 25),
      ),
    );
  }
}
