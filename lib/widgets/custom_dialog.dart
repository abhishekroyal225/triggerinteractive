import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String description;
  final Widget descriptionWidget;

  final bool isTitleCenter;

  final String acceptButtonText;
  final void Function() onAcceptButtonPressed;

  final String cancelButtonText;
  final void Function() onCancelButtonPressed;

  const CustomDialog({
    Key key,
    @required this.title,
    @required this.acceptButtonText,
    this.description,
    this.descriptionWidget,
    this.cancelButtonText,
    this.onAcceptButtonPressed,
    this.onCancelButtonPressed,
    this.isTitleCenter = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Material(
      color: ColorResource.BLACK_000000.withOpacity(0.8),
      child: Container(
        height: size.height,
        width: size.width,
        padding: const EdgeInsets.all(50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText(
              title.toUpperCase(),
              fontSize: FontSize.baseFontSize,
              font: Font.HelveticaBold,
              color: ColorResource.WHITE_FFFFFF,
              textAlign: isTitleCenter ? TextAlign.center : TextAlign.left,
            ),
            const SizedBox(height: 25),
            descriptionWidget ??
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: CustomText(
                    description,
                    textAlign: TextAlign.center,
                    color: ColorResource.WHITE_FFFFFF,
                    lineHeight: 1.6,
                  ),
                ),
            const SizedBox(height: 45),
            CustomButton(
              50,
              onAcceptButtonPressed,
              title: acceptButtonText.toUpperCase(),
              color: ColorResource.RED_96262C,
              focusColor: ColorResource.RED_600C0F,
            ),
            if (cancelButtonText != null) ...[
              const SizedBox(height: 20),
              CustomButton(
                50,
                onCancelButtonPressed,
                borderWidth: 1,
                title: cancelButtonText.toUpperCase(),
                focusColor: ColorResource.BLACK_333333,
                borderColor: ColorResource.WHITE_FFFFFF,
              ),
            ]
          ],
        ),
      ),
    );
  }
}
