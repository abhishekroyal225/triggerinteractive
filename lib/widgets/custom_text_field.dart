import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CustomTextField extends StatelessWidget {
  final String title;
  final String placeHolderText;
  final Font font;
  final double fontSize;
  final Color color;
  final Color textFieldBackGroundColor;
  final void Function() onTextFieldTapHandler;
  final TextCapitalization textCapitalization;
  final String leadingImage;
  final double leadingImageWidth;
  final double leadingImageHeight;
  final String trailingImage;
  final double trailingImageWidth;
  final double trailingImageHeight;
  final double borderRadius;
  final bool isObscureText;
  final void Function() trailingImageTapHandler;
  final Function onValueChange;
  final double rightMargin;
  final double leftMargin;
  final TextInputType keyboardType;
  final bool isreadOnly;
  final bool enabled;
  final TextEditingController controller;

  const CustomTextField(this.title, this.placeHolderText, this.onValueChange,
      {this.isObscureText = false,
      this.trailingImage,
      this.trailingImageWidth,
      this.trailingImageHeight,
      this.trailingImageTapHandler,
      this.rightMargin = 0,
      this.leftMargin = 0,
      this.textCapitalization = TextCapitalization.none,
      this.keyboardType = TextInputType.text,
      this.leadingImage,
      this.leadingImageWidth,
      this.borderRadius,
      this.leadingImageHeight,
      this.isreadOnly = false,
      this.enabled = true,
      this.font = Font.HelveticaRegular,
      this.fontSize = FontSize.subFontSize,
      this.color = ColorResource.BLACK_000000,
      this.controller,
      this.textFieldBackGroundColor = ColorResource.WHITE_FFFFFF,
      this.onTextFieldTapHandler});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (title != null)
          Container(
            padding: EdgeInsets.only(
                top: 10, left: leftMargin, right: 17, bottom: 11),
            alignment: Alignment.centerLeft,
            child: CustomText(
              title,
              fontSize: 16,
            ),
          ),
        SizedBox(
          height: 50,
          child: Container(
            margin: EdgeInsets.only(left: leftMargin, right: rightMargin),
            padding: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              color: textFieldBackGroundColor,
              borderRadius: BorderRadius.circular(borderRadius ?? 0),
            ),
            child: Row(
              children: <Widget>[
                if (leadingImage != null)
                  InkWell(
                    child: Container(
                      margin: const EdgeInsets.only(right: 15),
                      alignment: Alignment.center,
                      width: leadingImageWidth,
                      height: leadingImageHeight,
                      child: SvgPicture.asset(leadingImage),
                    ),
                  ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: TextField(
                      onTap: onTextFieldTapHandler,
                      style: TextStyle(
                        color: color,
                      ),
                      enabled: enabled,
                      keyboardType: keyboardType,
                      onChanged: (value) {
                        onValueChange(value);
                      },
                      textCapitalization: textCapitalization,
                      controller: controller,
                      obscureText: isObscureText,
                      readOnly: isreadOnly,
                      decoration: InputDecoration.collapsed(
                        hintText: placeHolderText,
                        hintStyle: TextStyle(
                          color: ColorResource.GREY_969696,
                          fontFamily: font.toString(),
                          fontSize: fontSize,
                        ),
                      ),
                    ),
                  ),
                ),
                if (trailingImage != null)
                  InkWell(
                    onTap: trailingImageTapHandler,
                    child: Container(
                      alignment: Alignment.center,
                      width: trailingImageWidth,
                      height: trailingImageHeight,
                      child: SvgPicture.asset(trailingImage),
                    ),
                  ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
