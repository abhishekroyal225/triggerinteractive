import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final String subtitle;

  final int connectedDevices;
  final bool hideBackButton;

  const CustomAppBar(
    this.title, {
    this.subtitle,
    this.connectedDevices = 0,
    this.hideBackButton = false,
  });

  @override
  Size get preferredSize => const Size(double.infinity, 61);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            title,
            fontSize: FontSize.baseFontSize,
            color: ColorResource.GREY_969696,
            font: Font.HelveticaBold,
          ),
          CustomText(
            subtitle ??
                UserDataHandler.currentUser?.userName?.toUpperCase() ??
                "",
            color: ColorResource.GREY_969696,
            fontSize: FontSize.smallFontSize,
          ),
        ],
      ),
      brightness: Brightness.dark,
      leading: hideBackButton ? Container() : null,
      leadingWidth: hideBackButton ? 0 : null,
      backgroundColor: ColorResource.BLACK_333333,
      actions: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomText(
              StringResource.TARGETS.toUpperCase(),
              color: ColorResource.GREY_969696,
              fontSize: FontSize.smallFontSize,
            ),
            CustomText(
              connectedDevices.toString().padLeft(2, "0"),
              fontSize: FontSize.baseFontSize,
              font: Font.HelveticaBold,
              color: ColorResource.GREY_969696,
            ),
          ],
        ),
        const SizedBox(width: 15)
      ],
    );
  }
}
