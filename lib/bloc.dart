import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/bloc/scenario_progress_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';

class EchoBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    if (DebugMode.isInDebugMode &&
        event is! ScenarioProgressCounterRefreshEvent) {
      AppUtils.logMessage(event.toString());
    }
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    if (DebugMode.isInDebugMode &&
        transition.event is! ScenarioProgressCounterRefreshEvent) {
      AppUtils.logMessage(transition.toString());
    }
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    if (DebugMode.isInDebugMode) AppUtils.logMessage(error.toString());
  }
}
