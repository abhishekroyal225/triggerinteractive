import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trigger_interactive_app/authentication/authentication_bloc.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/un_authenticated_event.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/splash_screen.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthenticationBloc authenticationBloc;

  @override
  void initState() {
    super.initState();
    authenticationBloc = context.read<AuthenticationBloc>();
  }

  @override
  Widget build(BuildContext context) {
    eventBus.on<UnAuthenticatedEvent>().listen((event) {
      authenticationBloc.add(LoggedOutEvent());
    });

    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: getRoute,
        home: addAuthBloc(context, SplashScreen()),
      ),
    );
  }
}
