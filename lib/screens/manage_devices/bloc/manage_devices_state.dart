part of 'manage_devices_bloc.dart';

class ManageDevicesState extends BaseEquatable {}

class ManageDevicesInitialState extends ManageDevicesState {
  final String error;

  ManageDevicesInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}

class ManageDevicesLoadingState extends ManageDevicesState {
  @override
  bool operator ==(Object obj) => false;
}
