part of 'manage_devices_bloc.dart';

class ManageDevicesEvent extends BaseEquatable {}

class ManageDevicesInitialEvent extends ManageDevicesEvent {}

class ManageDevicesConnectEvent extends ManageDevicesEvent {
  final BaseDevice deviceDetail;
  final bool allowBLEOp;

  ManageDevicesConnectEvent(this.deviceDetail, [this.allowBLEOp = true]);
}

class ManageDevicesConnectAllEvent extends ManageDevicesEvent {
  final HubDevice hubDetail;

  ManageDevicesConnectAllEvent(this.hubDetail);
}

class ManageDevicesLoadingEvent extends ManageDevicesEvent {}

class ManageDevicesLoadedEvent extends ManageDevicesEvent {}

class ManageDevicesBluetoothToggleEvent extends ManageDevicesEvent {}

class ManageDevicesLocationToggleEvent extends ManageDevicesEvent {}

class ManageDevicesLocationServiceToggleEvent extends ManageDevicesEvent {}

class ManageDevicesAddTargetsEvent extends ManageDevicesEvent {}

class ManageDevicesGeneralStatusUpdateEvent extends ManageDevicesEvent {}
