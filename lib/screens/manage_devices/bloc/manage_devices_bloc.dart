import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/ble/ble.dart';
import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';
import 'package:trigger_interactive_app/ble/data/responses/device_info_response.dart';
import "package:trigger_interactive_app/ble/data/responses/discover_device_response.dart";
import 'package:trigger_interactive_app/ble/data/responses/error_notification_response.dart';
import 'package:trigger_interactive_app/ble/data/responses/general_status_response.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';
import 'package:trigger_interactive_app/ble/device/sta_device.dart';
import 'package:trigger_interactive_app/bus/device_disconnected_event_bus.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/home_state_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/constants.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'manage_devices_event.dart';
part 'manage_devices_state.dart';

class ManageDevicesBloc extends Bloc<ManageDevicesEvent, ManageDevicesState> {
  List<HubDevice> devices = [];

  bool isBluetoothOn = false;
  bool isLocationServicesEnabled = false;
  bool isLocationPermissionGranted = false;

  bool hasReceivedGeneralData = false;

  StreamSubscription _dataStreamSub;
  Timer pollGeneralStatusTimer;

  bool hasDiscoveredDevices = false;

  final BLE _ble = BLE();

  ManageDevicesBloc() : super(ManageDevicesInitialState());

  @override
  Stream<ManageDevicesState> mapEventToState(ManageDevicesEvent event) async* {
    if (event is ManageDevicesInitialEvent) {
      await _checkAllPermissions();
      final bool isHubAlreadyConnected =
          await DeviceDataHandler.selectedHub?.isConnectedBLE();

      if (isHubAlreadyConnected ?? false) {
        await DeviceDataHandler.selectedHub.requestDiscovery();
        yield ManageDevicesLoadingState();
      } else {
        DeviceDataHandler.selectedHub = null;
        hasDiscoveredDevices = false;
        hasReceivedGeneralData = false;
        eventBus.fire(HomeStateRefreshEventBus());

        yield* _scanAllDevices();
      }
    }

    if (event is ManageDevicesConnectEvent) {
      event.deviceDetail.isConnected = !event.deviceDetail.isConnected;
      yield ManageDevicesInitialState();

      if (event.deviceDetail is HubDevice) {
        final HubDevice hubDevice = event.deviceDetail as HubDevice;

        if (!hubDevice.isConnected) {
          hubDevice.mac = null;
          await hubDevice.disconnect();
          hasDiscoveredDevices = false;
          DeviceDataHandler.selectedHub = null;
          _dataStreamSub?.cancel();
        } else {
          hasReceivedGeneralData = false;

          hubDevice.targets?.clear();
          if (event.allowBLEOp) await hubDevice.connect();
          DeviceDataHandler.selectedHub = hubDevice;

          hubDevice.requestDiscovery(2);

          _listenToData(event);
        }
      } else {
        final BaseDevice targetDevice = event.deviceDetail;

        if (targetDevice.mac != null) {
          await DeviceDataHandler.selectedHub.claimRequest(
            targetDevice.mac,
            targetDevice.isConnected,
          );
          targetDevice.id = !targetDevice.isConnected
              ? null
              : DeviceDataHandler.createSTAId(targetDevice.type);
        }
      }
      eventBus.fire(HomeStateRefreshEventBus());

      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesConnectAllEvent) {
      yield ManageDevicesLoadingState();
      for (final BaseDevice targets in event.hubDetail.targets) {
        if (!targets.isConnected &&
            targets.mac != null &&
            hasReceivedGeneralData) {
          await DeviceDataHandler.selectedHub.claimRequest(targets.mac);
          targets.isConnected = true;
          targets.id ??= DeviceDataHandler.createSTAId(targets.type);
        }
      }
      eventBus.fire(HomeStateRefreshEventBus());

      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesLoadingEvent) {
      yield ManageDevicesLoadingState();
    }

    if (event is ManageDevicesLoadedEvent) {
      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesBluetoothToggleEvent) {
      isBluetoothOn = await _ble.turnBluetoothOn();

      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesLocationToggleEvent) {
      isLocationPermissionGranted = await _ble.enableLocationPermission();

      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesLocationServiceToggleEvent) {
      isLocationServicesEnabled = await _ble.enableLocationService();

      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesAddTargetsEvent) {
      yield ManageDevicesInitialState();
    }

    if (event is ManageDevicesGeneralStatusUpdateEvent) {
      yield ManageDevicesInitialState();
    }
  }

  void _listenToData(ManageDevicesConnectEvent event) {
    _dataStreamSub?.cancel();

    final HubDevice hubDevice = event.deviceDetail as HubDevice;

    _dataStreamSub = hubDevice.dataStream.listen((res) async {
      if (res.type == BLEDataType.discover_response) {
        final response = res.data as DiscoverDeviceResponse;

        if (response.isFinal) hasDiscoveredDevices = true;

        if (response.isFinal &&
            response.deviceType == DeviceType.hub &&
            hubDevice.mac == null) {
          hubDevice.mac = response.mac;
          await hubDevice.requestGeneralStatus();

          pollGeneralStatusTimer?.cancel();
          pollGeneralStatusTimer = Timer.periodic(
            const Duration(seconds: 30),
            (timer) => requestDeviceGeneralStatus(),
          );
        }

        if (response.deviceType == DeviceType.sta ||
            response.deviceType == DeviceType.rpt) {
          final bool targetExists = hubDevice.targets.any(
            (t) => t.mac == response.mac,
          );

          BaseDevice device = StaDevice(mac: response.mac);
          if (response.deviceType == DeviceType.rpt) {
            device = device.asDevice(DeviceType.rpt);
          }

          if (!targetExists) {
            hubDevice.targets.add(device);
            await hubDevice.requestGeneralStatus([response.mac]);
          }

          add(ManageDevicesAddTargetsEvent());
        }
        // Requesting info for each discovery response for checking firmware version.
        await hubDevice.requestDeviceInfo(response.mac);
      }

      if (res.type == BLEDataType.error_notification) {
        final response = res.data as ErrorNotificationResponse;

        final BaseDevice targetDevice = hubDevice.targets.firstWhere(
          (target) => target.mac == response.mac,
          orElse: () => null,
        );

        if (response.error == DeviceErrorType.error_transmitting_end_device) {
          if (targetDevice != null) {
            targetDevice.isConnected = false;
            targetDevice.isUnavailable = true;
            eventBus.fire(DeviceDisconnectedEventBus());
          }

          eventBus.fire(HomeStateRefreshEventBus());
        }

        if (response.error == DeviceErrorType.high_temperature_poweroff) {
          if (targetDevice != null) {
            targetDevice.hightTemperaturePowerOff = true;
            targetDevice.isUnavailable = true;
            targetDevice.isConnected = false;
            eventBus.fire(DeviceDisconnectedEventBus());
          }

          eventBus.fire(HomeStateRefreshEventBus());
        }

        if (response.error == DeviceErrorType.low_battery_poweroff) {
          if (targetDevice != null) {
            targetDevice.lowBatteryPowerOff = true;
            targetDevice.isUnavailable = true;
            targetDevice.isConnected = false;
            eventBus.fire(DeviceDisconnectedEventBus());
          }
        }

        if (hasReceivedGeneralData) {
          if (response.error != null) {
            final String message = response.error.toString().split(".")?.last ??
                StringResource.SOMETHING_WENT_WRONG;
            AppUtils.showToast(message.replaceAll("_", " "));
          }
        }
        add(ManageDevicesAddTargetsEvent());
      }

      if (res.type == BLEDataType.general_status) {
        final response = res.data as GeneralStatusResponse;

        if (hubDevice?.mac == response.mac) {
          hubDevice.batteryPercentage = response.battery;
          hubDevice.signalStrength = response.signal;
        }

        final BaseDevice targetDevice = hubDevice.targets.firstWhere(
          (target) => target.mac == response.mac,
          orElse: () => null,
        );

        if (targetDevice != null) {
          if (targetDevice.isUnavailable) targetDevice.isUnavailable = false;
          targetDevice.batteryPercentage = response.battery;
          targetDevice.signalStrength = response.signal;
          targetDevice.isConnected =
              response.connectedMac == event.deviceDetail.mac;
          targetDevice.highTemperature = response.highTemperature;
          if (targetDevice.isConnected) {
            targetDevice.id ??=
                DeviceDataHandler.createSTAId(targetDevice.type);
          }
          hasReceivedGeneralData = true;
          eventBus.fire(HomeStateRefreshEventBus());
        }

        add(ManageDevicesGeneralStatusUpdateEvent());
      }

      if (res.type == BLEDataType.device_info_response) {
        final response = res.data as DeviceInfoResponse;
        BaseDevice targetDevice = hubDevice.targets.firstWhere(
          (target) => target.mac == response.mac,
          orElse: () => null,
        );
        if (hubDevice.mac == response.mac) {
          targetDevice = hubDevice;
        }
        targetDevice?.updateVersion = response.triggerFWMajorVersion;
        targetDevice?.updateAvailable =
            response.triggerFWMajorVersion < Constants.FIRMWARE_VERSION;
        add(ManageDevicesGeneralStatusUpdateEvent());
      }
    });
  }

  Future<void> requestDeviceGeneralStatus() async {
    try {
      await DeviceDataHandler.selectedHub.requestGeneralStatus([
        DeviceDataHandler.selectedHub.mac,
        ...DeviceDataHandler.selectedHub.targets.map((e) => e.mac)
      ]);
    } catch (e) {/* */}
  }

  Future<void> _checkAllPermissions() async {
    try {
      await _ble.initialize();
      await _ble.stopScan();

      isBluetoothOn = await _ble.isBluetoothOn;
      isLocationPermissionGranted = await _ble.isLocationEnabled;
      isLocationServicesEnabled = await _ble.isLocationServicesEnabled;
    } finally {}
  }

  Stream<ManageDevicesState> _scanAllDevices() async* {
    yield ManageDevicesLoadingState();

    if (!(isBluetoothOn &&
        isLocationPermissionGranted &&
        isLocationServicesEnabled)) {
      yield ManageDevicesInitialState();

      return;
    }

    Future.delayed(const Duration(seconds: 5), () async {
      await _ble.stopScan();
      add(ManageDevicesLoadedEvent());
    });

    devices = await _ble.getPairedDevices();

    yield ManageDevicesLoadingState();

    if (devices.isNotEmpty) {
      add(ManageDevicesConnectEvent(devices.first, false));
    }

    _ble.getAvailableDevices.listen((device) {
      final bool alreadyExists = devices.any((element) =>
          element.peripheral.identifier == device.peripheral.identifier);

      if (!alreadyExists) {
        devices.add(device);

        add(ManageDevicesLoadingEvent());
      }
    });
  }

  void dispose() {
    pollGeneralStatusTimer?.cancel();
    _dataStreamSub?.cancel();
  }
}
