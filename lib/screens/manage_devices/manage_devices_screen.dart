import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/ble/device/hub_device.dart';
import 'package:trigger_interactive_app/screens/manage_devices/bloc/manage_devices_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';
import 'package:trigger_interactive_app/widgets/device_detail_widget.dart';

class ManageDevicesScreen extends StatefulWidget {
  @override
  _ManageDevicesScreenState createState() => _ManageDevicesScreenState();
}

class _ManageDevicesScreenState extends State<ManageDevicesScreen> {
  ManageDevicesBloc manageDevicesBloc;

  @override
  void initState() {
    super.initState();
    manageDevicesBloc = context.read<ManageDevicesBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      onFocusLost: () {
        manageDevicesBloc.pollGeneralStatusTimer?.cancel();
      },
      onFocusGained: () {
        manageDevicesBloc.pollGeneralStatusTimer?.cancel();
        manageDevicesBloc.pollGeneralStatusTimer = Timer.periodic(
          const Duration(seconds: 30),
          (timer) => manageDevicesBloc.requestDeviceGeneralStatus(),
        );
      },
      child: BlocConsumer<ManageDevicesBloc, ManageDevicesState>(
        listener: (context, state) {
          if (state is ManageDevicesInitialState) {
            if (state.error != null) AppUtils.showToast(state.error);
          }
        },
        builder: (context, state) {
          return Column(
            children: [
              _buildSelectiveRequestItemWidget(),
              if (manageDevicesBloc.devices?.isEmpty ?? true) ...[
                _buildNoDeviceWidget(),
                Expanded(child: Container()),
              ] else
                _buildHubListWidget(),
              _buildSearchDevicesButton(state is ManageDevicesLoadingState)
            ],
          );
        },
      ),
    );
  }

  Widget _buildHubListWidget() {
    final bool notConnected =
        manageDevicesBloc.devices.every((e) => !e.isConnected);

    return Expanded(
      child: ListView.builder(
        itemCount: manageDevicesBloc.devices.length,
        itemBuilder: (context, index) {
          final HubDevice hubDetails = manageDevicesBloc.devices[index];
          return Visibility(
            visible: hubDetails.isConnected || notConnected,
            child: Column(
              children: [
                DeviceDetailWidget(
                  isHub: true,
                  deviceDetail: hubDetails,
                  onConnectSwitchFlipped: _onConnectSwitchFlipped,
                  onIdentifyButtonPressed: () async {
                    await hubDetails.identifyRequest();
                  },
                ),
                if (hubDetails.isConnected)
                  _buildTargetDeviceListWidget(hubDetails)
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildSelectiveRequestItemWidget() {
    if (!manageDevicesBloc.isBluetoothOn) {
      return _buildRequestItemWidget(
        StringResource.BLUETOOTH_NOT_ENABLED,
        buttonText: StringResource.ENABLE,
        onPressed: _onBluetoothEnableButtonPressed,
      );
    }
    if (!manageDevicesBloc.isLocationPermissionGranted) {
      return _buildRequestItemWidget(
        StringResource.LOCATION_NOT_ENABLED,
        buttonText: StringResource.ENABLE,
        onPressed: _onLocationEnableButtonPressed,
      );
    }
    if (!manageDevicesBloc.isLocationServicesEnabled) {
      return _buildRequestItemWidget(
        StringResource.LOCATION_SERVICE_NOT_ENABLED,
        buttonText: StringResource.SETTINGS,
        onPressed: _onLocationServiceEnableButtonPressed,
      );
    }
    return Container();
  }

  Widget _buildRequestItemWidget(
    String title, {
    String buttonText,
    void Function() onPressed,
  }) {
    return Container(
      color: Colors.teal[700],
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title, style: const TextStyle(color: Colors.white60)),
          OutlinedButton(
            style: const ButtonStyle(
              foregroundColor: ColorResource.WHITE_FFFFFF,
            ),
            onPressed: onPressed,
            child: Text(buttonText),
          )
        ],
      ),
    );
  }

  Widget _buildTargetDeviceListWidget(HubDevice hubDetails) {
    if (!manageDevicesBloc.hasDiscoveredDevices && hubDetails.targets.isEmpty) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Padding(
            padding: EdgeInsets.only(top: 50),
            child: Center(child: CupertinoActivityIndicator()),
          )
        ],
      );
    }

    final bool isAllDeviceConnected =
        hubDetails.targets.every((element) => element.isConnected);

    hubDetails.targets.sort();

    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: CustomButton(
            50,
            isAllDeviceConnected
                ? null
                : () => _onConnectAllButtonPressed(hubDetails),
            title: StringResource.CONNECT_ALL_DEVICES,
            focusColor: ColorResource.RED_600C0F,
            color: isAllDeviceConnected
                ? ColorResource.GREY_D2D2D2
                : ColorResource.RED_96262C,
          ),
        ),
        _buildDividerWidget(),
        if (hubDetails.targets.isEmpty)
          _buildNoDeviceWidget()
        else ...[
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: hubDetails.targets.length,
            separatorBuilder: (_, __) => _buildDividerWidget(),
            itemBuilder: (context, index) {
              return DeviceDetailWidget(
                deviceDetail: hubDetails.targets[index],
                onConnectSwitchFlipped: _onConnectSwitchFlipped,
                isDisabled: !manageDevicesBloc.hasReceivedGeneralData,
                onIdentifyButtonPressed: () {
                  hubDetails.identifyRequest(hubDetails.targets[index].mac);
                },
              );
            },
          ),
          _buildDividerWidget(),
        ],
        const SizedBox(height: 20),
      ],
    );
  }

  Widget _buildNoDeviceWidget() {
    return Container(
      width: double.infinity,
      height: 75,
      padding: const EdgeInsets.all(25),
      decoration: const BoxDecoration(
        border: Border(bottom: BorderSide(color: ColorResource.WHITE_FFFFFF)),
      ),
      child: const CustomText(
        StringResource.NO_DEVICE_FOUND,
        font: Font.HelveticaBold,
        color: ColorResource.GREY_969696,
      ),
    );
  }

  Widget _buildDividerWidget() {
    return Container(
      height: 1,
      width: double.infinity,
      color: ColorResource.WHITE_FFFFFF,
    );
  }

  Widget _buildSearchDevicesButton(bool isLoading) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Material(
        child: InkWell(
          onTap: _onSearchDevicesButtonPressed,
          splashColor: Colors.transparent,
          focusColor: ColorResource.WHITE_F2F2F2,
          hoverColor: ColorResource.WHITE_F2F2F2,
          highlightColor: ColorResource.WHITE_F2F2F2,
          child: SizedBox(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomText(
                  isLoading
                      ? StringResource.SEARCHING_FOR_DEVICES
                      : StringResource.SEARCH_FOR_DEVICES,
                  fontSize: FontSize.baseFontSize,
                  font: Font.HelveticaBold,
                  color: ColorResource.RED_96262C,
                ),
                if (isLoading) ...[
                  const SizedBox(width: 8),
                  const CupertinoActivityIndicator()
                ]
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onConnectAllButtonPressed(HubDevice hubDetails) {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: StringResource.CONNECT_ALL_DEVICES,
          description: StringResource.ARE_YOU_SURE_CONNECT_ALL_DEVICES,
          cancelButtonText: StringResource.NO,
          acceptButtonText: StringResource.YES,
          onCancelButtonPressed: () => Navigator.pop(context),
          onAcceptButtonPressed: () {
            manageDevicesBloc.add(ManageDevicesConnectAllEvent(hubDetails));

            Navigator.pop(context);
          },
        );
      },
    );
  }

  void _onConnectSwitchFlipped(BaseDevice deviceDetails, {bool isHub = false}) {
    manageDevicesBloc.add(ManageDevicesConnectEvent(deviceDetails));
  }

  void _onSearchDevicesButtonPressed() {
    manageDevicesBloc.add(ManageDevicesInitialEvent());
  }

  Future<void> _onBluetoothEnableButtonPressed() async {
    manageDevicesBloc.add(ManageDevicesBluetoothToggleEvent());
  }

  Future<void> _onLocationEnableButtonPressed() async {
    manageDevicesBloc.add(ManageDevicesLocationToggleEvent());
  }

  Future<void> _onLocationServiceEnableButtonPressed() async {
    manageDevicesBloc.add(ManageDevicesLocationServiceToggleEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }
}
