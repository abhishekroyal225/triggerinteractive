import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/home_state_refresh_event_bus.dart';
import 'package:trigger_interactive_app/bus/settings_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'edit_username_event.dart';
part 'edit_username_state.dart';

class EditUsernameBloc extends Bloc<EditUsernameEvent, EditUsernameState> {
  TextEditingController usernameController = TextEditingController();

  EditUsernameBloc() : super(EditUsernameInitialState()) {
    usernameController.value = TextEditingValue(
      text: UserDataHandler.currentUser.userName,
    );
  }

  @override
  Stream<EditUsernameState> mapEventToState(
    EditUsernameEvent event,
  ) async* {
    if (event is EditUsernameInitialEvent) {
      yield EditUsernameInitialState();
    }

    if (event is EditUsernameUpdateEvent) {
      if (usernameController.text.trim().isEmpty) {
        yield EditUsernameInitialState(
          error: StringResource.USERNAME_SHOULD_NOT_BE_BLANK,
        );
        return;
      }

      UserDataHandler.updateUsername(usernameController.text);
      eventBus.fire(SettingsRefreshEventBus());
      eventBus.fire(HomeStateRefreshEventBus());
      yield EditUsernameSuccessState();
    }
  }

  void dispose() {
    usernameController.dispose();
  }
}
