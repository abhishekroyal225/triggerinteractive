part of 'edit_username_bloc.dart';

class EditUsernameState extends BaseEquatable {}

class EditUsernameInitialState extends EditUsernameState {
  final String error;

  EditUsernameInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}

class EditUsernameSuccessState extends EditUsernameState {}
