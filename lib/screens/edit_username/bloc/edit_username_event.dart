part of 'edit_username_bloc.dart';

class EditUsernameEvent extends BaseEquatable {}

class EditUsernameInitialEvent extends EditUsernameEvent {}

class EditUsernameUpdateEvent extends EditUsernameEvent {}
