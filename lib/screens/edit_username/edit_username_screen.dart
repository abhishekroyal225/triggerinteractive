import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/authentication/authentication_bloc.dart';
import 'package:trigger_interactive_app/screens/edit_username/bloc/edit_username_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_app_bar.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class EditUsernameScreen extends StatefulWidget {
  @override
  _EditUsernameScreenState createState() => _EditUsernameScreenState();
}

class _EditUsernameScreenState extends State<EditUsernameScreen> {
  AuthenticationBloc authenticationBloc;
  EditUsernameBloc editUsernameBloc;

  @override
  void initState() {
    super.initState();

    authenticationBloc = context.read<AuthenticationBloc>();
    editUsernameBloc = context.read<EditUsernameBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      color: ColorResource.GREY_E7E7E7,
      appBar: CustomAppBar(StringResource.USERNAME.toUpperCase()),
      body: BlocConsumer<EditUsernameBloc, EditUsernameState>(
        listener: (context, state) {
          if (state is EditUsernameInitialState) {
            if (state.error != null) {
              AppUtils.showToast(state.error);
            }
          }

          if (state is EditUsernameSuccessState) {
            AppUtils.showToast(
              StringResource.USERNAME_UPDATED,
              backgroundColor: Colors.green,
            );
            Navigator.pop(context);
          }
        },
        builder: (context, state) {
          return Container(
            padding: const EdgeInsets.all(20),
            decoration: const BoxDecoration(
              color: ColorResource.WHITE_FFFFFF,
              border: Border(bottom: BorderSide()),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const CustomText(
                  StringResource.YOUR_USERNAME,
                  font: Font.HelveticaBold,
                  color: ColorResource.GREY_969696,
                ),
                TextFormField(
                  maxLength: 10,
                  controller: editUsernameBloc.usernameController,
                  decoration: const InputDecoration(counterText: ''),
                )
              ],
            ),
          );
        },
      ),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.all(20),
        child: CustomButton(
          50,
          _onUpdateUsernameButtonPressed,
          color: ColorResource.RED_96262C,
          focusColor: ColorResource.RED_600C0F,
          title: StringResource.UPDATE_USERNAME,
        ),
      ),
    );
  }

  void _onUpdateUsernameButtonPressed() {
    editUsernameBloc.add(EditUsernameUpdateEvent());
  }

  @override
  void dispose() {
    editUsernameBloc.close();
    super.dispose();
  }
}
