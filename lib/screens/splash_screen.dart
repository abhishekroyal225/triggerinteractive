import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      appBar: AppBar(
        backgroundColor: ColorResource.BLACK_333333,
        centerTitle: true,
        brightness: Brightness.dark,
        title: SvgPicture.asset(ImageResource.TRIGGER_LOGO_ICON, width: 30),
      ),
      color: ColorResource.GREY_E7E7E7,
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            SvgPicture.asset(
              ImageResource.TRIGGER_LOGO,
              width: MediaQuery.of(context).size.width,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CupertinoActivityIndicator(radius: 25),
                  SizedBox(height: 20),
                  CustomText(
                    "${StringResource.LOADING}...",
                    font: Font.HelveticaBold,
                    color: ColorResource.RED_96262C,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
