import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/screens/score_board/bloc/score_board_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class ScoreBoardScreen extends StatefulWidget {
  @override
  _ScoreBoardScreenState createState() => _ScoreBoardScreenState();
}

class _ScoreBoardScreenState extends State<ScoreBoardScreen> {
  ScoreBoardBloc scoreBoardBloc;

  @override
  void initState() {
    super.initState();
    scoreBoardBloc = context.read<ScoreBoardBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ScoreBoardBloc, ScoreBoardState>(
      listener: (context, state) {
        if (state is ScoreBoardInitialState) {
          if (state.error != null) {
            AppUtils.showToast(state.error);
          }
        }
      },
      builder: (context, state) {
        return DefaultTabController(
          length: 2,
          child: Container(
            color: ColorResource.WHITE_FFFFFF,
            child: Column(
              children: [
                _buildTabWidget(),
                Expanded(
                  child: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      _buildScoreDataPage(
                        rank: scoreBoardBloc.monthRank,
                        scoreBoardData: scoreBoardBloc.monthScoreBoardData,
                        totalScore: scoreBoardBloc.monthTotalScore,
                      ),
                      _buildScoreDataPage(
                        rank: scoreBoardBloc.allRank,
                        scoreBoardData: scoreBoardBloc.allScoreBoardData,
                        totalScore: scoreBoardBloc.allTotalScore,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildTabWidget() {
    return Container(
      color: ColorResource.GREY_E7E7E7,
      child: TabBar(
        indicatorSize: TabBarIndicatorSize.tab,
        unselectedLabelColor: ColorResource.GREY_969696,
        labelColor: ColorResource.RED_96262C,
        labelPadding: const EdgeInsets.only(top: 15, bottom: 2),
        labelStyle: TextStyle(
          fontFamily: Font.HelveticaRegular.value,
          fontSize: FontSize.subFontSize,
        ),
        indicator: const UnderlineTabIndicator(
          insets: EdgeInsets.symmetric(horizontal: 30),
          borderSide: BorderSide(width: 4, color: ColorResource.RED_96262C),
        ),
        tabs: const [
          Tab(text: StringResource.CURRENT_MONTH),
          Tab(text: StringResource.YEAR_TO_DATE)
        ],
      ),
    );
  }

  Widget _buildScoreDataPage({
    double totalScore,
    int rank,
    List<ScoreBoardData> scoreBoardData,
  }) {
    return Column(
      children: [
        _buildScoreDetailsWidget(
          rank: rank,
          totalScore: totalScore,
          scoreBoardData: scoreBoardData,
        ),
        const SizedBox(height: 15),
        Expanded(child: _buildScoreboardTableWidget(scoreBoardData)),
      ],
    );
  }

  Widget _buildScoreDetailsWidget({
    double totalScore,
    int rank,
    List<ScoreBoardData> scoreBoardData,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: CustomText(
            scoreBoardBloc.userName,
            fontSize: FontSize.baseFontSize,
            font: Font.HelveticaBold,
          ),
        ),
        const Divider(),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const CustomText(
                "${StringResource.TOTAL_SCORE} :",
                fontSize: FontSize.baseFontSize,
                color: ColorResource.GREY_969696,
              ),
              CustomText(
                totalScore.toInt().toString(),
                fontSize: FontSize.baseFontSize,
                color: ColorResource.GREY_969696,
              )
            ],
          ),
        ),
        const Divider(),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const CustomText(
                "${StringResource.RANK} :",
                fontSize: FontSize.baseFontSize,
                color: ColorResource.GREY_969696,
              ),
              CustomText(
                "$rank/${scoreBoardData.length}",
                fontSize: FontSize.baseFontSize,
                color: ColorResource.GREY_969696,
              )
            ],
          ),
        ),
        const Divider(),
      ],
    );
  }

  Widget _buildScoreboardTableWidget(List<ScoreBoardData> scoreBoardData) {
    final width = MediaQuery.of(context).size.width / 3;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: width,
              child: const CustomText(
                StringResource.RANK,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: width,
              child: CustomText(
                StringResource.USERNAME.toUpperCase(),
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: width,
              child: const CustomText(
                StringResource.SCORE,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        const SizedBox(height: 25),
        Expanded(
          child: ListView.separated(
            separatorBuilder: (_, __) => const SizedBox(height: 20),
            itemCount: scoreBoardData.length,
            itemBuilder: (context, index) {
              return _buildScoreBoardItemWidget(
                index,
                scoreBoardData[index],
              );
            },
          ),
        )
      ],
    );
  }

  Widget _buildScoreBoardItemWidget(int index, ScoreBoardData scoreData) {
    final width = MediaQuery.of(context).size.width / 3;
    return Row(
      children: [
        SizedBox(
          width: width,
          child: CustomText(
            (index + 1).toString(),
            color: ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          width: width,
          child: CustomText(
            scoreData.user.userName.toString(),
            color: ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          width: width,
          child: CustomText(
            scoreData.score.toStringAsFixed(2),
            color: ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}
