import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/score_board_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/models/local/user_data.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'score_board_event.dart';
part 'score_board_state.dart';

class ScoreBoardBloc extends Bloc<ScoreBoardEvent, ScoreBoardState> {
  List<ScoreBoardData> monthScoreBoardData = [];
  String userName = "";
  double monthTotalScore = 0;
  int monthRank = 0;

  List<ScoreBoardData> allScoreBoardData = [];
  double allTotalScore = 0;
  int allRank = 0;

  ScoreBoardBloc() : super(ScoreBoardInitialState()) {
    eventBus.on<ScoreBoardRefreshEventBus>().listen((event) {
      add(ScoreBoardInitialEvent());
    });
  }

  @override
  Stream<ScoreBoardState> mapEventToState(ScoreBoardEvent event) async* {
    if (event is ScoreBoardInitialEvent) {
      final UserData currentUser = UserDataHandler.currentUser;
      userName = currentUser.userName;
      monthScoreBoardData = UserDataHandler.scoreBoardData();
      monthTotalScore = currentUser?.scoreData()?.score ?? 0;

      final int monthRankIndex = monthScoreBoardData.indexWhere(
        (score) => score.user.userId == currentUser.userId,
      );

      if (monthRankIndex != -1) {
        monthRank = monthRankIndex + 1;
      }

      allScoreBoardData = UserDataHandler.scoreBoardData(true);
      allTotalScore = currentUser?.scoreData(true)?.score ?? 0;

      final int allRankIndex = allScoreBoardData.indexWhere(
        (score) => score.user.userId == currentUser.userId,
      );

      if (allRankIndex != -1) {
        allRank = allRankIndex + 1;
      }

      yield ScoreBoardInitialState();
    }
  }
}

class ScoreBoardData {
  UserData user;
  double score;

  ScoreBoardData(this.user, this.score);
}
