part of 'score_board_bloc.dart';

class ScoreBoardState extends BaseEquatable {}

class ScoreBoardInitialState extends ScoreBoardState {
  final String error;

  ScoreBoardInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}
