part of 'subscription_bloc.dart';

class SubscriptionEvent extends BaseEquatable {}

class SubscriptionInitialEvent extends SubscriptionEvent {}
