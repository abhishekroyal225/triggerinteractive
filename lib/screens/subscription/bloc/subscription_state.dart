part of 'subscription_bloc.dart';

class SubscriptionState extends BaseEquatable {}

class SubscriptionInitialState extends SubscriptionState {
  final String error;

  SubscriptionInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}
