import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'subscription_event.dart';
part 'subscription_state.dart';

class SubscriptionBloc extends Bloc<SubscriptionEvent, SubscriptionState> {
  SubscriptionBloc() : super(SubscriptionInitialState());

  @override
  Stream<SubscriptionState> mapEventToState(
    SubscriptionEvent event,
  ) async* {
    if (event is SubscriptionInitialEvent) {
      yield SubscriptionInitialState();
    }
  }
}
