import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/authentication/authentication_bloc.dart';
import 'package:trigger_interactive_app/screens/subscription/bloc/subscription_bloc.dart';
import 'package:trigger_interactive_app/screens/subscription/cancel_subscription_dialog.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/constants.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_app_bar.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  AuthenticationBloc authenticationBloc;

  @override
  void initState() {
    super.initState();
    authenticationBloc = context.read<AuthenticationBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      color: ColorResource.GREY_E7E7E7,
      appBar: CustomAppBar(
        _getAppBarTitle(authenticationBloc.subscriptionStatus),
        subtitle: _getAppBarSubtitle(authenticationBloc.subscriptionStatus),
      ),
      body: BlocConsumer<SubscriptionBloc, SubscriptionState>(
        listener: (context, state) {
          if (state is SubscriptionInitialState) {
            if (state.error != null) {
              AppUtils.showToast(state.error);
            }
          }
        },
        builder: (context, state) {
          return _buildBody();
        },
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        if (authenticationBloc.subscriptionStatus != SubscriptionStatus.monthly)
          _buildSettingTile(
            children: [
              const Flexible(
                child: CustomText(
                  StringResource.MONTHLY_SUBSCRIPTION,
                  font: Font.HelveticaBold,
                  color: ColorResource.GREY_969696,
                ),
              ),
              const CustomText(
                "\$${Constants.MONTHLY_SUBSCRIPTION_AMOUNT}",
                font: Font.HelveticaBold,
                color: ColorResource.GREY_969696,
              ),
            ],
            onTap: () {
              // authenticationBloc.add(
              //   SubscriptionChangeEvent(SubscriptionStatus.monthly),
              // );
              // eventBus.fire(SettingsRefreshEventBus());
              // Navigator.pop(context);
            },
          ),
        _buildSettingTile(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  _getAnnualSubscriptionText(
                      authenticationBloc.subscriptionStatus),
                  font: Font.HelveticaBold,
                  color: ColorResource.GREY_969696,
                ),
                const CustomText(
                  StringResource.PAY_THE_YEAR_OFFER_TEXT,
                  color: ColorResource.GREY_969696,
                  fontSize: FontSize.smallFontSize,
                ),
              ],
            ),
            const Flexible(
              child: CustomText(
                "\$${Constants.ANNUAL_SUBSCRIPTION_AMOUNT}/month",
                font: Font.HelveticaBold,
                color: ColorResource.GREY_969696,
              ),
            ),
          ],
          onTap: () {
            // authenticationBloc.add(
            //   SubscriptionChangeEvent(SubscriptionStatus.yearly),
            // );
            // eventBus.fire(SettingsRefreshEventBus());
            // Navigator.pop(context);
          },
        ),
        if (authenticationBloc.subscriptionStatus != SubscriptionStatus.none)
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 150,
                child: CustomButton(
                  50,
                  _onCancelButtonPressed,
                  title: StringResource.CANCEL,
                  titleFont: Font.HelveticaRegular,
                  titleColor: ColorResource.GREY_969696,
                ),
              ),
            ],
          )
      ],
    );
  }

  Widget _buildSettingTile({List<Widget> children, Function() onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        constraints: const BoxConstraints(minHeight: 72),
        decoration: const BoxDecoration(
          color: ColorResource.WHITE_FFFFFF,
          border: Border(bottom: BorderSide(color: ColorResource.BLACK_6E6E6E)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: children,
        ),
      ),
    );
  }

  // Getters and setters
  String _getAppBarTitle(SubscriptionStatus status) {
    switch (status) {
      case SubscriptionStatus.none:
        return StringResource.SUBSCRIPTION;
      case SubscriptionStatus.monthly:
        return StringResource.UPDATE_SUBSCRIPTION;
      case SubscriptionStatus.yearly:
        return StringResource.UPDATE_SUBSCRIPTION;
    }
    return "";
  }

  String _getAppBarSubtitle(SubscriptionStatus status) {
    switch (status) {
      case SubscriptionStatus.none:
        return StringResource.SUBSCRIBE_FOR_FULL_ACCESS;
      case SubscriptionStatus.monthly:
        return null;
      case SubscriptionStatus.yearly:
        return null;
    }
    return "";
  }

  String _getAnnualSubscriptionText(SubscriptionStatus status) {
    switch (status) {
      case SubscriptionStatus.none:
        return StringResource.ANNUAL_SUBSCRIPTION;
      case SubscriptionStatus.monthly:
        return StringResource.SWITCH_TO_ANNUAL;
        break;
      case SubscriptionStatus.yearly:
        return "";
    }
    return "";
  }

  void _onCancelButtonPressed() {
    showDialog(
      context: context,
      builder: (context) => CancelSubscriptionDialog(
        onAcceptButtonPressed: () {
          // authenticationBloc
          //     .add(SubscriptionChangeEvent(SubscriptionStatus.none));
          // Navigator.pop(context)
          // eventBus.fire(SettingsRefreshEventBus());
          // Navigator.pop(context);
        },
      ),
    );
  }
}
