import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class CancelSubscriptionDialog extends StatelessWidget {
  final Function() onAcceptButtonPressed;
  const CancelSubscriptionDialog({this.onAcceptButtonPressed});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      title: StringResource.CANCEL_SUBSCRIPTION,
      descriptionWidget: Column(
        children: const [
          SizedBox(height: 20),
          CustomText(
            StringResource.YOU_WILL_LOSE_ACCESS_TO,
            fontSize: FontSize.baseFontSize,
            color: ColorResource.WHITE_FFFFFF,
          ),
          SizedBox(height: 20),
          CustomText(
            "- ${StringResource.ALL_ADDITIONAL_SCENARIO}",
            color: ColorResource.WHITE_FFFFFF,
          ),
          SizedBox(height: 10),
          CustomText(
            "- ${StringResource.SHOOTING_STATS_TRACKING}",
            color: ColorResource.WHITE_FFFFFF,
          ),
          SizedBox(height: 10),
          CustomText(
            "- ${StringResource.DETAILED_SCENARIO_REPORTS}",
            color: ColorResource.WHITE_FFFFFF,
          ),
          SizedBox(height: 30),
          CustomText(
            StringResource.CANCEL_YOUR_SUBSCRIPTION_TEXT,
            fontSize: FontSize.baseFontSize,
            textAlign: TextAlign.center,
            lineHeight: 1.5,
            color: ColorResource.WHITE_FFFFFF,
          ),
        ],
      ),
      acceptButtonText: StringResource.YES,
      onAcceptButtonPressed: onAcceptButtonPressed,
      cancelButtonText: StringResource.NO,
      onCancelButtonPressed: () => Navigator.pop(context),
    );
  }
}
