import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/home/bloc/home_tab_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_settings/bloc/scenario_settings_bloc.dart';
import 'package:trigger_interactive_app/screens/train/bloc/train_bloc.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class TrainScreen extends StatefulWidget {
  @override
  _TrainScreenState createState() => _TrainScreenState();
}

class _TrainScreenState extends State<TrainScreen> {
  TrainBloc trainBloc;
  HomeTabBloc homeTabBloc;

  @override
  void initState() {
    trainBloc = context.read<TrainBloc>();
    homeTabBloc = context.read<HomeTabBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorResource.WHITE_FFFFFF,
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 15,
          mainAxisSpacing: 15,
          childAspectRatio: 0.82,
        ),
        padding: const EdgeInsets.all(16),
        itemCount: trainBloc.scenarios.length,
        itemBuilder: (context, index) {
          return _buildScenarioTileWidget(trainBloc.scenarios[index]);
        },
      ),
    );
  }

  Widget _buildScenarioTileWidget(Scenario scenario) {
    return Material(
      color: scenario.isDisabled
          ? ColorResource.GREY_E7E7E7
          : ColorResource.GREY_969696,
      child: InkWell(
        highlightColor: ColorResource.RED_96262C,
        focusColor: ColorResource.RED_96262C,
        hoverColor: ColorResource.RED_96262C,
        splashColor: Colors.transparent,
        onTap: () => _onScenarioTilePressed(scenario),
        child: SizedBox(
          height: 186,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(scenario.image, height: 100),
              const SizedBox(height: 10),
              CustomText(
                scenario.label,
                color: ColorResource.WHITE_FFFFFF,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
                fontSize: FontSize.baseFontSize,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onScenarioTilePressed(Scenario scenario) {
    if (scenario.isDisabled) {
      showDialog(
        context: context,
        builder: (context) => CustomDialog(
          title: "${StringResource.COMING_SOON.toUpperCase()}!",
          description: StringResource.COMING_SOON_DESCRIPTION,
          acceptButtonText: StringResource.OK.toUpperCase(),
          onAcceptButtonPressed: () => Navigator.pop(context),
        ),
      );
      return;
    }

    if (scenario.minimumTargets > DeviceDataHandler.connectedDevices.length) {
      showDialog(
        context: context,
        builder: (context) => CustomDialog(
          title: StringResource.CONNECT_DEVICES,
          description:
              "${StringResource.YOU_MUST_CONNECT_AT_LEAST} ${scenario.minimumTargets} ${StringResource.SMART_TARGET_IN_ORDER_TEXT}",
          acceptButtonText: StringResource.CONNECT_DEVICES,
          cancelButtonText: StringResource.CANCEL,
          onAcceptButtonPressed: () {
            Navigator.pop(context);
            homeTabBloc.add(HomeTabBottomNavChangeEvent(1));
          },
          onCancelButtonPressed: () => Navigator.pop(context),
        ),
      );
      return;
    }

    Navigator.pushNamed(
      context,
      AppRoutes.SCENARIO_SETTINGS_SCREEN,
      arguments: ScenarioSettingsArguments(
        scenario,
        context.read<HomeTabBloc>(),
      ),
    );
  }
}
