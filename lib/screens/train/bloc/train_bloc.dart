import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'train_event.dart';
part 'train_state.dart';

class TrainBloc extends Bloc<TrainEvent, TrainState> {
  TrainBloc() : super(TrainInitialState());
  final List<Scenario> scenarios = [
    Scenario(
      label: StringResource.BASIC_COMBAT,
      image: ImageResource.BASIC_COMBAT_TILE,
      minimumTargets: 3,
    ),
    Scenario(
      label: StringResource.ELIMINATION,
      image: ImageResource.ELIMINATION_TILE,
      isDisabled: true,
    ),
    Scenario(
      label: StringResource.WAVES,
      image: ImageResource.WAVES_TILE,
      isDisabled: true,
    ),
    Scenario(
      label: StringResource.FRENZY,
      image: ImageResource.FRENZY_TILE,
      isDisabled: true,
    ),
    Scenario(
      label: StringResource.RUN_AND_GUN,
      image: ImageResource.RUN_AND_GUN_TILE,
      isDisabled: true,
    ),
    Scenario(
      label: StringResource.DUELING_TREE,
      image: ImageResource.DUELING_TREE_TILE,
      isDisabled: true,
    ),
  ];

  @override
  Stream<TrainState> mapEventToState(TrainEvent event) async* {
    if (event is TrainInitialEvent) {
      yield TrainInitialState();
    }
  }
}

class Scenario {
  final String label;
  final String image;
  final String instruction;

  final bool isDisabled;

  final int minimumTargets;

  Scenario({
    this.label,
    this.image,
    this.isDisabled = false,
    this.instruction,
    this.minimumTargets = 0,
  });
}
