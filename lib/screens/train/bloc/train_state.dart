part of 'train_bloc.dart';

class TrainState extends BaseEquatable {}

class TrainInitialState extends TrainState {
  final String error;

  TrainInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}
