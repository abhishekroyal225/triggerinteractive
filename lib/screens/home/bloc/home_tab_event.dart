part of 'home_tab_bloc.dart';

class HomeTabEvent extends BaseEquatable {}

class HomeTabInitialEvent extends HomeTabEvent {}

class HomeTabBottomNavChangeEvent extends HomeTabEvent {
  final int index;

  HomeTabBottomNavChangeEvent(this.index);
}

class HomeTabRefreshEvent extends HomeTabEvent {}

class HomeDevicesDisconnectedEvent extends HomeTabEvent {}
