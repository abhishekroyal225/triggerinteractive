import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/bus/device_disconnected_event_bus.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/home_state_refresh_event_bus.dart';
import 'package:trigger_interactive_app/screens/home/home_tab_screen.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'home_tab_event.dart';
part 'home_tab_state.dart';

class HomeTabBloc extends Bloc<HomeTabEvent, HomeTabState> {
  int selectedBottomNavIndex = 0;

  HomeTabBloc(HomeTabArguments args) : super(HomeTabInitialState()) {
    eventBus.on<HomeStateRefreshEventBus>().listen((event) {
      add(HomeTabRefreshEvent());
    });
    eventBus.on<DeviceDisconnectedEventBus>().listen((event) {
      add(HomeDevicesDisconnectedEvent());
    });
    selectedBottomNavIndex = args.initialTabIndex;
  }

  @override
  Stream<HomeTabState> mapEventToState(HomeTabEvent event) async* {
    if (event is HomeTabInitialEvent) {
      yield HomeTabInitialState();
    }

    if (event is HomeTabBottomNavChangeEvent) {
      selectedBottomNavIndex = event.index;

      yield HomeTabInitialState();
    }

    if (event is HomeTabRefreshEvent) {
      yield HomeTabInitialState();
    }

    if (event is HomeDevicesDisconnectedEvent) {
      yield HomeDevicesDisconnectedState();
    }
  }
}
