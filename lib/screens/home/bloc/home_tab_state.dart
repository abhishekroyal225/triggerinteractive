part of 'home_tab_bloc.dart';

class HomeTabState extends BaseEquatable {}

class HomeTabInitialState extends HomeTabState {
  @override
  bool operator ==(Object obj) => false;
}

class HomeDevicesDisconnectedState extends HomeTabState {
  @override
  bool operator ==(Object obj) => false;
}
