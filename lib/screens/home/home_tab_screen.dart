import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/screens/home/bloc/home_tab_bloc.dart';
import 'package:trigger_interactive_app/screens/manage_devices/manage_devices_screen.dart';
import 'package:trigger_interactive_app/screens/settings/settings_screen.dart';
import 'package:trigger_interactive_app/screens/train/train_screen.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_app_bar.dart';
import 'package:trigger_interactive_app/widgets/custom_bottom_nav.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';

class HomeTabScreen extends StatefulWidget {
  @override
  _HomeTabScreenState createState() => _HomeTabScreenState();
}

class _HomeTabScreenState extends State<HomeTabScreen> {
  HomeTabBloc homeTabBloc;

  List<Widget> pages;
  List<String> pageTitles;

  @override
  void initState() {
    super.initState();

    homeTabBloc = context.read<HomeTabBloc>();

    pageTitles = [
      StringResource.TRAIN,
      StringResource.MANAGE_DEVICES.toUpperCase(),
      // StringResource.STATS,
      // StringResource.SCORES,
      StringResource.SETTINGS,
    ];

    pages = [
      TrainScreen(),
      ManageDevicesScreen(),
      // Container(),
      // ScoreBoardScreen(),
      SettingsScreen()
    ];
  }

  @override
  Widget build(BuildContext context) {
    final int selectedIndex =
        context.select<HomeTabBloc, int>((bloc) => bloc.selectedBottomNavIndex);
    final Color backgroundColor = selectedIndex == 4
        ? ColorResource.WHITE_FFFFFF
        : ColorResource.GREY_E7E7E7;
    return BlocConsumer<HomeTabBloc, HomeTabState>(
      listener: (context, state) {
        if (state is HomeDevicesDisconnectedState) {
          _showDeviceDisconnectedDialog();
        }
      },
      builder: (context, state) {
        return CustomScaffold(
          color: backgroundColor,
          appBar: CustomAppBar(
            pageTitles[homeTabBloc.selectedBottomNavIndex],
            connectedDevices: DeviceDataHandler.connectedDevices.length,
          ),
          bottomNavigationBar: CustomBottomNav(
            onTap: _onBottomNavItemTapped,
            selectedIndex: homeTabBloc.selectedBottomNavIndex,
          ),
          body: Column(children: [
            Expanded(child: pages[homeTabBloc.selectedBottomNavIndex])
          ]),
        );
      },
    );
  }

  void _showDeviceDisconnectedDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: StringResource.DEVICE_DISCONNECTED.toUpperCase(),
          description: StringResource.DEVICE_DISCONNECTED_DESCRIPTION,
          acceptButtonText: StringResource.MANAGE_DEVICES,
          cancelButtonText: StringResource.OK,
          onCancelButtonPressed: () {
            Navigator.pop(context);
          },
          onAcceptButtonPressed: () {
            homeTabBloc.add(HomeTabBottomNavChangeEvent(1));
            Navigator.pop(context);
          },
        );
      },
    );
  }

  void _onBottomNavItemTapped(int index) {
    homeTabBloc.add(HomeTabBottomNavChangeEvent(index));
  }
}

class HomeTabArguments {
  final int initialTabIndex;
  HomeTabArguments({this.initialTabIndex = 0});
}
