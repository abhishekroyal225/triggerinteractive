import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/settings_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  List<String> hitDetectionSensitivityList = [];
  String selectedHitDetectionSensitivity;

  List<String> minutesToPowerDownOptionList = [];
  String selectedMinutesToPowerOption;

  bool isSoundEnabled = false;
  bool shouldReceiveNotification = false;
  bool shouldReceivePromotions = false;

  String currentUserName;

  SettingsBloc() : super(SettingsInitialState()) {
    eventBus.on<SettingsRefreshEventBus>().listen((event) {
      add(SettingsInitialEvent());
    });
  }

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is SettingsInitialEvent) {
      hitDetectionSensitivityList = ["Low", "Medium", "High"];
      selectedHitDetectionSensitivity = UserDataHandler.settings?.sensitivity ??
          hitDetectionSensitivityList.first;

      minutesToPowerDownOptionList = ["30", "60", "120", "None"];
      selectedMinutesToPowerOption =
          (UserDataHandler.settings?.minutesToPowerDown == 0
                  ? "None"
                  : UserDataHandler.settings.minutesToPowerDown.toString()) ??
              minutesToPowerDownOptionList.first;

      isSoundEnabled = UserDataHandler.settings?.sound ?? false;
      currentUserName = UserDataHandler.currentUser?.userName ?? "";
      yield SettingsInitialState();
    }

    if (event is SettingsHitDetectionChangedEvent) {
      selectedHitDetectionSensitivity = event.selectedHitDetection;
      UserDataHandler.updateSetting(
        sensitivity: selectedHitDetectionSensitivity,
      );
      yield SettingsHitDetectionChangedState(selectedHitDetectionSensitivity);
    }

    if (event is SettingsPowerDownTimeChangedEvent) {
      selectedMinutesToPowerOption = event.selectedMinutesToPowerOption;

      final int idleTime = int.tryParse(selectedMinutesToPowerOption) ?? 0;

      UserDataHandler.updateSetting(minutesToPowerDown: idleTime);

      for (final BaseDevice target
          in DeviceDataHandler.selectedHub?.targets ?? []) {
        DeviceDataHandler.selectedHub.updateSettings(idleTime, target.mac);
      }
      yield SettingsMinutesToPowerDownChangedState(
        selectedMinutesToPowerOption,
      );
    }

    if (event is SettingsEnableSoundValueFlippedEvent) {
      isSoundEnabled = !isSoundEnabled;
      UserDataHandler.updateSetting(sound: isSoundEnabled);
      yield SettingsSoundToggleState(isSoundEnabled);
    }
  }
}
