part of 'settings_bloc.dart';

class SettingsState extends BaseEquatable {}

class SettingsInitialState extends SettingsState {
  final String error;

  SettingsInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}

class SettingsSoundToggleState extends SettingsState {
  final bool isEnabled;

  SettingsSoundToggleState(this.isEnabled);

  @override
  List<Object> get props => [isEnabled];
}

class SettingsHitDetectionChangedState extends SettingsState {
  final String selectedValue;

  SettingsHitDetectionChangedState(this.selectedValue);

  @override
  List<Object> get props => [selectedValue];
}

class SettingsMinutesToPowerDownChangedState extends SettingsState {
  final String selectedValue;

  SettingsMinutesToPowerDownChangedState(this.selectedValue);

  @override
  List<Object> get props => [selectedValue];
}
