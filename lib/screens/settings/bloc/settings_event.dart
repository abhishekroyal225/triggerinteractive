part of 'settings_bloc.dart';

class SettingsEvent extends BaseEquatable {}

class SettingsInitialEvent extends SettingsEvent {}

class SettingsSubscriptionSelectedEvent extends SettingsEvent {
  final String selectedSubscription;

  SettingsSubscriptionSelectedEvent(this.selectedSubscription);
}

class SettingsHitDetectionChangedEvent extends SettingsEvent {
  final String selectedHitDetection;

  SettingsHitDetectionChangedEvent(this.selectedHitDetection);
}

class SettingsPowerDownTimeChangedEvent extends SettingsEvent {
  final String selectedMinutesToPowerOption;

  SettingsPowerDownTimeChangedEvent(this.selectedMinutesToPowerOption);
}

class SettingsEnableSoundValueFlippedEvent extends SettingsEvent {}
