import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/settings/bloc/settings_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/constants.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class SettingsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final bool isSubscribed = context.select<AuthenticationBloc, bool>(
    //   (bloc) => bloc.subscriptionStatus != SubscriptionStatus.none,
    // );

    final String currentUserName =
        context.select<SettingsBloc, String>((bloc) => bloc.currentUserName);

    return BlocListener<SettingsBloc, SettingsState>(
      listener: (context, state) {
        if (state is SettingsInitialState) {
          if (state.error != null) AppUtils.showToast(state.error);
        }
      },
      child: ListView(
        children: [
          const _SoundSettingTile(),
          const _HitDetectionDropDown(),
          const _MinutesToPowerDownDropDown(),
          // if (isSubscribed)
          _SettingTile(
            onPress: () => _onEditUsernameOptionSelected(context),
            children: [
              const Flexible(
                child: CustomText(
                  StringResource.EDIT_USERNAME,
                  font: Font.HelveticaBold,
                  color: ColorResource.GREY_969696,
                ),
              ),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Flexible(
                      child: CustomText(
                        currentUserName,
                        color: ColorResource.GREY_969696,
                      ),
                    ),
                    const SizedBox(width: 5),
                    const Icon(
                      Icons.arrow_forward_ios_outlined,
                      color: ColorResource.GREY_969696,
                      size: FontSize.smallFontSize,
                    )
                  ],
                ),
              )
            ],
          ),
          _SettingTile(
            onPress: _onSignUpFormEmailOptionSelected,
            children: [
              Flexible(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      CustomText(
                        StringResource.SIGN_UP_FOR_EMAILS,
                        font: Font.HelveticaBold,
                        color: ColorResource.GREY_969696,
                      ),
                      CustomText(
                        StringResource.RECEIVE_FIRMWARE_NOTIFICATION_TEXT,
                        color: ColorResource.GREY_969696,
                        fontSize: FontSize.smallFontSize,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          // const _UpdateSubscriptionTile(),
          Container(
            padding: const EdgeInsets.all(20),
            child: const CustomText(
              "${StringResource.APP_VERSION}: ${Constants.APP_VERSION}",
              fontSize: FontSize.smallFontSize,
              color: ColorResource.GREY_969696,
            ),
          )
        ],
      ),
    );
  }

  void _onSignUpFormEmailOptionSelected() {
    AppUtils.openUrl(Constants.SIGN_UP_NOTIFICATION_EMAIL);
  }

  void _onEditUsernameOptionSelected(BuildContext context) {
    Navigator.pushNamed(context, AppRoutes.EDIT_USERNAME_SCREEN);
  }
}

class _SettingTile extends StatelessWidget {
  final void Function() onPress;
  final List<Widget> children;

  const _SettingTile({Key key, this.onPress, this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        constraints: const BoxConstraints(minHeight: 72),
        decoration: const BoxDecoration(
          color: ColorResource.WHITE_FFFFFF,
          border: Border(bottom: BorderSide(color: ColorResource.BLACK_6E6E6E)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: children,
        ),
      ),
    );
  }
}

class _SoundSettingTile extends StatelessWidget {
  const _SoundSettingTile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SettingsBloc settingsBloc = context.read<SettingsBloc>();

    final bool isSoundEnabled = context.select<SettingsBloc, bool>(
      (bloc) => bloc.isSoundEnabled,
    );

    return _SettingTile(
      children: [
        const Flexible(
          child: CustomText(
            StringResource.SOUND,
            font: Font.HelveticaBold,
            color: ColorResource.GREY_969696,
          ),
        ),
        Switch(
          value: isSoundEnabled,
          onChanged: (val) => _onEnabledSoundSwitchFlipped(val, settingsBloc),
          activeColor: ColorResource.WHITE_FFFFFF,
          activeTrackColor: ColorResource.RED_96262C,
          inactiveTrackColor: ColorResource.GREY_969696,
        )
      ],
    );
  }

  void _onEnabledSoundSwitchFlipped(bool value, SettingsBloc settingsBloc) {
    settingsBloc.add(SettingsEnableSoundValueFlippedEvent());
  }
}

class _HitDetectionDropDown extends StatelessWidget {
  const _HitDetectionDropDown();

  @override
  Widget build(BuildContext context) {
    final SettingsBloc settingsBloc = context.read<SettingsBloc>();

    final String selectedValue = context.select<SettingsBloc, String>(
      (bloc) => bloc.selectedHitDetectionSensitivity,
    );

    final Iterable<DropdownMenuItem<String>> hitDetectionWidgets =
        settingsBloc.hitDetectionSensitivityList.map(
      (item) => DropdownMenuItem<String>(
        value: item.toLowerCase(),
        child: CustomText(item, color: ColorResource.GREY_969696),
      ),
    );

    return _SettingTile(
      children: [
        const Flexible(
          child: CustomText(
            StringResource.HIT_DETECTION_SENSITIVITY,
            font: Font.HelveticaBold,
            color: ColorResource.GREY_969696,
          ),
        ),
        SizedBox(
          width: 100,
          child: DropdownButton<String>(
            isExpanded: true,
            value: selectedValue,
            items: hitDetectionWidgets.toList(),
            onChanged: (val) =>
                _onHitDetectionSensitivityChanged(val, settingsBloc),
          ),
        )
      ],
    );
  }

  void _onHitDetectionSensitivityChanged(
    String value,
    SettingsBloc settingsBloc,
  ) {
    settingsBloc.add(SettingsHitDetectionChangedEvent(value));
  }
}

class _MinutesToPowerDownDropDown extends StatelessWidget {
  const _MinutesToPowerDownDropDown();

  @override
  Widget build(BuildContext context) {
    final SettingsBloc settingsBloc = context.read<SettingsBloc>();

    final String selectedValue = context.select<SettingsBloc, String>(
      (bloc) => bloc.selectedMinutesToPowerOption,
    );

    final Iterable<DropdownMenuItem<String>> minutesToPowerDownWidgets =
        settingsBloc.minutesToPowerDownOptionList.map(
      (item) => DropdownMenuItem<String>(
        value: item,
        child: CustomText(
          item.toString(),
          color: ColorResource.GREY_969696,
        ),
      ),
    );

    return _SettingTile(
      children: [
        const Flexible(
          child: CustomText(
            StringResource.MINUTES_TO_POWER_DOWN,
            font: Font.HelveticaBold,
            color: ColorResource.GREY_969696,
          ),
        ),
        SizedBox(
          width: 100,
          child: DropdownButton<String>(
            isExpanded: true,
            value: selectedValue,
            items: minutesToPowerDownWidgets.toList(),
            onChanged: (val) =>
                _onMinutesToPowerDownValueChanged(val, settingsBloc),
          ),
        )
      ],
    );
  }

  void _onMinutesToPowerDownValueChanged(
    String value,
    SettingsBloc settingsBloc,
  ) {
    settingsBloc.add(SettingsPowerDownTimeChangedEvent(value));
  }
}
// TODO : Delete or Uncommented when subscription is used.
// class _UpdateSubscriptionTile extends StatelessWidget {
//   const _UpdateSubscriptionTile();

//   @override
//   Widget build(BuildContext context) {
//     final bool isSubscribed = context.select<AuthenticationBloc, bool>(
//       (bloc) => bloc.subscriptionStatus != SubscriptionStatus.none,
//     );
//     final bool isAnnualSubscription = context.select<AuthenticationBloc, bool>(
//       (bloc) => bloc.subscriptionStatus == SubscriptionStatus.yearly,
//     );

//     if (isSubscribed) {
//       return _SettingTile(
//         onPress: () => isAnnualSubscription
//             ? _onCancelSubscriptionOptionSelected(context)
//             : _onSubscribedOptionSelected(context),
//         children: [
//           CustomText(
//             isAnnualSubscription
//                 ? StringResource.CANCEL_SUBSCRIPTION
//                 : StringResource.UPDATE_SUBSCRIPTION,
//             font: Font.HelveticaBold,
//             color: ColorResource.GREY_969696,
//           ),
//           Row(
//             children: [
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: const [
//                   CustomText(
//                     StringResource.RENEWAL_DATE,
//                     color: ColorResource.GREY_969696,
//                     fontSize: FontSize.smallFontSize,
//                   ),
//                   CustomText(
//                     StringResource.DEMO_DATE,
//                     color: ColorResource.GREY_969696,
//                     fontSize: FontSize.smallFontSize,
//                   ),
//                 ],
//               ),
//               const SizedBox(width: 5),
//               const Icon(
//                 Icons.arrow_forward_ios_outlined,
//                 color: ColorResource.GREY_969696,
//                 size: FontSize.smallFontSize,
//               )
//             ],
//           )
//         ],
//       );
//     }

//     return _SettingTile(
//       onPress: () => _onSubscribedOptionSelected(context),
//       children: const [
//         CustomText(
//           StringResource.SUBSCRIBE_FOR_FULL_ACCESS,
//           font: Font.HelveticaBold,
//           color: ColorResource.GREY_969696,
//         ),
//       ],
//     );
//   }

//   void _onSubscribedOptionSelected(BuildContext context) {
//     Navigator.pushNamed(context, AppRoutes.SUBSCRIPTION_SCREEN);
//   }

//   void _onCancelSubscriptionOptionSelected(BuildContext context) {
//     showDialog(
//       context: context,
//       builder: (context) => CancelSubscriptionDialog(
//         onAcceptButtonPressed: () {},
//       ),
//     );
//   }
// }
