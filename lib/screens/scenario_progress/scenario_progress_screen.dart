import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/models/local/game/score_data.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/bloc/scenario_progress_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/utils/time_utils.dart';
import 'package:trigger_interactive_app/widgets/custom_app_bar.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class ScenarioProgressScreen extends StatefulWidget {
  @override
  _ScenarioProgressScreenState createState() => _ScenarioProgressScreenState();
}

class _ScenarioProgressScreenState extends State<ScenarioProgressScreen> {
  ScenarioProgressBloc scenarioProgressBloc;

  @override
  void initState() {
    super.initState();
    scenarioProgressBloc = context.read<ScenarioProgressBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ScenarioProgressBloc, ScenarioProgressState>(
      listener: (context, state) {
        if (state is ScenarioProgressInitialState) {
          if (state.error != null) AppUtils.showToast(state.error);
        }
      },
      buildWhen: (_, state) => state is! ScenarioProgressCountDownRefreshState,
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async => false,
          child: CustomScaffold(
            appBar: _buildAppBarWidget(),
            body: scenarioProgressBloc.showCountDown
                ? _buildCountDownWidget()
                : _buildBody(),
          ),
        );
      },
    );
  }

  PreferredSizeWidget _buildAppBarWidget() {
    if (scenarioProgressBloc.showCountDown) {
      return AppBar(
        centerTitle: true,
        brightness: Brightness.dark,
        leading: Container(),
        backgroundColor: ColorResource.BLACK_333333,
        title: const CustomText(
          StringResource.SCENARIO_STARTING_IN,
          font: Font.HelveticaBold,
          color: ColorResource.GREY_969696,
        ),
      );
    }

    String appBarTitle = StringResource.IN_PROGRESS;

    if (scenarioProgressBloc.isPaused) {
      appBarTitle = StringResource.PAUSED;
    }
    if (scenarioProgressBloc.isCompleted) {
      appBarTitle = StringResource.COMPLETED;
    }
    return CustomAppBar(
      appBarTitle,
      connectedDevices: DeviceDataHandler.connectedDevices.length,
      hideBackButton: true,
    );
  }

  Widget _buildCountDownWidget() {
    return BlocBuilder<ScenarioProgressBloc, ScenarioProgressState>(
      buildWhen: (prevState, state) {
        return state is ScenarioProgressCountDownRefreshState;
      },
      builder: (context, state) {
        return Container(
          color: ColorResource.BLACK_6E6E6E,
          child: Center(
            child: CustomText(
              scenarioProgressBloc.countDown.toString(),
              font: Font.HelveticaBold,
              fontSize: FontSize.countdownFontSize,
              color: ColorResource.WHITE_FFFFFF,
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        _buildTargetStatsWidget(),
        _buildScoreWidget(),
        _buildCounterWidget(),
        Expanded(child: _buildScoreTableWidget()),
        _buildControlButtonWidgets()
      ],
    );
  }

  Widget _buildTargetStatsWidget() {
    return Row(
      children: [
        _buildTargetStatItemWidget(
            StringResource.TARGETS_HIT, scenarioProgressBloc.targetsHit),
        _buildVerticalDividerWidget(),
        _buildTargetStatItemWidget(
          StringResource.TARGETS_MISSED,
          scenarioProgressBloc.targetsMissed,
        ),
        _buildVerticalDividerWidget(),
        _buildTargetStatItemWidget(
          StringResource.TOTAL_TARGETS,
          scenarioProgressBloc.totalTargets,
        ),
        _buildVerticalDividerWidget(),
        _buildTargetStatItemWidget(
          "${StringResource.TARGETS_HIT} %",
          (scenarioProgressBloc.targetsHit * 100) ~/
              scenarioProgressBloc.totalTargets,
        ),
      ],
    );
  }

  Widget _buildTargetStatItemWidget(String label, int value) {
    return Expanded(
      child: Column(
        children: [
          CustomText(
            label.replaceFirst(" ", "\n"),
            textAlign: TextAlign.center,
            color: ColorResource.RED_96262C,
          ),
          const SizedBox(height: 5),
          CustomText(value.toString(), font: Font.HelveticaBold),
        ],
      ),
    );
  }

  Widget _buildVerticalDividerWidget() {
    return Container(
      width: 1,
      height: 70,
      color: ColorResource.GREY_E7E7E7,
      margin: const EdgeInsets.symmetric(vertical: 18),
    );
  }

  Widget _buildScoreWidget() {
    double averageReactionTime = 0;
    double score = 0;
    final List<double> splitTimes = scenarioProgressBloc.scoreData?.reversed
        ?.where((score) => !score.isMissed)
        ?.map((e) => e.splitTime)
        ?.toList();

    if (scenarioProgressBloc.isCompleted && (splitTimes?.isNotEmpty ?? false)) {
      double totalReactionTime = 0;

      averageReactionTime = splitTimes.first / 1000;
      if (splitTimes.length > 1) {
        totalReactionTime = splitTimes.reduce(
          (value, element) => value + element,
        );

        averageReactionTime = (totalReactionTime / splitTimes.length) / 1000;
      }

      score = ((100 / scenarioProgressBloc.totalTargets) *
              scenarioProgressBloc.targetsHit) +
          ((10 - averageReactionTime) * 10);
    }

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      color: ColorResource.GREY_E7E7E7,
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CustomText(
                  StringResource.REACTION_TIME,
                  color: ColorResource.RED_96262C,
                  font: Font.HelveticaBold,
                  fontSize: 20,
                ),
                const SizedBox(height: 5),
                CustomText(
                  "${averageReactionTime.toStringAsFixed(3)}s",
                  fontSize: FontSize.reactionScoreFontSize,
                  font: Font.HelveticaBold,
                )
              ],
            ),
          ),
          Container(width: 1, color: ColorResource.WHITE_FFFFFF),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CustomText(
                  StringResource.SCORE,
                  color: ColorResource.RED_96262C,
                  font: Font.HelveticaBold,
                  fontSize: 20,
                ),
                const SizedBox(height: 5),
                CustomText(
                  score.toStringAsFixed(3),
                  fontSize: FontSize.reactionScoreFontSize,
                  font: Font.HelveticaBold,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCounterWidget() {
    return BlocBuilder<ScenarioProgressBloc, ScenarioProgressState>(
      builder: (context, state) {
        return Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          color: ColorResource.RED_96262C,
          child: Center(
            child: CustomText(
              TimeUtils.formatMMSS(scenarioProgressBloc.trainingTimer),
              fontSize: FontSize.scenarioTimerFontSize,
              color: ColorResource.WHITE_FFFFFF,
              font: Font.HelveticaBold,
            ),
          ),
        );
      },
    );
  }

  Widget _buildScoreTableWidget() {
    final width = MediaQuery.of(context).size.width / 3;

    return Column(
      children: [
        const SizedBox(height: 23),
        Row(
          children: [
            SizedBox(
              width: width,
              child: const CustomText(
                StringResource.ORDER,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: width,
              child: const CustomText(
                StringResource.TARGET,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: width,
              child: const CustomText(
                StringResource.SPLIT_TIME,
                font: Font.HelveticaBold,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
        const SizedBox(height: 23),
        Expanded(
          child: ListView.separated(
            controller: scenarioProgressBloc.scrollController,
            padding: const EdgeInsets.symmetric(vertical: 15),
            separatorBuilder: (_, __) => const SizedBox(height: 20),
            itemCount: scenarioProgressBloc.scoreData.length,
            itemBuilder: (context, index) {
              return _buildScoreTableItemWidget(
                scenarioProgressBloc.scoreData[index],
              );
            },
          ),
        )
      ],
    );
  }

  Widget _buildScoreTableItemWidget(ScoreData score) {
    final width = MediaQuery.of(context).size.width / 3;

    return Row(
      children: [
        SizedBox(
          width: width,
          child: CustomText(
            score.order.toString(),
            color: score.isMissed
                ? ColorResource.RED_96262C
                : ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          width: width,
          child: CustomText(
            score.target.toString(),
            color: score.isMissed
                ? ColorResource.RED_96262C
                : ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(
          width: width,
          child: CustomText(
            score.isMissed
                ? "-"
                : "${((score.splitTime) / 1000).toStringAsFixed(3) ?? "-"}s",
            color: score.isMissed
                ? ColorResource.RED_96262C
                : ColorResource.GREY_969696,
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  Widget _buildControlButtonWidgets() {
    if (!scenarioProgressBloc.isPaused) {
      return Container(
        height: 70,
        color: ColorResource.GREY_E7E7E7,
        padding: const EdgeInsets.symmetric(horizontal: 17, vertical: 8),
        child: _buildCustomButtonWidget(
          icon: ImageResource.PAUSE_ICON,
          onPressed: _onPauseButtonPressed,
        ),
      );
    }

    return Container(
      height: 70,
      padding: const EdgeInsets.symmetric(horizontal: 17),
      color: ColorResource.GREY_E7E7E7,
      child: Row(
        children: [
          Expanded(
            child: _buildCustomButtonWidget(
              text: StringResource.EXIT,
              onPressed: _onExitButtonPressed,
            ),
          ),
          const SizedBox(width: 5),
          Expanded(
            child: scenarioProgressBloc.isCompleted
                ? _buildCustomButtonWidget(
                    icon: ImageResource.RESTART_ICON,
                    onPressed: _onRestartButtonPressed,
                  )
                : _buildCustomButtonWidget(
                    icon: ImageResource.PLAY_ICON,
                    onPressed: _onPlayButtonPressed,
                  ),
          ),
          const SizedBox(width: 5),
          Expanded(
            child: _buildCustomButtonWidget(
              text: StringResource.SETUP,
              onPressed: _onSetupButtonPressed,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCustomButtonWidget({
    String text,
    String icon,
    void Function() onPressed,
  }) {
    return Material(
      child: InkWell(
        onTap: onPressed,
        highlightColor: ColorResource.WHITE_F2F2F2,
        focusColor: ColorResource.WHITE_F2F2F2,
        hoverColor: ColorResource.WHITE_F2F2F2,
        splashColor: Colors.transparent,
        child: SizedBox(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (icon != null) SvgPicture.asset(icon, height: 20),
              if (text != null)
                CustomText(
                  text.toUpperCase(),
                  font: Font.HelveticaBold,
                  color: ColorResource.RED_96262C,
                )
            ],
          ),
        ),
      ),
    );
  }

  void _onPauseButtonPressed() {
    scenarioProgressBloc.add(ScenarioProgressTogglePlayEvent());
  }

  void _onPlayButtonPressed() {
    scenarioProgressBloc.add(ScenarioProgressTogglePlayEvent());
  }

  void _onRestartButtonPressed() {
    scenarioProgressBloc.add(ScenarioProgressRestartEvent());
  }

  void _onSetupButtonPressed() {
    Navigator.popUntil(
      context,
      ModalRoute.withName(AppRoutes.SCENARIO_SETTINGS_SCREEN),
    );
  }

  void _onExitButtonPressed() {
    Navigator.popUntil(
      context,
      ModalRoute.withName(AppRoutes.HOME_SCREEN),
    );
  }

  @override
  Future<void> dispose() async {
    scenarioProgressBloc.dispose();
    scenarioProgressBloc.close();
    super.dispose();
  }
}
