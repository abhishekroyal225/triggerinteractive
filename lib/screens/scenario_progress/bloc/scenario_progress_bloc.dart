import 'dart:async';
import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/ble/data/ble_data_parser.dart';
import 'package:trigger_interactive_app/ble/data/ble_data_writer.dart';
import 'package:trigger_interactive_app/ble/data/responses/game_event_response.dart';
import 'package:trigger_interactive_app/ble/device/base_device.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/score_board_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/models/local/game/score_data.dart';
import 'package:trigger_interactive_app/models/local/game/session_data.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/utils/audio_resource.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'scenario_progress_event.dart';
part 'scenario_progress_state.dart';

class ScenarioProgressBloc
    extends Bloc<ScenarioProgressEvent, ScenarioProgressState> {
  ScenarioPreset _selectedPreset;

  bool showCountDown = true;
  bool isCompleted = false;
  bool isPaused = false;
  List<ScoreData> scoreData = [];

  int countDown = 5;
  int trainingTimer = 0;

  Timer _countDownTimerObj;
  Timer _trainingTimerObj;

  int targetsHit = 0;
  int totalTargets;
  int targetsMissed = 0;
  double scoreText = 0;

  int _eventsCompleted = 0;

  StreamSubscription _eventStreamSubscription;

  final ScrollController scrollController = ScrollController();

  final AudioCache _audioPlayer = AudioCache(prefix: 'assets/audio/');

  ScenarioProgressBloc(ScenarioProgressArguments args)
      : super(ScenarioProgressInitialState()) {
    _selectedPreset = args.selectedPreset;
    totalTargets =
        _selectedPreset.simultaneousTarget * _selectedPreset.targetPresentation;

    if (DeviceDataHandler.selectedHub != null) {
      _listenToEventData();
    }
  }

  @override
  Stream<ScenarioProgressState> mapEventToState(
    ScenarioProgressEvent event,
  ) async* {
    if (event is ScenarioProgressInitialEvent) {
      if (DeviceDataHandler.selectedHub != null) {
        DeviceDataHandler.selectedHub
            .requestGameControl(GameControls.clear)
            .then((value) => _requestGameEvent());
      }

      if (UserDataHandler.settings.sound ?? false) {
        _audioPlayer.play(AudioResource.COUNTER_BEEP);
      }
      _countDownTimerObj = Timer.periodic(
        const Duration(seconds: 1),
        _countDownCallback,
      );

      // yield ScenarioProgressInitialState();
    }
    if (event is ScenarioProgressStopCountDownEvent) {
      showCountDown = false;

      if (DeviceDataHandler.selectedHub != null) {
        await DeviceDataHandler.selectedHub
            .requestGameControl(GameControls.start);
      }

      _trainingTimerObj = Timer.periodic(
        const Duration(seconds: 1),
        _trainingTimerCallback,
      );

      yield ScenarioProgressInitialState();
    }

    if (event is ScenarioProgressTogglePlayEvent) {
      isPaused = !isPaused;
      if (isPaused ?? true) {
        await DeviceDataHandler.selectedHub
            ?.requestGameControl(GameControls.pause);
        _trainingTimerObj.cancel();
        _trainingTimerObj = null;
      } else {
        _trainingTimerObj = Timer.periodic(
          const Duration(seconds: 1),
          _trainingTimerCallback,
        );

        await DeviceDataHandler.selectedHub
            .requestGameControl(GameControls.start);
      }

      yield ScenarioProgressInitialState();
    }

    if (event is ScenarioProgressCounterRefreshEvent) {
      yield ScenarioProgressCountDownRefreshState();
    }

    if (event is ScenarioProgressDataReceivedEvent) {
      yield ScenarioProgressInitialState();
    }

    if (event is ScenarioProgressRestartEvent) {
      showCountDown = true;
      isCompleted = false;
      isPaused = false;
      scoreData = [];

      countDown = 5;
      trainingTimer = 0;
      _countDownTimerObj = null;
      _trainingTimerObj = null;
      targetsHit = 0;

      targetsMissed = 0;
      scoreText = 0;

      _eventsCompleted = 0;
      yield ScenarioProgressInitialState();
      add(ScenarioProgressInitialEvent());
    }
  }

  void _countDownCallback(Timer timer) {
    countDown--;
    if (UserDataHandler.settings.sound ?? false) {
      _audioPlayer.play(AudioResource.COUNTER_BEEP);
    }
    add(ScenarioProgressCounterRefreshEvent());
    if (countDown == 0) {
      timer.cancel();
      add(ScenarioProgressStopCountDownEvent());
    }
  }

  void _trainingTimerCallback(Timer timer) {
    trainingTimer++;
    add(ScenarioProgressCounterRefreshEvent());
  }

  void _listenToEventData() {
    _eventStreamSubscription =
        DeviceDataHandler.selectedHub.eventDataStream.listen((event) {
      if (event.type == BLEDataType.game_event) {
        final eventData = event.data as GameEventResponse;

        final String deviceName =
            DeviceDataHandler.getDevice(eventData.mac)?.deviceName ??
                StringResource.UNKNOWN;

        if (eventData.hitDetected > 0) {
          targetsHit += eventData.hitDetected;

          final int splitTime = eventData.acquisitionTime;
          scoreData.add(
            ScoreData(
              order: scoreData.length + 1,
              target: deviceName,
              splitTime: splitTime.toDouble(),
            ),
          );
        }

        if (eventData.eventStatus != EventStatus.progress) {
          _eventsCompleted++;

          if (eventData.eventStatus == EventStatus.timed_out) {
            final int splitTime = eventData.currentActiveTime;
            targetsMissed++;

            scoreData.add(
              ScoreData(
                order: scoreData.length + 1,
                target: deviceName,
                splitTime: splitTime.toDouble(),
                isMissed: true,
              ),
            );
            add(ScenarioProgressDataReceivedEvent());
          }

          scrollController.animateTo(
            scrollController.position.maxScrollExtent,
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeOut,
          );

          if (_eventsCompleted >= totalTargets) {
            isCompleted = true;
            isPaused = true;
            _trainingTimerObj?.cancel();
            _trainingTimerObj = null;

            targetsMissed = totalTargets - targetsHit;

            UserDataHandler.addNewSession(
              score: _getScore(),
              sessionData: SessionData(
                targets: scoreData,
                targetsMiss: targetsMissed,
                tartgetsTotal: totalTargets,
                tartgetsHit: targetsHit,
              ),
            );
            eventBus.fire(ScoreBoardRefreshEventBus());
          }
        }
        add(ScenarioProgressDataReceivedEvent());
      }
    });
  }

  Future<void> _requestGameEvent() async {
    final List<TargetEvent> targetEvents = [];

    for (int eventI = 0;
        eventI < _selectedPreset.targetPresentation;
        eventI++) {
      final List<BaseDevice> targets =
          List.from(DeviceDataHandler.connectedDevices)..shuffle();

      for (int i = 0;
          i < min(_selectedPreset.simultaneousTarget, targets.length);
          i++) {
        targetEvents.add(TargetEvent(targets[i].mac, eventI));

        if (targetEvents.length >= 5) {
          DeviceDataHandler.selectedHub
              .requestGameRequest(_selectedPreset, targetEvents);

          targetEvents.clear();
        }
      }
    }
    DeviceDataHandler.selectedHub
        .requestGameRequest(_selectedPreset, targetEvents);
  }

  double _getScore() {
    double score = 0;
    final List<double> splitTimes = scoreData?.reversed
        ?.where((score) => !score.isMissed)
        ?.map((e) => e.splitTime)
        ?.toList();
    if (isCompleted && (splitTimes?.isNotEmpty ?? false)) {
      double totalReactionTime = 0;

      double averageReactionTime = splitTimes.first / 1000;
      if (splitTimes.length > 1) {
        totalReactionTime = splitTimes.reduce(
          (value, element) => value + element,
        );

        averageReactionTime = (totalReactionTime / splitTimes.length) / 1000;
      }

      score = ((100 / totalTargets) * targetsHit) +
          ((10 - averageReactionTime) * 10);
    }

    return score;
  }

  Future<void> dispose() async {
    _trainingTimerObj?.cancel();
    _countDownTimerObj?.cancel();
    _eventStreamSubscription?.cancel();
  }
}

class ScenarioProgressArguments {
  final ScenarioPreset selectedPreset;
  ScenarioProgressArguments({@required this.selectedPreset});
}

class TargetEvent {
  String targetMac;
  int eventId;
  TargetEvent(this.targetMac, this.eventId);
}
