part of 'scenario_progress_bloc.dart';

class ScenarioProgressState extends BaseEquatable {}

class ScenarioProgressInitialState extends ScenarioProgressState {
  final String error;

  ScenarioProgressInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}

class ScenarioProgressStopCountDownState extends ScenarioProgressState {}

class ScenarioProgressCountDownRefreshState extends ScenarioProgressState {
  @override
  bool operator ==(Object obj) => false;
}
