part of 'scenario_progress_bloc.dart';

class ScenarioProgressEvent extends BaseEquatable {}

class ScenarioProgressInitialEvent extends ScenarioProgressEvent {}

class ScenarioProgressStopCountDownEvent extends ScenarioProgressEvent {}

class ScenarioProgressDataReceivedEvent extends ScenarioProgressEvent {}

class ScenarioProgressCounterRefreshEvent extends ScenarioProgressEvent {}

class ScenarioProgressTogglePlayEvent extends ScenarioProgressEvent {}

class ScenarioProgressRestartEvent extends ScenarioProgressEvent {}
