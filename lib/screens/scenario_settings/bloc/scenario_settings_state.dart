part of 'scenario_settings_bloc.dart';

class ScenarioSettingsState extends BaseEquatable {}

class ScenarioSettingsInitialState extends ScenarioSettingsState {
  final String error;

  ScenarioSettingsInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}
