part of 'scenario_settings_bloc.dart';

class ScenarioSettingsEvent extends BaseEquatable {}

class ScenarioSettingsInitialEvent extends ScenarioSettingsEvent {}

class ScenarioSettingsLevelChangeEvent extends ScenarioSettingsEvent {
  final int settingIndex;
  final bool isReduced;

  ScenarioSettingsLevelChangeEvent(this.settingIndex, {this.isReduced = false});
}

class ScenarioSettingsPresetChangeEvent extends ScenarioSettingsEvent {
  final String preset;

  ScenarioSettingsPresetChangeEvent(this.preset);
}

class ScenarioSettingsUserSelectEvent extends ScenarioSettingsEvent {
  final String selectedUserId;

  ScenarioSettingsUserSelectEvent(this.selectedUserId);
}

class ScenarioSettingsToggleNewPresetEvent extends ScenarioSettingsEvent {
  final bool isEdit;
  final bool showTextField;
  final bool readOnly;
  final String label;

  ScenarioSettingsToggleNewPresetEvent(
      {this.showTextField = false,
      this.isEdit = false,
      this.readOnly = true,
      this.label});
}

class ScenarioSettingsSavePresetEvent extends ScenarioSettingsEvent {}

class ScenarioSettingsDeleteUserPresetEvent extends ScenarioSettingsEvent {
  final String preset;

  ScenarioSettingsDeleteUserPresetEvent(this.preset);
}

class ScenarioSettingsRetainUnsavedEvent extends ScenarioSettingsEvent {}

class ScenarioSettingsAddNewUserEvent extends ScenarioSettingsEvent {
  final String newUserName;

  ScenarioSettingsAddNewUserEvent(this.newUserName);
}
