import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:trigger_interactive_app/bus/event_bus.dart';
import 'package:trigger_interactive_app/bus/score_board_refresh_event_bus.dart';
import 'package:trigger_interactive_app/bus/settings_refresh_event_bus.dart';
import 'package:trigger_interactive_app/db/local_storage_manager.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/models/local/user_data.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_settings.dart';
import 'package:trigger_interactive_app/screens/home/bloc/home_tab_bloc.dart';
import 'package:trigger_interactive_app/screens/train/bloc/train_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';

part 'scenario_settings_event.dart';
part 'scenario_settings_state.dart';

class ScenarioSettingsBloc
    extends Bloc<ScenarioSettingsEvent, ScenarioSettingsState> {
  Scenario selectedScenario;
  HomeTabBloc homeTabBloc;

  bool showTextField = false;
  bool isEditSetting = false;
  bool isTextFieldReadOnly = true;

  TextEditingController presetTextEditingController = TextEditingController();

  List<UserData> userList = [];
  UserData selectedUser;

  List<String> scenarioPresetKeys = [
    "Beginner",
    "Intermediate",
    "Advanced",
    "Expert",
  ];

  String selectedScenarioPresetKey;

  List<ScenarioSettings> tempScenarioSettings;

  ScenarioSettingsBloc(ScenarioSettingsArguments argument)
      : super(ScenarioSettingsInitialState()) {
    selectedScenario = argument.selectedScenario;
    homeTabBloc = argument.homeTabBloc;

    presetTextEditingController = TextEditingController.fromValue(
      TextEditingValue(
        text: "${StringResource.CUSTOM} ${_generateCustomPresetName()}",
      ),
    );
  }

  @override
  Stream<ScenarioSettingsState> mapEventToState(
    ScenarioSettingsEvent event,
  ) async* {
    if (event is ScenarioSettingsInitialEvent) {
      userList = UserDataHandler.allUsers;
      selectedUser = UserDataHandler.currentUser;

      selectedScenarioPresetKey =
          (await LocalStorageManager.getPreviousSelectedScenarioKey()) ??
              "Beginner";

      final ScenarioPreset prevScenario =
          await LocalStorageManager.getUnSavedScenarioSettings();

      if (prevScenario != null) {
        presetTextEditingController.value =
            TextEditingValue(text: prevScenario.label);

        tempScenarioSettings = prevScenario.getSettings();
        showTextField = true;
        isTextFieldReadOnly = false;
      }

      await UserDataHandler.setGameType(selectedScenario.label);
      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsLevelChangeEvent) {
      tempScenarioSettings ??= UserDataHandler.scenarioPresetObjects
          .firstWhere((scenario) => scenario.label == selectedScenarioPresetKey)
          .getSettings()
          .map((settings) => settings.copyWith())
          .toList();

      final ScenarioSettings setting = tempScenarioSettings[event.settingIndex];

      if (setting.level == setting.min && event.isReduced) return;
      if (setting.level == setting.max && !event.isReduced) return;

      final bool isUserScenarioPreset = UserDataHandler.userScenarioPresetKeys
          .contains(selectedScenarioPresetKey);

      if (!isEditSetting) {
        add(
          ScenarioSettingsToggleNewPresetEvent(
            isEdit: isUserScenarioPreset,
            showTextField: true,
            readOnly: isTextFieldReadOnly,
          ),
        );
      }

      setting.level += event.isReduced ? -1 : 1;
      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsUserSelectEvent) {
      UserDataHandler.selectUser(event.selectedUserId);
      selectedUser = UserDataHandler.currentUser;
      eventBus.fire(ScoreBoardRefreshEventBus());
      eventBus.fire(SettingsRefreshEventBus());
      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsPresetChangeEvent) {
      showTextField = false;
      isEditSetting = false;
      tempScenarioSettings = null;
      selectedScenarioPresetKey = event.preset;
      LocalStorageManager.setPreviousSelectedScenarioKey(event.preset);
      LocalStorageManager.clearUnSavedScenarioSettings();
      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsToggleNewPresetEvent) {
      isEditSetting = event.isEdit;
      showTextField = event.showTextField;
      isTextFieldReadOnly = event.readOnly;

      tempScenarioSettings ??= UserDataHandler.scenarioPresetObjects
          .firstWhere((scenario) => scenario.label == selectedScenarioPresetKey)
          .getSettings()
          .map((setting) => setting.copyWith())
          .toList();

      presetTextEditingController.value = TextEditingValue(
        text: isEditSetting
            ? selectedScenarioPresetKey
            : event.label ??
                "${StringResource.CUSTOM} ${_generateCustomPresetName()}",
      );

      add(ScenarioSettingsRetainUnsavedEvent());

      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsSavePresetEvent) {
      final String tempSelectedScenarioPresetKey = selectedScenarioPresetKey;
      final bool keyAlreadyExists = UserDataHandler.userScenarioPresetKeys
          .contains(presetTextEditingController.text);

      if (isEditSetting) {
        if (keyAlreadyExists &&
            (presetTextEditingController.text != selectedScenarioPresetKey)) {
          AppUtils.showToast(StringResource.PRESET_NAME_ALREADY_EXIST);
          return;
        }
      } else {
        if (keyAlreadyExists) {
          AppUtils.showToast(StringResource.PRESET_NAME_ALREADY_EXIST);
          return;
        }

        if ((UserDataHandler.userScenarioPresetKeys?.length ?? 0) >= 10) {
          AppUtils.showToast(StringResource.CAN_ONLY_ADD_10_PRESETS);
          return;
        }
      }

      selectedScenarioPresetKey = presetTextEditingController.text;
      LocalStorageManager.setPreviousSelectedScenarioKey(
        selectedScenarioPresetKey,
      );

      final ScenarioPreset tempPreset = ScenarioPreset(
        label: selectedScenarioPresetKey,
        activeTime: tempScenarioSettings[0].level,
        inActiveTime: tempScenarioSettings[1].level,
        simultaneousTarget: tempScenarioSettings[2].level,
        targetPresentation: tempScenarioSettings[3].level,
      );

      LocalStorageManager.clearUnSavedScenarioSettings();

      UserDataHandler.addScenarioPreset(
        tempPreset,
        isEditSetting ? tempSelectedScenarioPresetKey : null,
      );

      add(ScenarioSettingsToggleNewPresetEvent());

      tempScenarioSettings = null;

      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsDeleteUserPresetEvent) {
      selectedScenarioPresetKey = scenarioPresetKeys.first;

      if (event.preset != null) {
        LocalStorageManager.setPreviousSelectedScenarioKey(
          selectedScenarioPresetKey,
        );
        UserDataHandler.deleteScenarioPreset(event.preset);
      }

      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsRetainUnsavedEvent) {
      if (showTextField && tempScenarioSettings != null) {
        LocalStorageManager.retainUnSavedScenarioSettings(
          ScenarioPreset(
            label: presetTextEditingController.text,
            activeTime: tempScenarioSettings[0].level,
            inActiveTime: tempScenarioSettings[1].level,
            simultaneousTarget: tempScenarioSettings[2].level,
            targetPresentation: tempScenarioSettings[3].level,
          ),
        );
      }
      yield ScenarioSettingsInitialState();
    }

    if (event is ScenarioSettingsAddNewUserEvent) {
      UserDataHandler.addNewUser(name: event.newUserName);
      selectedUser = UserDataHandler.currentUser;

      eventBus.fire(ScoreBoardRefreshEventBus());
      eventBus.fire(SettingsRefreshEventBus());

      yield ScenarioSettingsInitialState();
    }
  }

  ///To set the new preset with label **Custom X** where X => 1,2..* if not already present
  int _generateCustomPresetName({int no = 1}) {
    final String presetLabel = "${StringResource.CUSTOM} $no";
    final bool alreadyExists = UserDataHandler.userScenarioPresetKeys
        .any((element) => element == presetLabel);
    if (!alreadyExists) return no;
    return _generateCustomPresetName(no: no + 1);
  }
}

class ScenarioSettingsArguments {
  final HomeTabBloc homeTabBloc;
  final Scenario selectedScenario;

  ScenarioSettingsArguments(this.selectedScenario, this.homeTabBloc);
}
