import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trigger_interactive_app/db/device_data_handler.dart';
import 'package:trigger_interactive_app/db/user_data_handler.dart';
import 'package:trigger_interactive_app/models/local/user_data.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_preset.dart';
import 'package:trigger_interactive_app/models/scenarios/scenario_settings.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/home/bloc/home_tab_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_progress/bloc/scenario_progress_bloc.dart';
import 'package:trigger_interactive_app/screens/scenario_settings/bloc/scenario_settings_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_app_bar.dart';
import 'package:trigger_interactive_app/widgets/custom_bottom_nav.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_dialog.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';
import 'package:trigger_interactive_app/widgets/custom_text.dart';

class ScenarioSettingsScreen extends StatefulWidget {
  @override
  _ScenarioSettingsScreenState createState() => _ScenarioSettingsScreenState();
}

class _ScenarioSettingsScreenState extends State<ScenarioSettingsScreen> {
  ScenarioSettingsBloc scenarioSettingsBloc;
  String newUserName = "";
  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    scenarioSettingsBloc = context.read<ScenarioSettingsBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      appBar: CustomAppBar(
        scenarioSettingsBloc.selectedScenario.label.toUpperCase(),
        hideBackButton: true,
        connectedDevices: DeviceDataHandler.connectedDevices.length,
      ),
      body: _buildBody(),
      bottomNavigationBar: CustomBottomNav(onTap: _onBottomNavItemTapped),
    );
  }

  Widget _buildBody() {
    return BlocConsumer<ScenarioSettingsBloc, ScenarioSettingsState>(
      listener: (context, state) {
        if (state is ScenarioSettingsInitialState) {
          if (state.error != null) {
            AppUtils.showToast(state.error);
          }
        }
      },
      builder: (context, state) {
        return ListView(
          children: [
            _buildSelectUserWidget(),
            _buildInstructionsWidget(),
            _buildSettingListWidget(),
          ],
        );
      },
    );
  }

  Widget _buildSelectUserWidget() {
    final List userDropDownList = [
      ...scenarioSettingsBloc.userList,
      StringResource.ADD_NEW_USER
    ];

    return Container(
      width: double.infinity,
      height: 75,
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 5),
      color: ColorResource.RED_96262C,
      child: DropdownButtonFormField<String>(
        value: scenarioSettingsBloc.selectedUser?.userId,
        iconEnabledColor: ColorResource.WHITE_FFFFFF,
        onChanged: _onUserSelectedFromDropDown,
        decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: ColorResource.WHITE_FFFFFF),
          ),
        ),
        hint: const CustomText(
          StringResource.ADD_NEW_USER,
          color: ColorResource.WHITE_FFFFFF,
        ),
        selectedItemBuilder: (_) =>
            scenarioSettingsBloc.userList.map<Widget>((user) {
          return CustomText(user.userName, color: ColorResource.WHITE_FFFFFF);
        }).toList(),
        items: userDropDownList.map((user) {
          if (user is UserData) {
            return DropdownMenuItem(
              value: user.userId,
              child: CustomText(user.userName),
            );
          }
          return DropdownMenuItem(
            value: user.toString(),
            child: CustomText(user.toString()),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildInstructionsWidget() {
    return Container(
      color: ColorResource.GREY_E7E7E7,
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: CustomButton(
              50,
              _onInstructionsButtonPressed,
              title: StringResource.INSTRUCTIONS,
              color: ColorResource.WHITE_FFFFFF,
              titleColor: ColorResource.RED_96262C,
            ),
          ),
          const SizedBox(width: 20),
          Expanded(
            child: CustomButton(
              50,
              _onReportsButtonPressed,
              title: StringResource.REPORTS,
              color: ColorResource.WHITE_FFFFFF,
              titleColor: ColorResource.RED_96262C,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSettingListWidget() {
    if (scenarioSettingsBloc.selectedScenarioPresetKey == null) {
      return LinearProgressIndicator(
        backgroundColor: ColorResource.RED_96262C.withOpacity(0.4),
        valueColor: const AlwaysStoppedAnimation(ColorResource.RED_96262C),
      );
    }

    List<ScenarioSettings> presetSettings =
        scenarioSettingsBloc.tempScenarioSettings;

    if (presetSettings == null) {
      final ScenarioPreset preset =
          UserDataHandler.scenarioPresetObjects.firstWhere(
        (e) => e.label == scenarioSettingsBloc.selectedScenarioPresetKey,
        orElse: () => null,
      );

      presetSettings = preset?.getSettings() ?? [];
    }

    return Column(
      children: [
        _buildPresetWidget(),
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          separatorBuilder: (_, __) => Container(
            height: 1,
            color: ColorResource.GREY_E7E7E7,
            width: double.infinity,
          ),
          itemCount: presetSettings.length,
          itemBuilder: (context, index) {
            return _buildSettingItemWidget(presetSettings[index], index);
          },
        ),
        const SizedBox(height: 20),
        Container(
          padding: const EdgeInsets.all(20),
          child: CustomButton(
            50,
            _onStartButtonPressed,
            title: StringResource.START,
            color: ColorResource.RED_96262C,
            focusColor: ColorResource.RED_600C0F,
          ),
        )
      ],
    );
  }

  Widget _buildPresetWidget() {
    final bool isUserPresetSelected = UserDataHandler.userScenarioPresetKeys
        .contains(scenarioSettingsBloc.selectedScenarioPresetKey);

    final List<String> userPresetSelectedDropDownItems =
        !scenarioSettingsBloc.showTextField && isUserPresetSelected
            ? [StringResource.EDIT]
            : [];

    UserDataHandler.userScenarioPresetKeys.sort();

    final userPresetDropDownItems = UserDataHandler.userScenarioPresetKeys.map(
      (userPreset) => DropdownMenuItem(
        value: userPreset,
        child: Container(
          decoration: userPreset == UserDataHandler.userScenarioPresetKeys.first
              ? const BoxDecoration(
                  border:
                      Border(top: BorderSide(color: ColorResource.RED_96262C)))
              : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: CustomText(userPreset, color: ColorResource.GREY_969696),
              ),
              IconButton(
                onPressed: () => _onDeleteUserPresetIconTapped(userPreset),
                icon: const Icon(Icons.close, color: ColorResource.RED_96262C),
              )
            ],
          ),
        ),
      ),
    );

    final addNewDropDownItem = DropdownMenuItem(
      value: StringResource.ADD_NEW,
      child: Container(
        decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: ColorResource.RED_96262C)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            CustomText(
              StringResource.ADD_NEW,
              color: ColorResource.GREY_969696,
            ),
            IconButton(
              icon: Icon(Icons.add, color: ColorResource.RED_96262C),
              onPressed: null,
            ),
          ],
        ),
      ),
    );

    //Combines user-settings and default-presets
    final List<DropdownMenuItem<String>> dropDownItems = [
      ...userPresetSelectedDropDownItems.map((preset) {
        return DropdownMenuItem(
          value: preset,
          child: CustomText(preset, color: ColorResource.GREY_969696),
        );
      }),
      addNewDropDownItem,
      ...scenarioSettingsBloc.scenarioPresetKeys.map((preset) {
        return DropdownMenuItem(
          value: preset,
          child: CustomText(
            preset,
            color: ColorResource.GREY_969696.withOpacity(0.65),
          ),
        );
      }),
      ...userPresetDropDownItems,
    ];

    final selectedDropDownItems = [
      StringResource.ADD_NEW,
      ...userPresetSelectedDropDownItems,
      ...scenarioSettingsBloc.scenarioPresetKeys,
      ...UserDataHandler.userScenarioPresetKeys,
    ]
        .map<Widget>(
            (preset) => CustomText(preset, color: ColorResource.GREY_969696))
        .toList();

    if (scenarioSettingsBloc.showTextField) {
      return _buildAddPresetTextFieldWidget(dropDownItems: dropDownItems);
    }

    return _buildSelectPresetWidget(
      dropDownItems: dropDownItems,
      selectedDropDownItems: selectedDropDownItems,
    );
  }

  Widget _buildSelectPresetWidget({
    List<DropdownMenuItem<String>> dropDownItems,
    List<Widget> selectedDropDownItems,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: DropdownButtonFormField<String>(
        value: scenarioSettingsBloc.selectedScenarioPresetKey,
        selectedItemBuilder: (_) => selectedDropDownItems,
        items: dropDownItems,
        onChanged: _onSelectedPresetValueChanged,
      ),
    );
  }

  Widget _buildAddPresetTextFieldWidget({
    List<DropdownMenuItem<String>> dropDownItems,
  }) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: TextFormField(
        maxLength: 15,
        focusNode: _focusNode,
        onChanged: (newVal) {
          const int maxLength = 15;
          if (newVal.length > maxLength) {
            scenarioSettingsBloc.presetTextEditingController.value =
                TextEditingValue(
              text: newVal,
              selection: const TextSelection(
                baseOffset: maxLength,
                extentOffset: maxLength,
              ),
              composing: const TextRange(start: 0, end: maxLength),
            );
          }
        },
        style: const TextStyle(
          color: ColorResource.RED_96262C,
          fontSize: FontSize.subFontSize,
        ),
        controller: scenarioSettingsBloc.presetTextEditingController,
        // readOnly: scenarioSettingsBloc.isTextFieldReadOnly,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          counterText: "",
          prefixText: scenarioSettingsBloc.isEditSetting ? "*" : "",
          prefixStyle: const TextStyle(color: ColorResource.RED_96262C),
          suffix: SizedBox(
            width: MediaQuery.of(context).size.width * 0.4,
            child: DropdownButtonHideUnderline(
              child: SizedBox(
                height: 25,
                child: DropdownButton(
                  isExpanded: true,
                  icon: const Icon(Icons.more_horiz),
                  items: [
                    const DropdownMenuItem(
                      value: StringResource.SAVE,
                      child: CustomText(
                        StringResource.SAVE,
                        color: ColorResource.GREY_969696,
                      ),
                    ),
                    ...dropDownItems
                  ],
                  onChanged: _onSelectedPresetValueChanged,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSettingItemWidget(ScenarioSettings settings, int index) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () => _onSettingInfoIconTapped(index, settings.label),
            child: const Icon(Icons.info_outline),
          ),
          const SizedBox(width: 5),
          Expanded(
            child: CustomText(
              settings.label,
              fontSize: FontSize.scenarioSettingsFontSize,
              font: Font.HelveticaBold,
              color: ColorResource.GREY_969696,
            ),
          ),
          Row(
            children: [
              Material(
                color: ColorResource.RED_96262C,
                child: InkWell(
                  onTap: () => _onChangedLevelValue(index, isReduced: true),
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    child: const Icon(
                      Icons.remove,
                      size: FontSize.incrementDecrementSize,
                      color: ColorResource.WHITE_FFFFFF,
                    ),
                  ),
                ),
              ),
              Container(
                height: 45,
                width: 50,
                decoration: BoxDecoration(
                  color: ColorResource.WHITE_FFFFFF,
                  border: Border.all(color: ColorResource.GREY_969696),
                ),
                child: Center(
                  child: CustomText(
                    settings.level.toString(),
                    fontSize: FontSize.incrementDecrementSize,
                    font: Font.HelveticaBold,
                    color: ColorResource.GREY_969696,
                  ),
                ),
              ),
              Material(
                color: ColorResource.RED_96262C,
                child: InkWell(
                  onTap: () => _onChangedLevelValue(index),
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    child: const Icon(
                      Icons.add,
                      size: FontSize.incrementDecrementSize,
                      color: ColorResource.WHITE_FFFFFF,
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  void _onDeleteUserPresetIconTapped(String userPreset,
      {bool shouldExitDropDown = true}) {
    if (shouldExitDropDown) Navigator.pop(context);

    showDialog(
      context: context,
      builder: (_) => CustomDialog(
        title: StringResource.DELETE_SETTING,
        description:
            "${StringResource.ARE_YOU_SURE_DELETE} $userPreset ${StringResource.SCENARIO_SETTING}?",
        acceptButtonText: StringResource.YES,
        cancelButtonText: StringResource.NO,
        onCancelButtonPressed: () {
          Navigator.pop(context);
          scenarioSettingsBloc.add(ScenarioSettingsDeleteUserPresetEvent(null));
        },
        onAcceptButtonPressed: () {
          Navigator.pop(context);
          scenarioSettingsBloc
              .add(ScenarioSettingsDeleteUserPresetEvent(userPreset));
        },
      ),
    );
  }

  void _onInstructionsButtonPressed() {
    showDialog(
      context: context,
      builder: (_) => CustomDialog(
        title: StringResource.BASIC_COMBAT_INSTRUCTIONS,
        isTitleCenter: true,
        descriptionWidget: Column(
          children: const [
            CustomText(
              StringResource.BASIC_COMBAT_DESCRIPTION,
              color: ColorResource.WHITE_FFFFFF,
              textAlign: TextAlign.center,
              lineHeight: 1.5,
            ),
            SizedBox(height: 40),
            CustomText(
              "${StringResource.MINIMUM_TARGETS_REQUIRED} 1",
              color: ColorResource.WHITE_FFFFFF,
              font: Font.HelveticaBold,
              textAlign: TextAlign.center,
            ),
          ],
        ),
        acceptButtonText: StringResource.OK,
        onAcceptButtonPressed: () => Navigator.pop(context),
      ),
    );
  }

  Future<void> _onSelectedPresetValueChanged(String preset) async {
    switch (preset) {
      case StringResource.ADD_NEW:
        _focusNode.requestFocus();
        scenarioSettingsBloc.add(
          ScenarioSettingsToggleNewPresetEvent(
            showTextField: true,
            readOnly: false,
          ),
        );
        break;
      case StringResource.EDIT:
        _focusNode.requestFocus();
        scenarioSettingsBloc.add(
          ScenarioSettingsToggleNewPresetEvent(
            isEdit: true,
            showTextField: true,
          ),
        );
        break;
      case StringResource.SAVE:
        if (scenarioSettingsBloc.isEditSetting) {
          _buildOverwriteDialog();
          return;
        }
        scenarioSettingsBloc.add(ScenarioSettingsSavePresetEvent());
        break;

      default:
        scenarioSettingsBloc.add(ScenarioSettingsPresetChangeEvent(preset));
    }
  }

  void _buildOverwriteDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return CustomDialog(
          title: StringResource.OVERWRITE_SETTINGS,
          description: StringResource.OVERWRITE_SETTINGS_DESCRIPTION,
          acceptButtonText: StringResource.YES,
          cancelButtonText: StringResource.NO,
          onAcceptButtonPressed: () {
            scenarioSettingsBloc.add(ScenarioSettingsSavePresetEvent());
            Navigator.pop(context);
          },
          onCancelButtonPressed: () {
            scenarioSettingsBloc.add(
              ScenarioSettingsToggleNewPresetEvent(
                showTextField: true,
                readOnly: false,
                label: scenarioSettingsBloc.selectedScenarioPresetKey,
              ),
            );
            Navigator.pop(context);
          },
        );
      },
    );
  }

  void _onBottomNavItemTapped(int index) {
    scenarioSettingsBloc.homeTabBloc.add(HomeTabBottomNavChangeEvent(index));
    Navigator.pop(context);
  }

  void _onChangedLevelValue(int settingsIndex, {bool isReduced = false}) {
    scenarioSettingsBloc.add(
        ScenarioSettingsLevelChangeEvent(settingsIndex, isReduced: isReduced));
  }

  void _onReportsButtonPressed() {}

  void _onStartButtonPressed() {
    scenarioSettingsBloc.add(ScenarioSettingsRetainUnsavedEvent());

    if (DeviceDataHandler.connectedDevices.length < 3) {
      showDialog(
        context: context,
        builder: (context) {
          return CustomDialog(
            title: StringResource.CONNECT_DEVICES,
            description: StringResource.YOU_MUST_CONNECT_3STA_TEXT,
            acceptButtonText: StringResource.OK,
            onAcceptButtonPressed: () => Navigator.pop(context),
          );
        },
      );
      return;
    }

    ScenarioPreset preset;

    if (scenarioSettingsBloc.tempScenarioSettings != null) {
      preset = ScenarioPreset(
        label: "",
        activeTime: scenarioSettingsBloc.tempScenarioSettings[0].level,
        inActiveTime: scenarioSettingsBloc.tempScenarioSettings[1].level,
        simultaneousTarget: scenarioSettingsBloc.tempScenarioSettings[2].level,
        targetPresentation: scenarioSettingsBloc.tempScenarioSettings[3].level,
      );
    }

    preset ??= UserDataHandler.scenarioPresetObjects.firstWhere(
      (e) => e.label == scenarioSettingsBloc.selectedScenarioPresetKey,
      orElse: () => null,
    );

    if (DeviceDataHandler.connectedDevices.length < preset.simultaneousTarget) {
      AppUtils.showToast(StringResource.SIMULTANEOUS_TARGET_ERROR);
      return;
    }

    Navigator.pushNamed(
      context,
      AppRoutes.SCENARIO_PROGRESS_SCREEN,
      arguments: ScenarioProgressArguments(
        selectedPreset: preset,
      ),
    );
  }

  Widget _buildNewUserDialogWidget() {
    return Dialog(
      child: SizedBox(
        width: 200,
        height: 200,
        child: Column(children: [
          const SizedBox(height: 30),
          const CustomText(StringResource.NEW_USER),
          const SizedBox(height: 10),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            color: Colors.grey[200],
            child: TextField(
              decoration:
                  const InputDecoration(hintText: StringResource.USERNAME),
              onChanged: (val) => setState(() => newUserName = val),
            ),
          ),
          const SizedBox(height: 30),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            SizedBox(
              width: 100,
              child: CustomButton(
                50,
                () {
                  newUserName = "";
                  Navigator.pop(context);
                },
                titleColor: ColorResource.RED_600C0F,
                title: StringResource.CANCEL,
                titleFontSize: FontSize.smallFontSize,
              ),
            ),
            SizedBox(
              width: 100,
              child: CustomButton(
                40,
                () {
                  scenarioSettingsBloc.add(
                    ScenarioSettingsAddNewUserEvent(newUserName),
                  );
                  newUserName = "";

                  Navigator.pop(context);
                },
                title: StringResource.SUBMIT,
                titleFontSize: FontSize.smallFontSize,
                color: ColorResource.RED_600C0F,
              ),
            )
          ])
        ]),
      ),
    );
  }

  void _onSettingInfoIconTapped(int index, String label) {
    String description;

    switch (index) {
      case 0:
        description = StringResource.ACTIVE_TIME_INFO_TEXT;
        break;
      case 1:
        description = StringResource.INACTIVE_TIME_INFO_TEXT;
        break;

      case 2:
        description = StringResource.SIMULTANEOUS_TARGETS_INFO_TEXT;
        break;

      default:
        description = StringResource.TARGET_PRESENTATION_INFO_TEXT;
    }

    showDialog(
      context: context,
      builder: (context) => CustomDialog(
        title: label,
        description: description,
        acceptButtonText: StringResource.OK,
        onAcceptButtonPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  void _onUserSelectedFromDropDown(String userId) {
    if (userId != StringResource.ADD_NEW_USER) {
      scenarioSettingsBloc.add(ScenarioSettingsUserSelectEvent(userId));
    } else {
      showDialog(
        context: context,
        builder: (context) => _buildNewUserDialogWidget(),
      );
    }
  }
}
