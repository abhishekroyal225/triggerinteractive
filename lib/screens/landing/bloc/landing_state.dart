part of 'landing_bloc.dart';

class LandingState extends BaseEquatable {}

class LandingInitialState extends LandingState {
  final String error;

  LandingInitialState({this.error});

  @override
  bool operator ==(Object obj) => false;
}

class LandingLoadingState extends LandingState {}
