import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:trigger_interactive_app/utils/base_equatable.dart';

part 'landing_event.dart';
part 'landing_state.dart';

class LandingBloc extends Bloc<LandingEvent, LandingState> {
  LandingBloc() : super(LandingInitialState());

  @override
  Stream<LandingState> mapEventToState(LandingEvent event) async* {
    if (event is LandingInitialEvent) {
      yield LandingInitialState();
    }
  }
}
