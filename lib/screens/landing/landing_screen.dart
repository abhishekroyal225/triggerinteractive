import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trigger_interactive_app/router.dart';
import 'package:trigger_interactive_app/screens/home/home_tab_screen.dart';
import 'package:trigger_interactive_app/screens/landing/bloc/landing_bloc.dart';
import 'package:trigger_interactive_app/utils/app_utils.dart';
import 'package:trigger_interactive_app/utils/color_resource.dart';
import 'package:trigger_interactive_app/utils/font.dart';
import 'package:trigger_interactive_app/utils/font_size.dart';
import 'package:trigger_interactive_app/utils/image_resource.dart';
import 'package:trigger_interactive_app/utils/string_resource.dart';
import 'package:trigger_interactive_app/widgets/custom_button.dart';
import 'package:trigger_interactive_app/widgets/custom_loading_widget.dart';
import 'package:trigger_interactive_app/widgets/custom_scaffold.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  LandingBloc landingBloc;

  @override
  void initState() {
    landingBloc = context.read<LandingBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: ColorResource.BLACK_333333,
        title: SvgPicture.asset(ImageResource.TRIGGER_LOGO_ICON, width: 30),
        centerTitle: true,
      ),
      color: ColorResource.GREY_E7E7E7,
      // bottomNavigationBar: const _SubscribeFooter(),
      body: BlocListener<LandingBloc, LandingState>(
        listener: (context, state) {
          if (state is LandingInitialState) {
            if (state.error != null) AppUtils.showToast(state.error);
          }
        },
        child: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * 0.1),
                SvgPicture.asset(
                  ImageResource.TRIGGER_LOGO,
                  width: MediaQuery.of(context).size.width,
                ),
                const SizedBox(height: 170),
                CustomButton(
                  50,
                  _onConnectDevicesButtonPressed,
                  title: StringResource.CONNECT_DEVICES,
                  focusColor: ColorResource.RED_600C0F,
                  color: ColorResource.RED_96262C,
                ),
                const SizedBox(height: 20),
                CustomButton(
                  50,
                  _onSkipConnectButtonPressed,
                  title: StringResource.SKIP_CONNECT,
                  titleFont: Font.HelveticaRegular,
                  titleFontSize: FontSize.subFontSize,
                  titleColor: ColorResource.GREY_969696,
                ),
                const SizedBox(height: 15),
              ],
            ),
            const _CircularLoading()
          ],
        ),
      ),
    );
  }

  void _onConnectDevicesButtonPressed() {
    Navigator.pushReplacementNamed(
      context,
      AppRoutes.HOME_SCREEN,
      arguments: HomeTabArguments(initialTabIndex: 1),
    );
  }

  void _onSkipConnectButtonPressed() {
    Navigator.pushReplacementNamed(
      context,
      AppRoutes.HOME_SCREEN,
      arguments: HomeTabArguments(),
    );
  }

  @override
  void dispose() {
    landingBloc?.close();
    super.dispose();
  }
}

class _CircularLoading extends StatelessWidget {
  const _CircularLoading();

  @override
  Widget build(BuildContext context) {
    final bool isLoading = context.select<LandingBloc, bool>(
      (bloc) => bloc.state is LandingLoadingState,
    );

    if (isLoading) {
      return CustomLoadingWidget(
        color: ColorResource.WHITE_FFFFFF.withOpacity(0.5),
      );
    }

    return Container();
  }
}

// TODO : Delete or Uncommented when subscription is used. 
// class _SubscribeFooter extends StatelessWidget {
//   const _SubscribeFooter({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final bool isSubscribed = context.select<AuthenticationBloc, bool>(
//       (bloc) => bloc.subscriptionStatus != SubscriptionStatus.none,
//     );

//     if (isSubscribed) {
//       return Container(
//         width: double.infinity,
//         height: 100,
//         color: ColorResource.BLACK_333333,
//         child: const Center(
//           child: CustomText(
//             StringResource.YOU_ARE_SUBSCRIBED_FULL_ACCESS,
//             textAlign: TextAlign.center,
//             color: ColorResource.WHITE_FFFFFF,
//           ),
//         ),
//       );
//     }
//     return Container(
//       width: double.infinity,
//       height: 100,
//       color: ColorResource.BLACK_333333,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           const CustomText(
//             StringResource.GET_FULL_ACCESS,
//             color: ColorResource.WHITE_FFFFFF,
//           ),
//           const SizedBox(height: 10),
//           SizedBox(
//             width: 170,
//             child: CustomButton(
//               40,
//               () => _onSubscribeButtonPressed(context),
//               color: ColorResource.RED_96262C,
//               focusColor: ColorResource.RED_600C0F,
//               title: StringResource.SUBSCRIBE,
//               titleFont: Font.HelveticaRegular,
//               titleFontSize: FontSize.subFontSize,
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   void _onSubscribeButtonPressed(BuildContext context) {
//     //TODO: Navigate to settings subscription page
//     Navigator.pushReplacementNamed(
//       context,
//       AppRoutes.HOME_SCREEN,
//       arguments: HomeTabArguments(initialTabIndex: 4),
//     );
//   }
// }
